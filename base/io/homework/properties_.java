package base.io.homework;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.junit.Test;

import base.designPattern.vo.Dog;

public class properties_ {
    public static void main(String[] args) {

    }

    @Test
    public void proTest() throws FileNotFoundException, IOException {
        Properties p = new Properties();
        p.load(new FileReader("D:\\vsProject\\base\\io\\src1\\dog.properties"));
        System.out.println(
                new Dog(p.getProperty("name"), Integer.parseInt(p.getProperty("age")), p.getProperty("color")));
    }
}
