package base.io.homework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class addLineNum {
    public static void main(String[] args) {

    }

    @Test
    public void addLine() throws IOException {
        String filePath = "D:\\vsProject\\base\\io\\src2\\2.txt\\";
        BufferedReader bf = new BufferedReader(new FileReader(filePath));

        String line = "";
        int i = 0;
        List<String> txtData = new ArrayList<>();
        while ((line = bf.readLine()) != null) {
            txtData.add(String.valueOf(i++) + line);
        }
        bf.close();
        BufferedWriter bw = new BufferedWriter(new FileWriter(filePath));
        for (String string : txtData) {
            System.out.println(string);
            bw.write(string);
            bw.newLine();
        }

        bw.close();

    }
}
