package base.io.nor;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Test;

public class copyFile {
    public static void main(String[] args) {

    }

    @Test
    public void copyFile1() {
        String filePath = "D:\\vsProject\\base\\io\\io\\src1\\1.txt";
        String resPath = "D:\\vsProject\\base\\io\\io\\src2\\2.txt";
        InputStream inputStream = null;
        OutputStream outputStream = null;
        byte[] bytes = new byte[3];
        try {
            outputStream = new FileOutputStream(resPath);
            inputStream = new FileInputStream(filePath);
            int readLen = 0;
            while ((readLen = inputStream.read(bytes)) != -1) {
                // 一定要用这个不然会多输出
                outputStream.write(bytes, 0, readLen);
                // outputStream.write(bytes);
            }
        } catch (IOException e) {

            e.printStackTrace();
        } finally {
            try {
                if (outputStream != null)
                    outputStream.close();
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException e) {

                e.printStackTrace();
            }

        }
    }

    @Test
    public void copyFile2() {

        String filePath = "D:\\vsProject\\base\\io\\src1\\1.txt";
        String resPath = "D:\\vsProject\\base\\io\\src2\\2.txt";
        BufferedReader bufferedReader = null;
        BufferedWriter bufferedWriter = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(filePath));
            bufferedWriter = new BufferedWriter(new FileWriter(resPath));
            String temp = "";
            while ((temp = bufferedReader.readLine()) != null) {
                bufferedWriter.write(temp);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {

            e.printStackTrace();
        } finally {
            if (bufferedReader != null)
                try {
                    bufferedReader.close();
                } catch (IOException e1) {

                    e1.printStackTrace();
                }
            if (bufferedWriter != null)
                try {
                    bufferedWriter.close();
                } catch (IOException e) {

                    e.printStackTrace();
                }
        }

    }

    @Test
    public void copyFile3() {
        String filePath = "D:\\vsProject\\base\\io\\src1\\1.png";
        String resPath = "D:\\vsProject\\base\\io\\src2\\2.jpg";
        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        try {
            bufferedInputStream = new BufferedInputStream(new FileInputStream(filePath));
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(resPath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int length = 0;
        byte[] buf = new byte[11];//一次读取11个字节
        try {
            while ((length = bufferedInputStream.read(buf)) != -1) {
                bufferedOutputStream.write(buf, 0, length);
            }
        } catch (IOException e) {
         
            e.printStackTrace();
        } finally {
            try {
                if (bufferedInputStream != null) bufferedInputStream.close();
                if(bufferedOutputStream!=null)bufferedOutputStream.close();
            } catch (IOException e) {
               
                e.printStackTrace();
            }

        }
    }

}
