package base.io.objIO;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.Test;

import base.designPattern.vo.objTest;

public class objIO {
    public static void main(String[] args) {

    }
    @Test
    public void objIOWrite() throws IOException {
        String filePath = "D:\\vsProject\\base\\io\\src1\\data.zyh";
        ObjectOutputStream oo = null;
        try {
            oo = new ObjectOutputStream(new FileOutputStream(filePath));
            oo.writeObject(new objTest("12", 1));
            oo.writeUTF("Zyhadaw");
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            if(oo!=null)oo.close();
        }

    }
    @Test
    public void objIORead() throws IOException, ClassNotFoundException {
        String filePath = "D:\\vsProject\\base\\io\\src1\\data.zyh";
        ObjectInputStream oi = null;
        oi=new ObjectInputStream(new FileInputStream(filePath));
        // System.out.println(oi.readObject());
   
       
        
   
        System.out.println(oi.readUTF());
        if(oi!=null)oi.close();
    }
}
