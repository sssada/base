package base.SortAlgorithm;

import java.util.Arrays;
import java.util.Random;

public class MergeSort {
    public static void main(String[] args) {
        int[] nums = new int[100];
        Random r = new Random();
        for (int i = 0; i < nums.length; i++) {
            nums[i] = r.nextInt(100);
        }

        nums = mergeSort(nums, 0, nums.length - 1);
        System.out.println(Arrays.toString(nums));
    }

    public static int[] mergeSort(int[] nums, int bg, int end) {
        int mid = (bg + end) / 2;
        if (bg >= end)
            return nums;

        mergeSort(nums, bg, mid);
        mergeSort(nums, mid + 1, end);

        return merge(nums, bg, end);

    }

    public static int[] merge(int[] nums, int bg, int end) {
        int temp[] = new int[end - bg + 1];
        int mid = (bg + end) / 2;
        int index = 0;
        int i = bg, j = mid + 1;
        while (i <= mid && j <= end) {
            if (nums[i] > nums[j]) {
                temp[index++] = nums[j++];
            } else {
                temp[index++] = nums[i++];
            }
        }
        while (i <= mid) {
            temp[index++] = nums[i++];
        }
        while (j <= end) {
            temp[index++] = nums[j++];
        }
        for (int a = 0; a < temp.length; a++) {
            nums[bg++] = temp[a];
        }
        return nums;
    }

}
