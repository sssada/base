package base.SortAlgorithm;

import java.util.Random;

public class QuickSort {
    public static void main(String[] args) {

        int[] nums = new int[10];
        Random r = new Random();
        for (int i = 0; i < nums.length; i++) {
            nums[i] = r.nextInt(10);
        }
        nums = quickSort(nums, 0, nums.length - 1, (nums.length - 1) / 2);
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i] + ",");
        }
    }

    public static int[] quickSort(int[] nums, int bg, int end, int index) {
        if (bg >= end)
            return nums;
        int value = nums[index];
        nums[index] = nums[bg];
        nums[bg] = value;
        int right = end;
        int left = bg;

        while (left < right) {
            while (left < right && nums[right] < value) {
                right--;
            }
            nums[left] = nums[right];

            while (left < right && nums[left] >= value) {
                left++;
            }
            nums[right] = nums[left];

        }
        nums[left] = value;
        quickSort(nums, bg, left-1,(bg+(right-bg)/2));
        quickSort(nums, left + 1, end,(right+1+(end-right-1)/2));

        return nums;
    }
}