package base.SortAlgorithm;

import java.util.Random;

public class SelectSort {
    public static void main(String[] args) {
        int[] nums = new int[10];
        Random r = new Random();
        for (int i = 0; i < nums.length; i++) {
            nums[i] = r.nextInt(10);
        }

        nums = selectSort(nums);

        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i] + ",");
        }
    }

    public static int[] selectSort(int[] nums) {
        for (int i = 0; i < nums.length - 1; i++) {
            int minIndex=i;
            int min=nums[i];
            for (int j = i + 1; j < nums.length; j++) {
                if (min > nums[j]) {
                    minIndex=j;
                    min=nums[j];
                }
            }
            if(minIndex==i)continue;
            nums[i] = nums[i] + nums[minIndex];
            nums[minIndex] = nums[i] - nums[minIndex];
            nums[i] = nums[i] - nums[minIndex];
        }
        return nums;
    }
}
