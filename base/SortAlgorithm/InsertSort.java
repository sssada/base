package base.SortAlgorithm;

import java.util.Random;

public class InsertSort{
    public static void main(String[] args) {
        int[] nums = new int[10];
        Random r = new Random();
        for (int i = 0; i < nums.length; i++) {
            nums[i] = r.nextInt(10);
        }
        
        insertSort(nums);
   
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i] + ",");
        }

    }

    public static int[] insertSort(int[] nums){
        for (int i = 1; i < nums.length; i++) {// 插入排序
            int j=i;
            int temp=nums[i];
            while(j>0&&temp<nums[j-1]){
                nums[j]=nums[j-1];
                j-=1;
            }
            nums[j]=temp;
        }
        return nums;
    }
}