package base.SortAlgorithm;

import java.util.Random;

public class ShellSort {
    public static void main(String[] args) {
        int[] nums = new int[100];
        Random r = new Random();
        for (int i = 0; i < nums.length; i++) {
            nums[i] = r.nextInt(100);
        }

        nums = shellSort(nums);

        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i] + ",");
        }
    }

    public static int[] shellSort(int[] nums) {
        for (int inc = nums.length / 2; inc > 0; inc = inc / 2) {// 间隔距离
            for (int i = inc; i < nums.length; i++) {// 插入排序
                int j=i;
                int temp=nums[i];
                while(j>=inc&&temp<nums[j-inc]){
                    nums[j]=nums[j-inc];
                    j-=inc;
                }
                nums[j]=temp;
            }
        }
        return nums;
    }

}
