package base.SortAlgorithm;

import java.util.Arrays;
import java.util.Random;

public class HeapSort {
    public static void main(String[] args) {
        int n = 99999999;
        // int[] nums = { 2,5,3};
        int[] nums = new int[n];
        Random r = new Random();
        for (int i = 0; i < nums.length; i++) {
            nums[i] = r.nextInt(n);
        }
        
        System.out.println(Arrays.toString(heapSort(nums)));
    }

    public static int[] heapSort(int[] nums) {
        
        downAdj(nums);

        for(int i=0;i<nums.length;i++){
            int size=nums.length-2-i;
            swap(nums,0,size+1);
            adjHeap(nums,(size-1)/2,size);
        }
        return nums;
    }
    public static int[] downAdj(int[] nums) {
        int parent = (nums.length-2) / 2 ;
        for (int i = parent; i >= 0; i--) {
            adjHeap(nums, parent, nums.length-1);
        }
        return nums;
    }

    public static void adjHeap(int[] nums,int index,int size){
        int right=index*2+2;
        int left=index*2+1;
        if(index>size||index<0){
            return;
        }
        if(left>size)return; //叶子节点
        else if(left==size){//有左孩子
            if(nums[index]<nums[left]){
                swap(nums,index,left);
                adjHeap(nums,left,size);
            }
        }else{
            if(nums[right]>nums[left]&&nums[right]>nums[index]){
                swap(nums,index,right);
                adjHeap(nums,right,size);
            }else if(nums[left]>=nums[right]&&nums[left]>nums[index]){
                swap(nums,index,left);
                adjHeap(nums,right,left);
            }
        } 
    }

    public static void swap(int[] nums, int a, int b) {
        if (a == b)
            return;
        nums[a] = nums[a] + nums[b];
        nums[b] = nums[a] - nums[b];
        nums[a] = nums[a] - nums[b];
    }
}
