package base.SortAlgorithm;

import java.util.Arrays;
import java.util.Random;

public class SpeedTest {
    public static void main(String[] args) {
        int n=10000000;
        int[] nums = new int[n];
        Random r = new Random();
        for (int i = 0; i < nums.length; i++) {
            nums[i] = r.nextInt(n);
        }
        
        long s;
        s=System.currentTimeMillis();
        QuickSort.quickSort(Arrays.copyOf(nums, nums.length),0,nums.length-1,(nums.length-1)/2);
        System.out.println("FastSort:" + (System.currentTimeMillis()-s));

        s=System.currentTimeMillis();
        HeapSort.heapSort(Arrays.copyOf(nums, nums.length));
        System.out.println("heapSort:" + (System.currentTimeMillis()-s));

        s=System.currentTimeMillis();
        MergeSort.mergeSort(Arrays.copyOf(nums, nums.length),0,nums.length-1);
        System.out.println("MergeSort:" + (System.currentTimeMillis()-s));

        s=System.currentTimeMillis();
        ShellSort.shellSort(Arrays.copyOf(nums, nums.length));
        System.out.println("shellsort:" + (System.currentTimeMillis()-s));

        s=System.currentTimeMillis();
        InsertSort.insertSort(Arrays.copyOf(nums, nums.length));
        System.out.println("insertSort:" + (System.currentTimeMillis()-s));

        s=System.currentTimeMillis();
        SelectSort.selectSort(Arrays.copyOf(nums, nums.length));
        System.out.println("selectSort:" + (System.currentTimeMillis()-s));

        s=System.currentTimeMillis();
        BubbleSort.bubbleSort(Arrays.copyOf(nums, nums.length));
        System.out.println("BubbleSort:" + (System.currentTimeMillis()-s));

        


        // s=System.currentTimeMillis();
        // nums = ShellSort.shellSort(nums);
        // System.out.println("shellsort:" + (System.currentTimeMillis()-s));

        // for (int i = 0; i < nums.length; i++) {
        //     System.out.print(nums[i] + ",");
        // }
    }

 
}
