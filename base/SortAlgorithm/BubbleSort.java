package base.SortAlgorithm;

import java.util.Random;

public class BubbleSort {
    public static void main(String[] args) {
        int[] nums=new int[100];
        Random r=new Random();
        for (int i = 0; i < nums.length; i++) {
            nums[i]=r.nextInt(100);
        }
        nums=bubbleSort(nums);
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i]+",");
        }
        
    }
    public static int[] bubbleSort(int[] nums){
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length-i-1; j++) {
                if(nums[j]>nums[j+1]){
                    nums[j]=nums[j]+nums[j+1];
                    nums[j+1]=nums[j]-nums[j+1];
                    nums[j]=nums[j]-nums[j+1];
                }
            }
        }
        return nums;
    }
}
