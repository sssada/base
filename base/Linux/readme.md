# Linux常用命令
## yum软件管理相关
* 安装软件：yum install xxx
* 卸载软件：yum remove xxx
* 搜索软件：yum search xxx
* 清理缓存：yum clean packages
* 列出已安装：yum list
* 软件包信息：yum info xxx
## 文件操作命令
* ls列出当前文件夹下的文件
  * ls-a 列出所有文件，包括隐藏文件
  * ls-l 列出所有文件，以及文件相关的信息（权限、大小、时间等）
* cd/xxx 进入xxx目录。cd/表示进入根目录
* tough 新建文件
* mkdir 新建文件夹
  * mkdir -p xxx/xxx/xxx 新建多级文件夹
* rm xxx 删除xxx文件（只能删文件不能删文件夹）
  * rm -r xxx 删除文件夹，并且递归进去把所有的文件都删了。不过会询问
  * rm -rf xxx 删除文件夹，并且递归进去把所有的文件都删了。不会询问，强制删除
  * rmdir xxx 删除文件夹（但是只能删除空的）
* cp xxx zzz 把xxx复制到zzz
* mv xxx zzz 把xxx移动到zzz
* pwd 当前显示路径  
## vim
* 安装 yum install vim
* vim xxx使用vim编辑文件
  * 打开文件后，i进入插入模式
  * 退出 esx+:wq w保存 q退出
* gg快速定位到文件首行
* G快速定位到文件末尾
* dd删除光标坐在行，ndd删除光标所在行在内的n行
* u撤回操作
* 重做操作ctrl+r（和撤回操作相反）
* yy复制当前行，nyy复制光标所在行在内的n行
* p向下粘贴，P向上粘贴（注意大小写）
* . 重复上一个操作
* 显示行号:set number
* 取消显示:set nonumber
## 文件权限
* rwx
  * read代表的数字是4,$2^2$
  * write代表的数字是2,$2^1$
  * x(执行)代表的数字是1,$2^0$
* 文件权限一般由三组数据组成
  * User文件所有者的权限
  * Group同组用户的权限
  * Other其他用户的权限
* 权限由数字来表示
  * 7=4+2+1 表示rwx都可以
  * 4表示可以r
  * 3表示可以wx
  * 5表示可以rx以此类推
* 修改文件权限 chmod num xxx

## 文件读取
* tail xxx 文件末尾读取，而且只显示几行
* head xxx 文件头部读取,也只显示头几行
* cat xxx 读取整个文件
* more xxx 分页读取可以用回车往下翻
* less xxx 可控分页可以上下翻
* 退出读取：ctrl+c可退出大部分情况；less有冒号，用ctrl+c退不出，键入q或ctrl+z退出。
## 管道命令
x|y 可以将x运行的结果作为输入给y运行。
## 文件搜索
* 文件搜索命令：grep,grep查找具体的文本,而不是查找文件  
  * grep -v 是反向查询，查询不包含查询内容的文本
  * 格式：grep -n "xxxx" filename
  * -n：显示行数，可省略。
  * 组合：grep "word" fileName | more -- 在文件中查找“word”，并且分页读取
## 找到所有使用 80端口的应用
* lsof -i:80。还有进一步筛选的话，可以使用管道配合grep使用。  
![](img/%E7%AE%A1%E9%81%93.png)  
* netstat可以检查当前网络状态，使用管道配合grep使用就可以查看特定端口了
  * -a，显示所有
  * -n，不用别名显示，只用数字显示
  * -p，显示进程号和进程名
  * 可以组合使用netstat -anp|grep 80
## 进程相关命令
* ps -A 显示所有进程信息
* ps -u username 指定用户的进程信息
* ps -ef 连同命令行显示所有进程信息
* ps -l 列出与本次登录有关的进程信息
* ps -aux 显示当前内存中的进程信息
* top 查看内存中进程的动态信息
* kill -9 pid 杀死进程。
## 文件压缩 tar
* 创建压缩文件：tar -cf 压缩后的文件名.tar 要压缩的文件名
* 解压 tar -xf fileName.tar
## 文件查找命令find
* find .表示查找当前目录
* find /表示查找当前目录以及当前目录的子目录
* -type
  * f表示查找文件
  * d表示查找文件夹
* -name "*.c" 查找文件名符合"*.c"的
* 组合使用find . -type d -name "*.c" 查找当前目录下文件名符合"*.c"的文件夹
