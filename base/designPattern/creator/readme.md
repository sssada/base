# [单例模式(Singleton)](Singleton/)
# [简单工厂模式(SimpleFactoryPattern)](SimpleFactoryPattern/)
# [工厂方法模式(FactoryMethodPattern)](FactoryMethodPattern/)
# [抽象工厂模式(AbstractFactory)](AbstractFactory/)
# [原型模式(Prototype)](Prototype/)
# [建造者模式(Builder)](Builder/)

# 相似创建者模式的对比
## 工厂方法模式和建造者模式
**工厂方法模式**注重整体对象的创建，而**建造者模式**注重部件构造的过程，通过一个个部件的搭建来最终生成一个复杂对象。  
也就是说**工厂方法模式**生成电脑就是直接生成一台电脑，**建造者模式**就是先拿到cpu、gpu然后主板什么的最终拼成一台电脑。
## 抽象工厂模式和建造者模式
**抽象工厂模式**关注的是一个产品族的生产。他不关心构建的过程，只关心什么产品由什么工厂生产即可。  
**建造者**则是按照指定的蓝图构建产品，他的目的是通过组装零件来生产一个新产品。  
如果**抽象工厂模式**是汽车配件生产厂，那**建造者**就是汽车组装厂。由抽象工厂生产零件，再由建造者组装。