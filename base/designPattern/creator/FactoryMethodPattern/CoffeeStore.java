package base.designPattern.creator.FactoryMethodPattern;

import base.designPattern.creator.FactoryMethodPattern.abs.Coffee;
import base.designPattern.creator.FactoryMethodPattern.abs.CoffeeFactory;

public class CoffeeStore {
    private CoffeeFactory factory;
    CoffeeStore(CoffeeFactory factory){
        this.factory=factory;
    }
    public Coffee orderCoffee(){
        return factory.createCoffee();
    }
}
