/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-13 16:20:51
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 16:05:48
 * @ Description:
 */

/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-13 16:20:51
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-13 16:43:44
 * @ Description:
 */

package base.designPattern.creator.FactoryMethodPattern.imp;

import base.designPattern.creator.FactoryMethodPattern.abs.Coffee;

public class Cappuccino implements Coffee {
    private String name="Cappuccino";
    @Override
    public void addMilk() {
        System.out.println("加卡布奇诺的奶");
    }

    @Override
    public void addSugar() {
        System.out.println("加卡布奇诺的糖");
    }

    @Override
    public String getName() {
        
        return name;
    }
    
    
}
