/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-13 16:47:30
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 16:05:58
 * @ Description:
 */

package base.designPattern.creator.FactoryMethodPattern.imp;

import base.designPattern.creator.FactoryMethodPattern.abs.Coffee;
import base.designPattern.creator.FactoryMethodPattern.abs.CoffeeFactory;

public class LatteFactory implements CoffeeFactory{

    @Override
    public Coffee createCoffee() {
       
        return new Latte();
    }
    
}
