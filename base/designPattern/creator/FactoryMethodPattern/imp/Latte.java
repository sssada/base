/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-13 16:44:15
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-13 16:56:10
 * @ Description:
 */

package base.designPattern.creator.FactoryMethodPattern.imp;

import base.designPattern.creator.FactoryMethodPattern.abs.Coffee;

public class Latte implements Coffee{
    private String name="Latte";
    @Override
    public void addMilk() {
       System.out.println("加拿铁的奶");
        
    }

    @Override
    public void addSugar() {
        System.out.println("加拿铁的糖");
    }

    @Override
    public String getName() {
       
        return name;
    }
    
    
}
