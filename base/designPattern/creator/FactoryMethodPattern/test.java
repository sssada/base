package base.designPattern.creator.FactoryMethodPattern;

import base.designPattern.creator.FactoryMethodPattern.abs.Coffee;
import base.designPattern.creator.FactoryMethodPattern.abs.CoffeeFactory;
import base.designPattern.creator.FactoryMethodPattern.imp.CappuccinoFactory;

public class test {
    public static void main(String[] args) {
        CoffeeFactory factory = new CappuccinoFactory();
        CoffeeStore store = new CoffeeStore(factory);

        Coffee coffee=store.orderCoffee();
        coffee.addMilk();
        coffee.addSugar();

    }
}
