/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-13 16:12:38
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-13 16:53:58
 * @ Description:
 */

package base.designPattern.creator.FactoryMethodPattern.abs;

public interface Coffee {
    void addMilk();

    void addSugar();

    String getName();

}