/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-13 16:15:09
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-13 16:20:28
 * @ Description:
 */

package base.designPattern.creator.FactoryMethodPattern.abs;

public interface CoffeeFactory {
    Coffee createCoffee();
}