/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-19 14:11:09
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-19 14:16:03
 * @ Description:
 */

package base.designPattern.creator.Builder;

import base.designPattern.creator.Builder.imp.BeefHamburgBuilder;
import base.designPattern.creator.Builder.imp.FishHamburgBuilder;

public class test {
    
    public static void main(String[] args) {
        Director director=new Director();
        FishHamburgBuilder fishHamburgBuilder=new FishHamburgBuilder();
        director.setBuilder(fishHamburgBuilder);
        director.createHamburg();
        System.out.println("____________________________________________________");
        BeefHamburgBuilder beefHamburgBuilder=new BeefHamburgBuilder();
        director.setBuilder(beefHamburgBuilder);
        director.createHamburg();
        
    }
}
