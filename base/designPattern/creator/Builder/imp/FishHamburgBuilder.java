/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-19 14:07:40
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-19 14:14:36
 * @ Description:
 */

package base.designPattern.creator.Builder.imp;

import base.designPattern.creator.Builder.Hamburg;
import base.designPattern.creator.Builder.abs.Builder;

public class FishHamburgBuilder extends Builder {

    public void createMeat() {
        System.out.println("往汉堡里加鱼肉");
        hamburg.setMeat("fish");
    }

    public void setName() {
        System.out.println("鳕鱼汉堡");
        hamburg.setName("鳕鱼汉堡");
    }

    public void createVegetable() {
        System.out.println("加蔬菜");
        hamburg.setVegetable("蔬菜");
    }

    public void createBread() {
        System.out.println("加面包");
        hamburg.setBread("加面包");
    }

    public Hamburg getHamburg() {
        return hamburg;
    }
}
