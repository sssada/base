/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-19 13:43:07
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-19 14:13:59
 * @ Description:
 */
package base.designPattern.creator.Builder.abs;

import base.designPattern.creator.Builder.Hamburg;

public abstract class Builder {
    protected Hamburg hamburg=new Hamburg();
    
    public abstract void createMeat();

    public abstract void setName();

    public abstract void createVegetable();

    public abstract void createBread();

    public abstract Hamburg getHamburg();
}
