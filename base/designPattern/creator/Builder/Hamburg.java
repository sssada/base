/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-19 13:48:17
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-19 13:55:08
 * @ Description:
 */

package base.designPattern.creator.Builder;

public class Hamburg {
    private String name;
    private String meat;
    private String bread;
    private String vegetable;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeat() {
        return meat;
    }

    public void setMeat(String meat) {
        this.meat = meat;
    }

    public String getBread() {
        return bread;
    }

    public void setBread(String bread) {
        this.bread = bread;
    }

    public String getVegetable() {
        return vegetable;
    }

    public void setVegetable(String vegetable) {
        this.vegetable = vegetable;
    }

}
