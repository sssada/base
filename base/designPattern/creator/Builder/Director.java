/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-19 13:59:34
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-19 14:11:37
 * @ Description:
 */

package base.designPattern.creator.Builder;

import base.designPattern.creator.Builder.abs.Builder;

public class Director {
    private Builder builder;
    
    public Builder getBuilder() {
        return builder;
    }

    public void setBuilder(Builder builder) {
        this.builder = builder;
    }

    public Hamburg createHamburg() {
        builder.createBread();
        builder.createMeat();
        builder.createVegetable();
        builder.setName();
        return builder.getHamburg();
    }
}
