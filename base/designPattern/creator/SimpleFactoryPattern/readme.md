# 简单工厂模式
使用一个接口**A**来规定工厂生产的产品的的规范。然后使用这个接口**A**的实现类来说明实际生产的产品。 
## 举例
我们的程序需要输出日志
```
public class Log {
    public void log(String name){
        if (name.equals("txt")) {
            System.out.println("往txt中输出数据");
        } else if (name.equals("cmd")) {
            System.out.println("往CMD中输出数据");
        } 
    }
}
```
但是一般情况下，我们会有很多种``Log``类，如果每个``Log``类都可以往txt和命令行中输出日志。假设有n种``Log``类，那么这段代码我们就要写n次，这问题还不大。但是突然有一天，你需要往一数据库里面写日志了，那我们就需要修改上面的代码，改成这样。
```
public class Log {
    public void log(String name){
        if (name.equals("txt")) {
            System.out.println("往txt中输出数据");
        } else if (name.equals("cmd")) {
            System.out.println("往CMD中输出数据");
        } else if(name.equals("db")){
            System.out.println("往数据库种输出日志");
        }
    }
}
```
改一次还简单，但是别忘了**我们有n个Log类**，这就要了老命了。我们要去n个地方改，而且忘记改了就会导致系统出错。  
因此我们可以使用简单工厂模式。
1. 抽象出一个``Log``产品  
   我们定义一个``Log``接口来规范``Log``的功能。
 ```
 public interface Log{
    public void writeLog();
}
```
2. 写实现类``CMDLog``,``TxtLog``来表示具体``Log``的类（具体的产品）  
```
public class CMDLog implements Log{
    @Override
    public void writeLog() {
        System.out.println("往CMD中输出数据");
        
    }
}
public class TxtLog implements Log{
    @Override
    public void writeLog() {
        System.out.println("往txt中输出数据");
    }
}
```
3. 定义一个``LogFactory``工厂来返回Log产品  
```
public class LogFactory {
    public static Log createLog(String name) {
        if (name.equals("txt")) {
            return new TxtLog();
        } else if (name.equals("cmd")) {
            return new CMDLog();
        } else {
            return null;
        }
    }
}
```
4. 不同的``Log``调用工厂来获得``Log``实现类，进行业务操作即可  
```
public class ApacheLog {
    public void log(String name){
        Log log=LogFactory.createLog(name);
        log.writeLog();
    }
    public void ApacheLogMethod(){
        System.out.println("Apache日志专属方法");
    }
}
public class SysLog {
    public void log(String name){
        Log log=LogFactory.createLog(name);
        log.writeLog();
    }
    public void sysLogMethod(){
        System.out.println("系统日志专属方法");
    }
}
```
``SysLog``和``ApacheLog``都有他们独有的方法，但是又可以随意的往任何地方输出日志，虽然最后还是用了if/else来判断具体往哪里输出日志，但是当我们需要增加新的需求时，我们只需要修改``LogFactory``的代码和新增一个实现类就好了。不需要去修改``SysLog``和``ApacheLog``了。
## 测试
```
 public static void main(String[] args) {
        ApacheLog apacheLog=new ApacheLog();
        SysLog sysLog=new SysLog();

        apacheLog.log("txt");
        apacheLog.ApacheLogMethod();

        sysLog.log("cmd");
        sysLog.sysLogMethod();
    }
```
输出：  
往txt中输出数据  
Apache日志专属方法  
往CMD中输出数据     
系统日志专属方法  
## 优点：  
1. 一个调用者想创建一个对象，只要知道其名称就可以了。 
2. 扩展性高，如果想增加一个产品，只要扩展一个工厂类就可以。 
3. 屏蔽产品的具体实现，调用者只关心产品的接口。  
## 缺点：
每次增加一个产品时，都需要增加一个具体类和对象实现工厂，使得系统中类的个数成倍增加，在一定程度上增加了系统的复杂度。
## 使用场景： 
1. 日志记录器：记录可能记录到本地硬盘、系统事件、远程服务器等，用户可以选择记录日志到什么地方。 
2. 数据库访问，当用户不知道最后系统采用哪一类数据库，以及数据库可能有变化时。 
3. 设计一个连接服务器的框架，需要三个协议，"POP3"、"IMAP"、"HTTP"，可以把这三个作为产品类，共同实现一个接口。
## 注意事项
作为一种创建类模式，在任何需要生成复杂对象的地方，都可以使用工厂方法模式。有一点需要注意的地方就是复杂对象适合使用工厂模式，而简单对象，特别是只需要通过 new 就可以完成创建的对象，无需使用工厂模式。如果使用工厂模式，就需要引入一个工厂类，会增加系统的复杂度。
