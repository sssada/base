package base.designPattern.creator.SimpleFactoryPattern;

public class test {
    public static void main(String[] args) {
        ApacheLog apacheLog=new ApacheLog();
        SysLog sysLog=new SysLog();

        apacheLog.log("txt");
        apacheLog.ApacheLogMethod();

        sysLog.log("cmd");
        sysLog.sysLogMethod();
    }
}
