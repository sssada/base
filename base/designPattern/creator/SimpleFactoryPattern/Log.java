/**
 * @ Author: OldZhang
 * @ Create Time: 2022-04-19 11:48:36
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-09 19:36:50
 * @ Description:
 */

package base.designPattern.creator.SimpleFactoryPattern;
public interface Log{
    public void writeLog();
}