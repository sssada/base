/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-09 19:37:12
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-09 20:07:23
 * @ Description:
 */

package base.designPattern.creator.SimpleFactoryPattern;

import base.designPattern.creator.SimpleFactoryPattern.imp.CMDLog;
import base.designPattern.creator.SimpleFactoryPattern.imp.TxtLog;

public class LogFactory {
    public static Log createLog(String name) {
        if (name.equals("txt")) {
            return new TxtLog();
        } else if (name.equals("cmd")) {
            return new CMDLog();
        } else {
            return null;
        }
    }
}
