/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-09 19:41:13
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-09 19:43:41
 * @ Description:
 */

package base.designPattern.creator.SimpleFactoryPattern;

public class ApacheLog {
    public void log(String name){
        Log log=LogFactory.createLog(name);
        log.writeLog();
    }
    public void ApacheLogMethod(){
        System.out.println("Apache日志专属方法");
    }
}
