/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-09 19:38:12
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-09 19:38:38
 * @ Description:
 */

package base.designPattern.creator.SimpleFactoryPattern.imp;

import base.designPattern.creator.SimpleFactoryPattern.Log;

public class TxtLog implements Log{

    @Override
    public void writeLog() {
        System.out.println("往txt中输出数据");
    }
    
}
