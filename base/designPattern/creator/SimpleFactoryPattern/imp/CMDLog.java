/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-09 19:38:55
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-09 19:45:35
 * @ Description:
 */

package base.designPattern.creator.SimpleFactoryPattern.imp;

import base.designPattern.creator.SimpleFactoryPattern.Log;

public class CMDLog implements Log{
    @Override
    public void writeLog() {
        System.out.println("往CMD中输出数据");
        
    }
    
}
