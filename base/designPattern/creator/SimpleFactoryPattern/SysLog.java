/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-09 19:41:13
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-09 19:43:09
 * @ Description:
 */

package base.designPattern.creator.SimpleFactoryPattern;

public class SysLog {
    public void log(String name){
        Log log=LogFactory.createLog(name);
        log.writeLog();
    }
    public void sysLogMethod(){
        System.out.println("系统日志专属方法");
    }
}
