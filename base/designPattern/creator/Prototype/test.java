/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-19 10:02:55
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-19 10:05:44
 * @ Description:
 */

package base.designPattern.creator.Prototype;

public class test {
    public static void main(String[] args) throws CloneNotSupportedException {
        Certificate certificate = new Certificate();
        certificate.setName("原型证书");
        Certificate copy = certificate.clone();
        copy.setName("复制证书");

        System.out.println(certificate.getName());
        System.out.println(copy.getName());
    }

}
