/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-19 10:01:10
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-19 10:05:29
 * @ Description:
 */

package base.designPattern.creator.Prototype;

public class Certificate implements Cloneable{
    Certificate(){
        System.out.println("新建证书");
    }
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Certificate clone() throws CloneNotSupportedException {
        System.out.println("复制一份证书");
        return (Certificate) super.clone();
    }
    
}
