/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-18 12:44:26
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-18 13:13:54
 * @ Description:序列化，反序列化破坏单例模式
 */

package base.designPattern.creator.Singleton;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import base.designPattern.creator.Singleton.Lazy.Lazy2;

public class Serialization {
    public static void main(String[] args) throws Exception{
        write2File();
        Lazy2 l=read4File();
        Lazy2 l2=read4File();
        System.out.println(l==l2);
    }

    // 序列化
    public static void write2File() throws FileNotFoundException, IOException {
        Lazy2 l = Lazy2.getInstance();
        ObjectOutputStream oStream = new ObjectOutputStream(new FileOutputStream("obj.txt"));
        oStream.writeObject(l);
        oStream.close();
    }
    //反序列化
    public static Lazy2 read4File() throws FileNotFoundException, IOException, ClassNotFoundException{
        ObjectInputStream oInputStream=new ObjectInputStream(new FileInputStream("obj.txt"));
        Lazy2 l=(Lazy2)oInputStream.readObject();
        oInputStream.close();
        return l;
        
    }
}
