# 单例模式
单例模式确保了在整个系统中这个类只有一个对象被创建。他提供了一种访问他唯一的一个对象的方式，可以直接访问，不需要额外实例化对象。  
单例模式有几个显著的特点：
1. 私有化构造方法，这样就不能通过构造方法来创建对象了。
2. 对外提供静态方法来让外接获取对象。
3. 私有化静态变量，来保证全局只有一个变量。
## 分类
单例模式分为两类：
1. 饿汉式：类加载的时候对象就会被创建
2. 懒汉式：类加载时对象不会被创建，首次使用的时候类才会被创建。
## 饿汉式的几种实现方式
类加载的时候就生成了对象，但是不用的话就很浪费内存.
### 静态变量方法
```
public class HungrySingleton1 {
    
    private HungrySingleton1(){}

    private static HungrySingleton1 instance=new HungrySingleton1();

    public static HungrySingleton1 getInstance(){
        return instance;
    }

}
```
### 静态代码块方法
```
public class HungrySingleton2 {
    private HungrySingleton2(){}

    private static HungrySingleton2 instance;

    static{
        instance=new HungrySingleton2();
    }
    public static HungrySingleton2 getInstance(){
        return instance;
    }
}
```
上述的这两种方式其实差不多，就是在类里面new了一个私有静态变量，然后对外的方法只返回他。
### 使用枚举类实现单例模式
```
public enum HungrySingleton3 {
    INSTANCE;
    public void method1(){
        System.out.println("do sth");
    }
}
```
怎么样是不是非常的简单，而且枚举类的实现方式是最安全的。不会被反射或者序列化破解。
## 懒汉式的几种实现方式
懒汉式在类被使用到的时候，才会生成对象避免了内存的浪费。
### 线程不安全的懒汉式
这个方式就是在调用``getInstance()``方法的时候会先判断是否已经创建了对象。如果创建了就返回，没有就创建一个在返回。但是在多线程的情况下，有可能出二次创建的现象。
```
public class Lazy1 {
    private Lazy1() {
    }

    private static Lazy1 instance;

    public static  Lazy1 getInstance() {
        if (instance == null) {
            instance=new Lazy1();
        }
        return instance;
    }
}
```
### 线程安全的懒汉方
和上面的差异就是多了一个``synchronized``关键字。使用synchronized关键字给``getInstance()``方法加锁，但是锁的加在了整个方法上，但是性能较差。
```
public class Lazy1 {
    private Lazy1() {
    }

    private static Lazy1 instance;

    public static synchronized Lazy1 getInstance() {
        if (instance == null) {
            instance=new Lazy1();
        }
        return instance;
    }
}
```
### 双重检查模式
双重检查锁方式，把锁加在了方法里面而不是整个方法。只有在需要创建对象的时候才需要获得锁，让锁的粒度更细了。效率也就更高。
```
public class Lazy2 {
    private Lazy2(){}
    //volatile 是为了保证指令的有序性，不然在多线程环境下 由于jvm的指令重排序可能会导致空指针
    private static volatile Lazy2 instance;

    public static Lazy2 getInstance(){
        if(instance==null){
            synchronized(Lazy2.class){
                if(instance==null){
                    instance=new Lazy2();
                }
            }
        }
        return instance;
    }
}
```
### 静态内部类模式
通过在对象里面创建一个静态内部类来持有对象。静态内部类在类加载是不会被加载，只有在被调用的时候才会被加载，从而实现了懒汉式。而且是线程安全的。
```
public class Lazy3 {
    private Lazy3(){}    

    private static class Lazy3Holder{
        private static final Lazy3 INSTANCE=new Lazy3();
    }
    public static Lazy3 getInstance(){
        return Lazy3Holder.INSTANCE;
    }
}
```
## 问题
### 序列化、反序列化破坏单例模式
先说一下基本的概念，
1. 序列化：把对象转换成字节流，然后写入到文件或者通过网络传输给其他终端。
2. 反序列化：把字节流重新转化为对象。  
因此我们把类序列化成字节流，然后保存到文件中.就能做到创建对象。
```
    public static void write2File() throws FileNotFoundException, IOException {
        Lazy2 l = Lazy2.getInstance();
        ObjectOutputStream oStream = new ObjectOutputStream(new FileOutputStream("obj.txt"));
        oStream.writeObject(l);
        oStream.close();
    }
```
然后通过字节流在反序列化成对象。
```
    public static Lazy2 read4File() throws FileNotFoundException, IOException, ClassNotFoundException{
        ObjectInputStream oInputStream=new ObjectInputStream(new FileInputStream("obj.txt"));
        Lazy2 l=(Lazy2)oInputStream.readObject();
        oInputStream.close();
        return l;
    }
```
我们直接反序列化两次，生成两个对象。输出结果为：**false**。说明我们生成了两个不同的对象，破坏了单例模式全局唯一对象的特性。
```
    public static void main(String[] args) throws Exception{
        write2File();
        System.out.println(read4File()==read4File());
    }
```
### 反射破解单例模式
反射可以通过一个类对应的class类来获得这个类**所有**的成员变量和方法，**不管是私有的还是公开的**。  
可以理解为，每个类都有一个大门。门里面是他的私有的变量和方法，只有类自己有钥匙打开大门去操作他们。但是反射不讲武德，他有电锯。直接把门锯开了，然后去操作这个类私有的变量和方法。
```
public class Reflection {
    public static void main(String[] args) throws Exception{
        //获取Lazy2的class类
        Class<Lazy2> class1=Lazy2.class;
        //获取私有构造方法
        Constructor con=class1.getDeclaredConstructor();
        //暴力破门（设置私有方法可访问）
        con.setAccessible(true);
        //使用构造方法创建对象
        Lazy2 l=(Lazy2) con.newInstance();
        Lazy2 l2=(Lazy2) con.newInstance();
        System.out.println(l==l2);
    }
}
```
输出：**false**
## 解决方式
一下的例子我都是在[Lazy2](Lazy/Lazy2.java)上修改的
### 序列化、反序列化
在需要序列化反序列化的单例模式类中添加``readResolve()``方法来规定反序列化时的返回值。  
在反序列化时，Java会判断是否有这个函数，如果有就返回这个函数的返回值。没有就会new一个新的对象来返回
```
    public Object readResolve(){
        return instance;
    }
```
```
 public static void main(String[] args) throws Exception{
        write2File();
        Lazy2 l=read4File();
        Lazy2 l2=read4File();
        System.out.println(l==l2);
    }
```
输出：**true**
### 反射
既然反射时通过调用构造方法来实现的创建对象的，那么我们就在构造方法里面加上一点逻辑判断，在有对象的情况下报错，不让他创建就好了。
```
private Lazy2(){
        synchronized(Lazy2.class){
            if(instance!=null){
                throw new RuntimeErrorException(null, "不准创建多个对象");
            }
        } 
    }
```
在常规调用之后，在使用反射创建对象就会报错。
```
    Lazy2 l=Lazy2.getInstance();
    Lazy2 l1=(Lazy2) con.newInstance();
```
但是直接使用反射还是可以破解
```
Lazy2 l1=(Lazy2) con.newInstance();
Lazy2 l2=(Lazy2) con.newInstance();
```
**反射实在太猛了**
### 枚举大法好！
Java规范字规定，每个枚举类型及其定义的枚举变量在JVM中都是唯一的。完美符合单例模式！


