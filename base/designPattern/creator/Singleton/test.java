/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-18 10:26:58
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-18 12:43:38
 * @ Description:
 */

package base.designPattern.creator.Singleton;

import base.designPattern.creator.Singleton.Lazy.Lazy3;

public class test {
    public static void main(String[] args) {
        Lazy3 l=Lazy3.getInstance();
        Lazy3 l2=Lazy3.getInstance();
        System.out.println(l==l2);

        
    }
}
