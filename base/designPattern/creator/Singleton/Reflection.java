/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-18 12:59:43
 * @ Modified by: OldZhang
 * @ Modified time: 2022-09-22 19:49:32
 * @ Description:反射破坏单例模式
 */

package base.designPattern.creator.Singleton;

import java.lang.reflect.Constructor;

import base.designPattern.creator.Singleton.Lazy.Lazy2;

public class Reflection {
    public static void main(String[] args) throws Exception{
        //获取Lazy2的class类
        Class<Lazy2> class1=Lazy2.class;
        //获取私有构造方法
        Constructor<Lazy2> con=class1.getDeclaredConstructor();
        //暴力破门（设置私有方法可访问）
        con.setAccessible(true);
        
        Lazy2 l=Lazy2.getInstance();
        //使用构造方法创建对象
        Lazy2 l1=(Lazy2) con.newInstance();
        Lazy2 l2=(Lazy2) con.newInstance();
       
        // System.out.println(l==l2);
    }
}
