/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-18 09:33:00
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-18 13:31:09
 * @ Description:双重检查锁方式，就是让锁的粒度更细
 */

package base.designPattern.creator.Singleton.Lazy;

import java.io.Serializable;

import javax.management.RuntimeErrorException;

public class Lazy2 implements Serializable{
    private Lazy2(){
        synchronized(Lazy2.class){
            if(instance!=null){
                throw new RuntimeErrorException(null, "不准创建多个对象");
            }
        } 
    }
    //volatile 是为了保证指令的有序性，不然在多线程环境下 由于jvm的指令重排序可能会导致空指针
    private static volatile Lazy2 instance;

    public static Lazy2 getInstance(){
        if(instance==null){
            synchronized(Lazy2.class){
                if(instance==null){
                    instance=new Lazy2();
                }
            }
            
        }
        return instance;
    }
    
    public Object readResolve(){
        return instance;
    }
}
