/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-18 09:55:59
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-18 11:25:26
 * @ Description:使用静态内部类的方式
 */

package base.designPattern.creator.Singleton.Lazy;

public class Lazy3 {
    private Lazy3(){}    

    private static class Lazy3Holder{
        private static final Lazy3 INSTANCE=new Lazy3();
    }
    public static Lazy3 getInstance(){
        return Lazy3Holder.INSTANCE;
    }
}
