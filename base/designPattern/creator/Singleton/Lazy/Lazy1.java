/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-18 09:30:20
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-18 09:43:04
 * @ Description:线程不安全的方式,加上synchronized就安全了
 */
package base.designPattern.creator.Singleton.Lazy;

public class Lazy1 {
    private Lazy1() {
    }

    private static Lazy1 instance;

    // public static Lazy1 getInstance() {
    //     if (instance == null) {
    //         instance=new Lazy1();
    //     }
    //     return instance;
        
    // }
    public static synchronized Lazy1 getInstance() {
        if (instance == null) {
            instance=new Lazy1();
        }
        return instance;
        
    }
}
