/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-15 14:49:14
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-18 10:43:00
 * @ Description:饿汉式单例模式，静态变量版
 */

package base.designPattern.creator.Singleton.Hungry;

public class HungrySingleton1 {
    
    private HungrySingleton1(){}

    private static HungrySingleton1 instance=new HungrySingleton1();

    public static HungrySingleton1 getInstance(){
        return instance;
    }

}
