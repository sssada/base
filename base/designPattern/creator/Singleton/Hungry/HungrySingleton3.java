/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-18 10:02:23
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-18 10:38:11
 * @ Description:枚举类实现单例模式
 */

package base.designPattern.creator.Singleton.Hungry;

public enum HungrySingleton3 {
    INSTANCE;
    public void method1(){
        System.out.println("do sth");
    }
}
