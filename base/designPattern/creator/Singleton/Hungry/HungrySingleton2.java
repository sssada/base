/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-15 14:49:14
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-18 10:43:27
 * @ Description:饿汉式单例模式，静态代码块版
 */

package base.designPattern.creator.Singleton.Hungry;

public class HungrySingleton2 {
    private HungrySingleton2(){}

    private static HungrySingleton2 instance;

    static{
        instance=new HungrySingleton2();
    }
    public static HungrySingleton2 getInstance(){
        return instance;
    }
}
