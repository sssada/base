/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-14 16:02:11
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 16:05:19
 * @ Description:
 */

package base.designPattern.creator.AbstractFactory.imp;

import base.designPattern.creator.AbstractFactory.abs.Computer;

public class AppleComputer implements Computer {

    @Override
    public void game() {

        System.out.println("用苹果电脑打游戏");

    }

    @Override
    public void office() {

        System.out.println("用苹果电脑办公");

    }

}
