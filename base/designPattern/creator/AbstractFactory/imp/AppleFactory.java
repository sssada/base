/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-14 16:06:42
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 16:07:12
 * @ Description:
 */

package base.designPattern.creator.AbstractFactory.imp;

import base.designPattern.creator.AbstractFactory.abs.Computer;
import base.designPattern.creator.AbstractFactory.abs.ElectronicsFactory;
import base.designPattern.creator.AbstractFactory.abs.Phone;

public class AppleFactory implements ElectronicsFactory{

    @Override
    public Phone createPhone() {
        
        return new ApplePhone();
    }

    @Override
    public Computer creatComputer() {
       
        return new AppleComputer();
    }
    
}
