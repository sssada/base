/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-14 16:01:09
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 16:05:24
 * @ Description:
 */

package base.designPattern.creator.AbstractFactory.imp;

import base.designPattern.creator.AbstractFactory.abs.Computer;

public class HuaweiComputer implements Computer{

    @Override
    public void game() {
        
        System.out.println("用华为电脑打游戏");
    }

    @Override
    public void office() {
       
        System.out.println("用华为电脑办公");
    }
    
}
