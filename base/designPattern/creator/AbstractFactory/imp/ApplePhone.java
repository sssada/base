/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-14 16:00:12
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 19:04:08
 * @ Description:
 */

package base.designPattern.creator.AbstractFactory.imp;

import base.designPattern.creator.AbstractFactory.abs.Phone;

public class ApplePhone implements Phone{

    @Override
    public void call() {
        System.out.println("用苹果手机打电话");    
    }
    
}
