/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-14 15:58:52
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 16:06:32
 * @ Description:
 */

package base.designPattern.creator.AbstractFactory.imp;

import base.designPattern.creator.AbstractFactory.abs.Computer;
import base.designPattern.creator.AbstractFactory.abs.ElectronicsFactory;
import base.designPattern.creator.AbstractFactory.abs.Phone;

public class HuaweiFactory implements ElectronicsFactory{

    @Override
    public Phone createPhone() {
    
       return new HuaweiPhone();
    }

    @Override
    public Computer creatComputer() {
   
        return new HuaweiComputer();
    }
    
}
