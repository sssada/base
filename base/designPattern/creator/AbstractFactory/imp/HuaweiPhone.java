/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-14 15:59:33
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 16:00:00
 * @ Description:
 */

package base.designPattern.creator.AbstractFactory.imp;

import base.designPattern.creator.AbstractFactory.abs.Phone;

public class HuaweiPhone implements Phone{

    @Override
    public void call() {
        System.out.println("用华为手机打电话");
        
    }
    
}
