/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-14 15:56:10
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 19:02:03
 * @ Description:
 */

package base.designPattern.creator.AbstractFactory.abs;

public interface Computer {
    void game();

    void office();
}
