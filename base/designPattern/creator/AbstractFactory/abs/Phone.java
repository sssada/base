/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-14 15:55:11
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 15:57:41
 * @ Description:
 */

package base.designPattern.creator.AbstractFactory.abs;

public interface Phone {
    void call();
}
