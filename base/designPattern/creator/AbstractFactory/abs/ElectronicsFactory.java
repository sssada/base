/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-14 15:53:07
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 15:57:35
 * @ Description:
 */

package base.designPattern.creator.AbstractFactory.abs;
public interface ElectronicsFactory{
    Phone createPhone();
    Computer creatComputer();
}