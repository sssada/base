/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-14 16:17:53
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-15 14:14:18
 * @ Description:
 */

package base.designPattern.creator.AbstractFactory;

import base.designPattern.creator.AbstractFactory.abs.Computer;
import base.designPattern.creator.AbstractFactory.abs.ElectronicsFactory;
import base.designPattern.creator.AbstractFactory.abs.Phone;
import base.designPattern.creator.AbstractFactory.imp.HuaweiFactory;


public class test {
    public static void main(String[] args) {
        ElectronicsFactory factory=new HuaweiFactory();
        // ElectronicsFactory factory=new AppleFactory();

        ElectronicsStore store=new ElectronicsStore(factory);

        Computer computer= store.sellComputer();
        Phone phone= store.sellPhone();

        computer.game();
        computer.office();

        phone.call();
        
    }
}
