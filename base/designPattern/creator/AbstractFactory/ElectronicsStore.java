/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-14 16:09:35
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-14 16:20:01
 * @ Description:
 */

package base.designPattern.creator.AbstractFactory;

import base.designPattern.creator.AbstractFactory.abs.Computer;
import base.designPattern.creator.AbstractFactory.abs.ElectronicsFactory;
import base.designPattern.creator.AbstractFactory.abs.Phone;

public class ElectronicsStore {
    private ElectronicsFactory factory;

    public ElectronicsStore(ElectronicsFactory factory) {
        this.factory=factory;
    }
    
    Phone sellPhone(){
        return factory.createPhone();
    }
    Computer sellComputer(){
        return factory.creatComputer();
    }
}
