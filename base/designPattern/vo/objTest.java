package base.designPattern.vo;

import java.io.Serializable;

public class objTest implements Serializable {
    private String var1;
    private int var2;
    // private static final long serialVersionUID=1L;
    public objTest(String var1, int var2) {
        this.var1 = var1;
        this.var2 = var2;
    }
    @Override
    public String toString() {
        return "objTest [var1=" + var1 + ", var2=" + var2 + "]";
    }
    public String getVar1() {
        return var1;
    }
    public void setVar1(String var1) {
        this.var1 = var1;
    }
    public int getVar2() {
        return var2;
    }
    public void setVar2(int var2) {
        this.var2 = var2;
    }
    

}
