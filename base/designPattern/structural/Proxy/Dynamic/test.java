/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-20 14:59:01
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-20 16:00:50
 * @ Description:
 */

package base.designPattern.structural.Proxy.Dynamic;

import base.designPattern.structural.Proxy.Dynamic.abs.ComputerOrz;

public class test {
    public static void main(String[] args) {
        ProxyFactory proxyFactory = new ProxyFactory();
        ComputerOrz computerOrz = proxyFactory.getProxyObj();
        
        computerOrz.sell();
    }
}
