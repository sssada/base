/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-20 15:24:31
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-20 16:13:41
 * @ Description:
 */

package base.designPattern.structural.Proxy.Dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import base.designPattern.structural.Proxy.Dynamic.abs.ComputerOrz;
import base.designPattern.structural.Proxy.Dynamic.imp.Asus;

public class ProxyFactory {
    private Asus asus = new Asus();

    public ComputerOrz getProxyObj() {
        // 可以通过Proxy.newProxyInstance()来动态的获得代理对象
        /*
         * ClassLoader loader, 类加载器
         * Class<?>[] interfaces, 该类所实现的接口
         * InvocationHandler, 代理对象要调用的方法
         */
        ComputerOrz proxyObj = (ComputerOrz) Proxy.newProxyInstance(
                asus.getClass().getClassLoader(),
                asus.getClass().getInterfaces(),
                new InvocationHandler() {
                    @Override
                    /*
                     * proxy 代理对象，就是返回的那个
                     * method 反射出来的方法对象
                     * args 调用方法的参数
                     */
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        method.invoke(asus, args);
                        System.out.println("电脑城加价2000卖给消费者(动态代理)");
                        return null;
                    }
                });
        return proxyObj;
    }
}
