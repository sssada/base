/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-20 14:52:08
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-20 16:00:32
 * @ Description:
 */

package base.designPattern.structural.Proxy.Dynamic.imp;

import base.designPattern.structural.Proxy.Dynamic.abs.ComputerOrz;

public class Asus implements ComputerOrz{
    @Override
    public void sell() {
        System.out.println("根据华硕出厂价8000卖电脑");
    }
}