/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-20 14:59:01
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-20 14:59:22
 * @ Description:
 */

package base.designPattern.structural.Proxy.Static;

public class test {
    public static void main(String[] args) {
        ComputerCity city=new ComputerCity();
        city.sell();
    }
}
