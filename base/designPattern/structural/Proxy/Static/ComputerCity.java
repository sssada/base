/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-20 14:57:25
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-20 15:05:07
 * @ Description:
 */

package base.designPattern.structural.Proxy.Static;

import base.designPattern.structural.Proxy.Static.abs.ComputerOrz;
import base.designPattern.structural.Proxy.Static.imp.Asus;

public class ComputerCity implements ComputerOrz{
    private Asus asus=new Asus();

    @Override
    public void sell() {
        asus.sell();
        System.out.println("电脑城再加价2000卖出");
        
    }
}
