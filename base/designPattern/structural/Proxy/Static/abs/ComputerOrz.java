/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-20 14:53:28
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-20 14:55:42
 * @ Description:上游电脑厂商接口
 */

package base.designPattern.structural.Proxy.Static.abs;

public interface ComputerOrz {
    void sell();
}
