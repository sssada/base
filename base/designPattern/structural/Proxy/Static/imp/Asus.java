/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-20 14:52:08
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-20 14:58:47
 * @ Description:
 */

package base.designPattern.structural.Proxy.Static.imp;

import base.designPattern.structural.Proxy.Static.abs.ComputerOrz;

public class Asus implements ComputerOrz{
    @Override
    public void sell() {
        System.out.println("根据华硕出厂价8000卖电脑");
    }
}