/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 14:56:17
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 15:14:10
 * @ Description:
 */

package base.designPattern.structural.Decorator;

import base.designPattern.structural.Decorator.abs.ExtFood;
import base.designPattern.structural.Decorator.abs.FastFood;

public class Beef extends ExtFood{

    public Beef(FastFood fastFood) {
        super("加牛肉", 5, fastFood);
    }

    @Override
    public float cost() {
        return getPrice()+getFastFood().cost();
    }
    public String getName(){
        return getFastFood().getName()+super.getName();
    }
}
