/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 14:27:44
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 14:37:08
 * @ Description:
 */

package base.designPattern.structural.Decorator;

import base.designPattern.structural.Decorator.abs.FastFood;

public class Rice extends FastFood {

    public Rice() {
        super("炒饭", 10);
    }

    @Override
    public float cost() {
        return super.getPrice();
    }

}
