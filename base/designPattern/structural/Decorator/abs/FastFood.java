/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 14:23:57
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 14:34:13
 * @ Description:
 */


package base.designPattern.structural.Decorator.abs;
public abstract class FastFood{
    private String name;
    private float price;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public float getPrice() {
        return price;
    }
    public void setPrice(float price) {
        this.price = price;
    }
    public FastFood(String name,float price) {
        this.name=name;
        this.price=price;
    }
    public abstract float cost();
    
}