/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 14:37:27
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 14:44:11
 * @ Description:
 */

package base.designPattern.structural.Decorator.abs;

public abstract class ExtFood extends FastFood{
    private FastFood fastFood;
    
    
    public FastFood getFastFood() {
        return fastFood;
    }


    public void setFastFood(FastFood fastFood) {
        this.fastFood = fastFood;
    }


    public ExtFood(String name, float price, FastFood fastFood) {
        super(name, price);
        this.fastFood = fastFood;
    }


    @Override
    public abstract float cost();
    

}
