/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 14:30:16
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 16:23:41
 * @ Description:
 */

package base.designPattern.structural.Decorator;

public class test {
    public static void main(String[] args) {
        Rice rice=new Rice();
        System.out.println(rice.getName()+":" +rice.cost());
        Egg egg=new Egg(rice);
        System.out.println(egg.getName()+":" +egg.cost());
        Beef beef=new Beef(egg);
        System.out.println(beef.getName()+":" +beef.cost());
    }
}
