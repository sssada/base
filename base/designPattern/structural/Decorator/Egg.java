package base.designPattern.structural.Decorator;

import base.designPattern.structural.Decorator.abs.ExtFood;
import base.designPattern.structural.Decorator.abs.FastFood;

public class Egg extends ExtFood{

    public Egg( FastFood fastFood) {
        super("加鸡蛋",1,fastFood);
    }

    @Override
    public float cost() {
        // TODO Auto-generated method stub
        return getPrice()+getFastFood().cost();
    }
    @Override
    public String getName(){
      return getFastFood().getName()+super.getName();
    }
}
