/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:23:58
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 10:06:52
 * @ Description:
 */



package base.designPattern.structural.Adapter.classAdapter.abs;

public interface TFCard {
    String readTFMsg();

    void writeMsg(String msg);
}
