/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:23:16
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 09:48:04
 * @ Description:
 */

package base.designPattern.structural.Adapter.classAdapter.abs;

public interface SDCard {
    String readSDMsg();

    void writeMsg(String msg);
}
