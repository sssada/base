/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:49:34
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 09:57:08
 * @ Description:
 */

package base.designPattern.structural.Adapter.classAdapter;

import base.designPattern.structural.Adapter.classAdapter.abs.SDCard;

public class Computer {
    public String readMsg(SDCard sdCard) {
        System.out.println(sdCard.readSDMsg());
        return sdCard.readSDMsg();
    }

    public void writeMsg(SDCard sdCard, String msg) {
        sdCard.writeMsg(msg);
    }
}
