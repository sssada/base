/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:24:31
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 09:56:58
 * @ Description:
 */

package base.designPattern.structural.Adapter.classAdapter.imp;

import base.designPattern.structural.Adapter.classAdapter.abs.SDCard;

public class SDCardImp implements SDCard {

    @Override
    public String readSDMsg() {
        String msg = "从sd卡中读取数据";
        return msg;
    }

    @Override
    public void writeMsg(String msg) {
        System.out.println("往sd卡中写入数据:" + msg);
    }

}
