/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:26:45
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 09:48:27
 * @ Description:
 */

package base.designPattern.structural.Adapter.classAdapter.imp;

import base.designPattern.structural.Adapter.classAdapter.abs.TFCard;

public class TFCardImp implements TFCard{
    @Override
    public String readTFMsg() {
        String msg="从tf卡中读取数据";
        return msg;
    }

    @Override
    public void writeMsg(String msg) {
        System.out.println("往tf卡中写入数据:"+msg);
        
    }
}
