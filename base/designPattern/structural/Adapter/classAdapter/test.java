/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:51:33
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 09:57:04
 * @ Description:
 */

package base.designPattern.structural.Adapter.classAdapter;

import base.designPattern.structural.Adapter.classAdapter.abs.SDCard;
import base.designPattern.structural.Adapter.classAdapter.imp.SDCardImp;

public class test {
    public static void main(String[] args) {
        Computer computer = new Computer();
        Adapter adapter = new Adapter();
        SDCard sdCard = new SDCardImp();

        computer.readMsg(sdCard);
        computer.writeMsg(sdCard, "sd卡");
        System.out.println("============================================");
        computer.readMsg(adapter);
        computer.writeMsg(adapter, "适配器");
    }
}
