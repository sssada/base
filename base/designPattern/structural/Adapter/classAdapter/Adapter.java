/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:27:37
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 10:07:19
 * @ Description:
 */

package base.designPattern.structural.Adapter.classAdapter;

import base.designPattern.structural.Adapter.classAdapter.abs.SDCard;
import base.designPattern.structural.Adapter.classAdapter.imp.TFCardImp;

public class Adapter extends TFCardImp implements SDCard{

    @Override
    public void writeMsg(String msg) {
        super.writeMsg(msg);
    }

    @Override
    public String readSDMsg() {
        String msg=super.readTFMsg();
        return msg;
    }
    
}
