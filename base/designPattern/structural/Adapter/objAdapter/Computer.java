/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:49:34
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 10:11:01
 * @ Description:
 */

package base.designPattern.structural.Adapter.objAdapter;

import base.designPattern.structural.Adapter.objAdapter.abs.SDCard;

public class Computer {
    public String readMsg(SDCard sdCard) {
        System.out.println(sdCard.readSDMsg());
        return sdCard.readSDMsg();
    }

    public void writeMsg(SDCard sdCard, String msg) {
        sdCard.writeMsg(msg);
    }
}
