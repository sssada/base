/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:51:33
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 10:12:39
 * @ Description:
 */

package base.designPattern.structural.Adapter.objAdapter;

import base.designPattern.structural.Adapter.objAdapter.abs.TFCard;
import base.designPattern.structural.Adapter.objAdapter.imp.TFCardImp;
import base.designPattern.structural.Adapter.objAdapter.abs.SDCard;
import base.designPattern.structural.Adapter.objAdapter.imp.SDCardImp;

public class test {
    public static void main(String[] args) {
        Computer computer = new Computer();
        TFCard tfCard=new TFCardImp();
        Adapter adapter = new Adapter(tfCard);
        SDCard sdCard = new SDCardImp();

        computer.readMsg(sdCard);
        computer.writeMsg(sdCard, "sd卡");
        System.out.println("============================================");
        computer.readMsg(adapter);
        computer.writeMsg(adapter, "适配器");
    }
}
