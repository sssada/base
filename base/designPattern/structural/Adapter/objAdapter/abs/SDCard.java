/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:23:16
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 10:10:30
 * @ Description:
 */

package base.designPattern.structural.Adapter.objAdapter.abs;

public interface SDCard {
    String readSDMsg();

    void writeMsg(String msg);
}
