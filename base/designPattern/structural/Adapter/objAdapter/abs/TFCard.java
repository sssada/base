/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:23:58
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 10:10:33
 * @ Description:
 */



package base.designPattern.structural.Adapter.objAdapter.abs;

public interface TFCard {
    String readTFMsg();

    void writeMsg(String msg);
}
