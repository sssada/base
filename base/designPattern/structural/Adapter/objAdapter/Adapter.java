/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:27:37
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 10:12:08
 * @ Description:
 */

package base.designPattern.structural.Adapter.objAdapter;

import base.designPattern.structural.Adapter.objAdapter.abs.SDCard;
import base.designPattern.structural.Adapter.objAdapter.abs.TFCard;

public class Adapter implements SDCard {
    private TFCard tfCard;

    Adapter(TFCard tfCard) {
        this.tfCard = tfCard;
    }

    @Override
    public void writeMsg(String msg) {
        tfCard.writeMsg(msg);
    }

    @Override
    public String readSDMsg() {
        String msg = tfCard.readTFMsg();
        return msg;
    }

}
