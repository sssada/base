/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-21 09:24:31
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-21 10:10:48
 * @ Description:
 */

package base.designPattern.structural.Adapter.objAdapter.imp;

import base.designPattern.structural.Adapter.objAdapter.abs.SDCard;

public class SDCardImp implements SDCard {

    @Override
    public String readSDMsg() {
        String msg = "从sd卡中读取数据";
        return msg;
    }

    @Override
    public void writeMsg(String msg) {
        System.out.println("往sd卡中写入数据:" + msg);
    }

}
