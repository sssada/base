package base.designPattern.principle.SingleResponsibilityPrinciple;

public class BadDemo {
    public static void userType(String userType){
        if(userType.equals("游客")){
            System.out.println("请先登录");
        }else if(userType.equals("普通用户")){
            System.out.println("要看广告");
        }else{
            System.out.println("免广告");
        }
    }
}

