/**
 * @ Author: OldZhang
 * @ Create Time: 2022-05-27 11:18:27
 * @ Modified by: OldZhang
 * @ Modified time: 2022-05-27 11:19:04
 * @ Description:
 */

package base.designPattern.principle.SingleResponsibilityPrinciple.imp;

import base.designPattern.principle.SingleResponsibilityPrinciple.UserType;

public class VipUser implements UserType{

    @Override
    public void userTypeAction() {
        System.out.println("免广告");
    }
    
}
