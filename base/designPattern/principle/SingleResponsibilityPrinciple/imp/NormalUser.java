/**
 * @ Author: OldZhang
 * @ Create Time: 2022-05-27 11:17:23
 * @ Modified by: OldZhang
 * @ Modified time: 2022-05-27 11:22:49
 * @ Description:
 */

package base.designPattern.principle.SingleResponsibilityPrinciple.imp;

import base.designPattern.principle.SingleResponsibilityPrinciple.UserType;


public class NormalUser implements UserType{
    @Override
    public void userTypeAction() {
        System.out.println("要看广告");
        
    }
}
