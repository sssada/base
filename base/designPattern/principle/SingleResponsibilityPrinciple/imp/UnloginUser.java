/**
 * @ Author: OldZhang
 * @ Create Time: 2022-05-27 11:16:26
 * @ Modified by: OldZhang
 * @ Modified time: 2022-05-27 11:23:07
 * @ Description:
 */

package base.designPattern.principle.SingleResponsibilityPrinciple.imp;

import base.designPattern.principle.SingleResponsibilityPrinciple.UserType;

public class UnloginUser implements UserType{
    @Override
    public void userTypeAction() {
       System.out.println("请先登录");
        
    }
}
