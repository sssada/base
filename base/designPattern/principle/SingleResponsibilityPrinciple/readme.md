# 单一职责原则(Single responsibility principle)
SRP单一职责原则就是说，一个类他只负责一件事情。这样子就比较好维护，不然的话，一个类里面就会有很多逻辑（if/else）堆在一起，变成一个屎山类。  
比如对不同的**用户类型**，系统提供不同的功能  
[**错误示范**](BadDemo.java),这个代码把所有用户类型的功能逻辑都放在了一个类里面，然后使用if/else来判断。当之后要修改或者添加用户功能的时候，维护就会很复杂。违背了SRP原则
```
public class BadDemo {
    public void userType(String userType){
        if(userType.equals("游客")){
            System.out.println("请先登录");
        }else if(userType.equals("普通用户")){
            System.out.println("要看广告");
        }else{
            System.out.println("免广告");
        }
    }
}
```
[**正确示范**](UserType.java),我们首先定义了一个接口，用来表示用户的行为。然后使用不同的[实现类](imp/)来具体实现用户逻辑，这样之后需要新增一个用户类型就只需要多建一个实现类。修改某个用户的逻辑只需要修改对应的实现类就行了。
```
//UserType.java
public interface UserType {
    void userTypeAction();
}

//NormalUser
public class NormalUser implements UserType{
    @Override
    public void userTypeAction() {
        System.out.println("要看广告");
        
    }
}

//UnloginUser
public class UnloginUser implements UserType{
    @Override
    public void userTypeAction() {
       System.out.println("请先登录");
        
    }
}

//VipUser
public class VipUser implements UserType{
    @Override
    public void userTypeAction() {
        System.out.println("免广告");
    }
}

```

