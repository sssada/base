/**
 * @ Author: OldZhang
 * @ Create Time: 2022-05-27 11:15:33
 * @ Modified by: OldZhang
 * @ Modified time: 2022-05-27 11:17:42
 * @ Description:
 */

package base.designPattern.principle.SingleResponsibilityPrinciple;

public interface UserType {
    void userTypeAction();
}
