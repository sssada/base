/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-05 14:58:17
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-15 14:37:06
 * @ Description:
 */
package base.designPattern.principle.InterfaceSegregationPrinciple;

public interface Vehicle {
    void drive();// 驾驶

    void oiling(int money);// 加油
}