/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-02 23:44:27
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-02 23:59:11
 * @ Description:
 */

package base.designPattern.principle.OpenClosedPrinciple;

public interface Circular {
    static final double pai=3.14;
    public double getS(double r);
    public double getP(double r);
    public void roll();
}
