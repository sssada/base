package base.designPattern.principle.OpenClosedPrinciple;

public class test {
    public static void main(String[] args) {
        Circular c1=new CircularImp();
        Circular c2=new CircularExt();
        System.out.println(c1.getS(1)); 
        c1.roll();
        System.out.println(c2.getS(1));
        c2.roll();
    }
}
