/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-02 23:56:37
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-03 00:02:39
 * @ Description:
 */

package base.designPattern.principle.OpenClosedPrinciple;

public class CircularImp implements Circular {

    @Override
    public double getS(double r) {
        return r * r * pai;
    }

    @Override
    public double getP(double r) {
        return 2 * pai * r;
    }

    @Override
    public void roll() {
        System.out.println("我会滚");

    }

}
