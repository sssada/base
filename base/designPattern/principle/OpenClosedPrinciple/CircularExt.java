/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-02 23:52:47
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-03 00:03:17
 * @ Description:
 */

package base.designPattern.principle.OpenClosedPrinciple;

public class CircularExt extends CircularImp {
    private static final double pai = 3.1415926;

    @Override
    public double getP(double r) {
        return 2 * pai * r;
    }

    @Override
    public double getS(double r) {
        return r * r * pai;
    }

}
