# 开闭原则（OpenClosedPrinciple）
开闭原则就是对扩展开放对修改关闭，可能听起来有点抽象我们举个例子来看。  
比如说你定义了一个接口**Circular**来计算圆的面积和周长，那么你肯定要在接口里面定义一个静态变量**π**。，大家都是用同一个实现类**CircularImp**。
## 对修改关闭
大家用这个**CircularImp**类的用的很舒服，但是突然有一天有个人发现**π=3.14**不够精确，他想要让**π=3.1415926**。如果他直接修改接口**Circular**里面的值，那么所有用这个类的人都会收到影响，其他人并不想要这么精确的**π**。所以一般来说不会直接对这个接口进行修改，而是对他进行扩展。这就是对修改关闭！
```
public interface Circular {
    static final double pai=3.14;
    public double getS(double r);
    public double getP(double r);
    public void roll();
}

public class CircularImp implements Circular {

    @Override
    public double getS(double r) {
        return r * r * pai;
    }

    @Override
    public double getP(double r) {
        return 2 * pai * r;
    }

    @Override
    public void roll() {
        System.out.println("我会滚");

    }

}
```
## 对扩展开放
既然想要用到精确的**π**，又不能直接修改原来的类（因为这个例子比较简单所以直接在原来的类那里加个传参就行，但是复杂的情况下就还是要扩展）。因此我们就new一个新的类来继承原来的类就好了。然后重写两个需要**π**的方法，**roll**就不用重写可以保留。
```
public class CircularExt extends CircularImp {
    private static final double pai = 3.1415926;

    @Override
    public double getP(double r) {
        return 2 * pai * r;
    }

    @Override
    public double getS(double r) {
        return r * r * pai;
    }

}
```
## 测试
```
public class test {
    public static void main(String[] args) {
        Circular c1=new CircularImp();
        Circular c2=new CircularExt();
        System.out.println(c1.getS(1)); 
        c1.roll();
        System.out.println(c2.getS(1));
        c2.roll();
    }
}
```
输出：  
3.14  
我会滚  
3.1415926  
我会滚  
