/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-04 21:41:02
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-04 21:48:47
 * @ Description:
 */

package base.designPattern.principle.LawOfDemeter;

import java.util.List;

public class SellLeader {
    private SellDepartment sellDepartment;

    public SellDepartment getSellDepartment() {
        return sellDepartment;
    }

    public void setSellDepartment(SellDepartment sellDepartment) {
        this.sellDepartment = sellDepartment;
    }

    public int getSellNum() {
        List<Integer> sellNum = sellDepartment.getSellNumber();
        int res = 0;
        for (Integer integer : sellNum) {
            res += integer;
        }
        return res;
    }

}
