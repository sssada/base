# 迪米特原则
类于类之间要低耦合，也就是说要减少每个类之间的相互了解。让系统的功能模块更加的独立，相互之间没有或者尽量少的依赖。  
强调只与你的直接朋友交谈，不跟“陌生人”说话。在软件设计中，如果两个实体无须直接通信，那么就不应当发生直接的相互调用，可以通过第三方转发该调用。从而降低类间的耦合，增强类的独立性。
```
public class SellDepartment {
    private List<Integer> sellNumber;
    private List<String> employee;

    public List<Integer> getSellNumber() {
        return sellNumber;
    }

    public void setSellNumber(List<Integer> sellNumber) {
        this.sellNumber = sellNumber;
    }

    public List<String> getEmployee() {
        return employee;
    }

    public void setEmployee(List<String> employee) {
        this.employee = employee;
    }
}
public class SellLeader {
    private SellDepartment sellDepartment;

    public SellDepartment getSellDepartment() {
        return sellDepartment;
    }

    public void setSellDepartment(SellDepartment sellDepartment) {
        this.sellDepartment = sellDepartment;
    }

    public int getSellNum() {
        List<Integer> sellNum = sellDepartment.getSellNumber();
        int res = 0;
        for (Integer integer : sellNum) {
            res += integer;
        }
        return res;
    }

}
public class Boss {
    private SellLeader sellLeader;

    public SellLeader getSellLeader() {
        return sellLeader;
    }

    public void setSellLeader(SellLeader sellLeader) {
        this.sellLeader = sellLeader;
    }

    public void getSellNum() {
        System.out.println(sellLeader.getSellNum());
    }
}

```
  
