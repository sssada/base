/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-04 21:46:24
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-04 21:55:51
 * @ Description:
 */

package base.designPattern.principle.LawOfDemeter;

public class Boss {
    private SellLeader sellLeader;

    public SellLeader getSellLeader() {
        return sellLeader;
    }

    public void setSellLeader(SellLeader sellLeader) {
        this.sellLeader = sellLeader;
    }

    public void getSellNum() {
        System.out.println(sellLeader.getSellNum());
    }
}
