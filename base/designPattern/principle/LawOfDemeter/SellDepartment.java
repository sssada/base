/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-04 21:36:21
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-04 21:49:17
 * @ Description:
 */

package base.designPattern.principle.LawOfDemeter;

import java.util.List;

public class SellDepartment {
    private List<Integer> sellNumber;
    private List<String> employee;

    public List<Integer> getSellNumber() {
        return sellNumber;
    }

    public void setSellNumber(List<Integer> sellNumber) {
        this.sellNumber = sellNumber;
    }

    public List<String> getEmployee() {
        return employee;
    }

    public void setEmployee(List<String> employee) {
        this.employee = employee;
    }

}