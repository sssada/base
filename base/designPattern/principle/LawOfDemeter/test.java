/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-04 21:49:22
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-05 15:31:01
 * @ Description:
 */



package base.designPattern.principle.LawOfDemeter;

import java.util.ArrayList;
import java.util.List;



public class test {
    public static void main(String[] args) {

        List<Integer> nums = new ArrayList<>();
        nums.add(1);
        nums.add(2);
        nums.add(5);

        SellDepartment sellDepartment = new SellDepartment();
        sellDepartment.setSellNumber(nums);
        SellLeader sellLeader = new SellLeader();
        sellLeader.setSellDepartment(sellDepartment);

        Boss boss = new Boss();
        boss.setSellLeader(sellLeader);

        boss.getSellNum();
    }

}
