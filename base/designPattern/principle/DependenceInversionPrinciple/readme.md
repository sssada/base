# 依赖倒置原则（DIP）
依赖导致原则的核心就是**面向接口编程**，高层模块不应该依赖低层模块，二者都应该依赖其抽象。使用接囗或抽象类的目的是制定好规范，而不涉及任何具体的搡作，把展现细节的任务交给他们的实现类去完成。
## 错误举例
假设现在有一个大厨类``Cook``,他会做番茄``Tomato``和土豆``Potato``这两道菜
```
public class Cook {
    public void cookTometo(Tomato tomato) {
        tomato.cook();
    }
    public void cookPotato(Potato potato){
        potato.cook();
    }
}

public class Tomato {
    public void cook(){
        System.out.println("做番茄");
    }
}

public class Potato {
    public void cook(){
        System.out.println("做土豆");
    }
}
```
如果大厨又学会了一道新菜，哪个就需要在``Cook``类中增加新的方法，代码的耦合度较高。
## 正确示范
首先我们把厨师要做的菜抽象成``Food``接口，``Tomato``和``Patato``来实现``Food``，厨师``Cook``只负责烹饪``Food``，不管他具体是什么菜。具体的烹饪过程有``Food``的实现类来完成。
```
public interface Food {
    void cook();
}
public class Tomato implements Food{
    public void cook(){
        System.out.println("做番茄");
    }
}
public class Potato implements Food{
    public void cook(){
        System.out.println("做土豆");
    }
}
public class Cook {
    public void cook(Food food) {
        food.cook();
    }
}
```
这样之后厨师又要做什么新菜了只需要在多一个实现类就行了