/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-05 15:38:53
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-05 15:45:03
 * @ Description:
 */

package base.designPattern.principle.DependenceInversionPrinciple;

public class Potato implements Food{
    public void cook(){
        System.out.println("做土豆");
    }
}
