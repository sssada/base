/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-05 15:44:46
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-05 15:44:57
 * @ Description:
 */

package base.designPattern.principle.DependenceInversionPrinciple;

public interface Food {
    void cook();
}
