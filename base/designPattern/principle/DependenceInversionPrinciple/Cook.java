/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-05 15:35:11
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-05 15:46:48
 * @ Description:
 */

package base.designPattern.principle.DependenceInversionPrinciple;

public class Cook {
    public void cook(Food food) {
        food.cook();
    }
}
