/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-05 15:37:21
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-05 15:45:44
 * @ Description:
 */

package base.designPattern.principle.DependenceInversionPrinciple;

public class Tomato implements Food{
    public void cook(){
        System.out.println("做番茄");
    }
}
