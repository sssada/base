/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-05 15:37:21
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-05 15:38:12
 * @ Description:
 */

package base.designPattern.principle.DependenceInversionPrinciple.wrong;

public class Tomato {
    public void cook(){
        System.out.println("做番茄");
    }
}
