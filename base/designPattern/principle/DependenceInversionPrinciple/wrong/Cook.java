/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-05 15:35:11
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-05 15:39:33
 * @ Description:
 */

package base.designPattern.principle.DependenceInversionPrinciple.wrong;

public class Cook {
    public void cookTometo(Tomato tomato) {
        tomato.cook();
    }
    public void cookPotato(Potato potato){
        potato.cook();
    }
}
