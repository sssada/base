/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-05 15:38:53
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-05 15:39:12
 * @ Description:
 */

package base.designPattern.principle.DependenceInversionPrinciple.wrong;

public class Potato {
    public void cook(){
        System.out.println("做土豆");
    }
}
