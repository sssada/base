/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-03 20:39:32
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-04 16:24:05
 * @ Description:
 */

package base.designPattern.principle.LiserReplacementPrinciple;

public class Bike extends Vehicle{
    public void use(){
        System.out.println("脚蹬");
    }
}
