/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-03 20:35:47
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-03 20:39:23
 * @ Description:机动车类
 */

package base.designPattern.principle.LiserReplacementPrinciple;

public class Vehicle {
    public void drive(){
        System.out.println("行驶");
    }
}
