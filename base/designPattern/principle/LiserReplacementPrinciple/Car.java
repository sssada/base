/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-03 20:38:18
 * @ Modified by: OldZhang
 * @ Modified time: 2022-06-04 16:23:42
 * @ Description:
 */

package base.designPattern.principle.LiserReplacementPrinciple;

public class Car extends Vehicle{
    public void use(){
        System.out.println("用油");
    }
}
