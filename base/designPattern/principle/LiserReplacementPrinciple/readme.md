# 里氏替换原则(LSP)
简单的说里氏替换原则就一句话，就是**在任何地方把一个父类替换成他的子类，代码都可以正常的运行**，也就是**继承的时候要保证超类的特性在子类中依然成立**。  
里氏替换原则主要是为了防止我们在代码里面乱继承，然后把代码搞得很乱，变得乱七八糟的。  
比如 交通工具类`Vehicle`,汽车类``Car``,自行车类``Bike``,很明显我们不能让``Bike``继承``Car``
```
public class Vehicle {
    public void drive(){
        System.out.println("行驶");
    }
}

public class Car extends Vehicle{
    public void use(){
        System.out.println("用油");
    }
}
public class Bike extends Vehicle{
    public void use(){
        System.out.println("脚蹬");
    }
}
```


