package base.clone;

import java.io.Serializable;

class Mao implements Cloneable,Serializable{
    private String changduan;
    private String color;
    public Mao(String changduan, String color) {
        this.changduan = changduan;
        this.color = color;
    }
    @Override
    public String toString() {
        return "Mao [changduan=" + changduan + ", color=" + color + "]";
    }
    public String getChangduan() {
        return changduan;
    }
    public void setChangduan(String changduan) {
        this.changduan = changduan;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    @Override
    public Mao clone()  {
        
        try {
            return (Mao)super.clone();
        } catch (CloneNotSupportedException e) {
            
            e.printStackTrace();
            return null;
        }
    }
    
    
}
class Dog implements Cloneable,Serializable {
    private String name;
    private int age;
    private String color;
    private Mao mao=new Mao("long","black");
    public Dog(String name, int age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }
    
    public Dog() {

    }

    

    public Mao getMao() {
        return mao;
    }

    public void setMao(Mao mao) {
        this.mao = mao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Dog [age=" + age + ", color=" + color + ", mao=" + mao + ", name=" + name + "]";
    }

    /**
     * 重写clone方法
     */
    @Override
    public Dog clone() {

        try {
            //深拷贝
            Dog res=(Dog)super.clone();
            res.mao=mao.clone();
            return res;
            //半深半浅
            // return (Dog)super.clone();
        } catch (CloneNotSupportedException e) {
           
            e.printStackTrace();
            return null;
        }
    }

}