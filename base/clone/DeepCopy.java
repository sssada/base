package base.clone;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.jupiter.api.Test;

public class DeepCopy {
    private Dog d1 = new Dog("d1", 1, "green");

    public static void main(String[] args) {

    }

    /**
     * 直接new一个新对象然后把老对象的值set进去
     * 但是对象复杂的时候很麻烦
     */
    @Test
    public void newObj() {
        Dog d2 = new Dog(d1.getName(), d1.getAge(), d1.getColor());
        d2.setAge(88);
        System.out.println("d1:" + d1);
        System.out.println("d2:" + d2);
    }

    /**
     * 这里我进行了处理实现了深拷贝
     * 注意这个类得实现Cloneable接口
     */
    @Test
    public void deepClone() {
        Mao mao = new Mao("long", "black");
        d1.setMao(mao);
        Dog d3 = d1.clone();
        d3.setName("d3");
        mao.setChangduan("short");
        mao.setColor("red");
        System.out.println("d1:" + d1);
        System.out.println("d3:" + d3);
    }

    /**
     * 使用序列化和反序列化进行深拷贝
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Test
    public void serDeepCopy() throws IOException, ClassNotFoundException {
        Mao mao=new Mao("short", "red");
        d1.setMao(mao);
        //先把序列化数据放到内存操作流buf中
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        ObjectOutputStream obs = new ObjectOutputStream(buf);
        obs.writeObject(d1);
        obs.close();
        //从buf流中读取数据 并反序列化
        ByteArrayInputStream ios = new ByteArrayInputStream(buf.toByteArray());
        ObjectInputStream ois=new ObjectInputStream(ios);
        Dog d4=(Dog)ois.readObject();
        // d4.getMao().setColor("black");
        d4.setName("d4");
        mao.setColor("black");
        System.out.println("d1:" + d1);
        System.out.println("d4:" + d4);

    }

}
