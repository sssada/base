package base.clone;

import org.junit.Test;



public class ShallowCopy {
    private Dog d1 = new Dog("d1", 1, "green");

    public static void main(String[] args) {

    }

    /**
     * 直接赋值是浅拷贝
     */
    @Test
    public void assign() {
        Dog d2 = d1;
        d2.setName("d2");
        // 可以看到修改了d2后，d1也改变了
        System.out.println("d1 name:" + d1.getName());
        System.out.println("d2 name:" + d2.getName());

    }
    /**
     * clone 算是半深半浅吧
     * 通过重写clone方法来实现拷贝(前提是里面没有自定义的类[除非你自定义的类里面也重写了clone])
     * 注意这个类得实现Cloneable接口
     */
    @Test
    public void clone_() {
        Dog d1 = new Dog("d1", 1, "green");
        Mao mao =new Mao("long","black");
        d1.setMao(mao);
        Dog d3 = d1.clone();
        d3.setAge(2);
        System.out.println("d1:的年龄" + d1.getAge());
        System.out.println("d3:的年龄" + d3.getAge());
        System.out.println("-----------------对基本类型和基本类型的包装类（有final不可变）生效--------------------");
        mao.setChangduan("short");
        mao.setColor("red");
        System.out.println("d1的Mao属性" + d1.getMao()+"d1:的年龄" + d1.getAge());
        System.out.println("d3的Mao属性" + d3.getMao()+"d3:的年龄" + d3.getAge());
        System.out.println("-----------------对对象中的引用类型不生效（除非你在clone函数中对那个引用类型也进行clone）--------------------");
    }

   

}
