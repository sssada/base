# 基础
## TCP/IP的五层网络模型
![在这里插入图片描述](https://img-blog.csdnimg.cn/f9ce9d5664914dde8cd35790ee6f6fd6.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAT2xkWmhhbmdZSA==,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)  
不同设备上的进程间通信，就需要⽹络通信，⽽设备是多样性的，所以要兼容多种多样的设备，就协商出了⼀套通⽤的⽹络协议。  
这个⽹络协议是分层的，每⼀层都有各⾃的作⽤和职责，接下来就分别对每⼀层进⾏介绍。  
**应用层**  
应用层是5层模型中的最上层，也是我们最常接触的层。各种进程和App都是运行在这一层上的。应用层不直接传输数据，而是吧数据发送给下一层传输层，由传输层来进行数据的传输。应用层只关心如何给用户提供应用服务，并不关心数据是怎么传输的。  
**应用层是⼯作在操作系统中的用户态，传输层及以下则⼯作在内核态。**
传输层
应用层的数据包会发送给传输层，传输层为应用层提供数据传输的服务。（这个传输实际是是传输到网络层，或者由一个应用传输到另一个应用，而不是另一台设备） 
在传输层中有两个协议**TCP**和**UDP**
* TCP的全称是传输层控制协议，绝大多数的应用都是使用的TCP协议。比如应用层的http协议。TCP是面对流的协议，提供可靠的面向连接的运输服务，是点对点通信。（下载等应用场景）。相比于UDP多了很多功能：⽐如流量控制、超时重传、拥塞控制等，这些都是为了保证数据包能可靠地传输给对⽅。
* UDP是面向报文的协议，不提供可靠交付，并且不需要连接，不仅仅支持点对点而且支持多播和广播。（**群视频聊天、语音等**）UDP很简单，简单到只负责发送数据包，不保证数据包是否能抵达对⽅，但它实时性相对更好，传输效率也⾼。  
应用需要传输的数据可能会⾮常⼤，如果直接传输就不好控制，因此当传输层的数据包⼤⼩超过MSS（Maximum Segment Size，TCP 最⼤报⽂段⻓度），就要将数据包分块，这样即使中途有⼀个分块丢失或损坏了，只需要重新发送这⼀个分块，⽽不⽤重新
发送整个数据包。在TCP协议中，我们把每个分块称为⼀个 TCP 段（TCP Segment）。  

<img src="img/传输层TCP段.png">  

当设备作为接收方的时候，传输层就需要把数据转发给应用层。但是应用层中可能会有多个应用在传输数据，因此需要使用端口来区分不同的应用。比如80端口一般是Web服务用的，22是ssh远程登录的时候用的。  
端口号被携带在传输层的报文中
1. 网络层  
网络层负责实际上的不同设备间的数据传输功能。  

<img src="img/网络层.png">  

⽹络层最常使⽤的是IP协议（Internet Protocol），IP协议会将传输层的报⽂作为数据部分，再加上IP包头组装成IP报⽂，如果IP报⽂⼤⼩超过 MTU（以太⽹中⼀般为 1500 字节）就会再次进⾏分⽚，得到⼀个即将发送到⽹络的IP报⽂。  

<img src="img/网络层数据.png">    

IP协议有两个作用：
* 寻址  
网络层通过IP地址来找到对应的接收设备，IP地址32个位，分为4段，每个段8位。为了寻址的方便，IP地址有两种意义：
* 网络号：表示了这个IP地址是属于哪个子网的
* 主机号：表示同一个子网下的具体哪台设备
在寻址的过程中会配合子网掩码来计算出网络号和主机号。在匹配到网络号之后才会匹配主机号。


* 路由  
在实际场景中两台设备不是通过一条网线直接连起来的，中间还有很多其他的设备，比如网关、路由器、交换机等。所以从一台设备到另一台设备的路径有很多，因此当数据到达一个网络结点的时候就需要路由算法来决定下一步该往哪里走。

> 总的来说：寻址的作用就是告诉你目的地在哪里，路由的作用就是告诉你应该怎么去这个目的地。
### 数据链路层
实际场景中，⽹络并不是⼀个整体，⽐如你家和我家就不属于⼀个⽹络，所以数据不仅可以在同⼀个⽹络中设备间进⾏传输，也可以跨⽹络进⾏传输。⼀旦数据需要跨⽹络传输，就需要有⼀个设备同时在两个⽹络当中，这个设备⼀般是路由器，路由器可以通过路由
表计算出下⼀个要去的IP地址。然后通过ARP协议找到该⽬的地的MAC地址，而每⼀台设备的⽹卡都会有⼀个MAC地址，它就是⽤来唯⼀标识设备的。在数据链路中传递的数据叫做帧，每帧包括**数据**和**控制信息**，我们能通过控制信息判断帧数据是否出错，出错了就丢掉。**交换机、网格**运行在这一层。
### 物理层
物理层提供一个底层物理硬件的通信。通过2进制比特流来实现，2进制比特流使用物理硬件的高低电压来体现。

## OSI七层模型
![在这里插入图片描述](https://img-blog.csdnimg.cn/f9ce9d5664914dde8cd35790ee6f6fd6.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAT2xkWmhhbmdZSA==,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)
OSI协议全称开放式通信系统互连参考模型，总共7层。每层都为其相邻层服务，但是他们各自实现自己的服务，并不关心相邻层是如何实现的。
1. 物理层  
物理层是OSI7层协议的最底层，物理层提供一个底层物理硬件的通信。通过2进制比特流来实现，2进制比特流使用物理硬件的高低电压来体现。设备A从数据链路层获取流形式数据后转化为比特流发送给设备B,设备B接收后将数据转化为二进制机器码给数据链路层。**网卡、集线器、中继器**工作子物理层。
2. 数据链路层  
数据链路层是OSI7层协议的第2层，链路层中传递的数据称为**帧**。链路层将从物理层获得的数据封装为帧，转发给上一层网络层。也将来自网络层的数据拆分成位流的形式发送给物理层。每帧包括**数据**和**控制信息**，我们能通过控制信息判断帧数据是否出错，出错了就丢掉。**交换机、网格**运行在这一层。
3. 网络层  
网络层是OSI7层协议的第3层，也叫IP层，两台主机通信的链路不止一条，每条链路之间使用的协议也可以不同。网络层将**网络地址翻译为物理地址**并选择合适的**网间路由和交换节点**来保证数据的传递。在发送数据时，网络层把传输层产生的报文或用户数据报封装成**分组**和**包**向下传输到数据链路层。**路由器**工作在这一层。网络层使用IP协议和许多路由协议
4. 传输层  
传输层是OSI7层协议的第4层，传输层实现了两台主机进程之间的通信（使用**TCP**或**UDP**协议）。进程使用传输层的服务传送应用层报文。传输层有复用和分用的功能。复用就是指多个应用层进程可同时使用下面传输层的服务。分用，是传输层把收到的信息分别交付上面应用层中的相应进程。
5. 会话层  
会话层是OSI7层协议的第5层，**会话层就是负责建立、管理和终止表示层实体之间的通信会话。**（淘宝不能用微信支付就是因为淘宝和微信之间没有建立会话）
6. 表示层（翻译官）  
表示层是OSI7层协议的第6层，**表示层让通信的应用程序能够解释交换数据的含义**。表示层对应用层的数据进行数据编译。将会话层的数据编译成如视频、图片等传递给应用层。将应用层的报文压缩，加密等传递给会话层。
7. 应用层  
应用层是OSI7层协议的顶层，定义了应用进程之间的交互规则，提供了应用层协议，通过不同的协议为不同的应用提供服务。如域名系统DNS，万维网应用http协议，STMP协议，FTP协议等，方便应用程序之间进行通信。


# HTTP
Http:超文本传输协议（Hyper Text Transfer Protocol，HTTP）超⽂本传输协议。可以拆分成三部分来理解：
* 超文本
* 传输
* 协议
1. 超文本  
http传输的内容是超文本，超文本是超越了普通⽂本的⽂本，它是⽂字、图⽚、视频等的混合体，最关键有超链接，能从
⼀个超⽂本跳转到另外⼀个超⽂本。最常见的超文本就是Html了，他本身只是普通的文本，但是经过浏览器的解释编译就变成一个有视频、图片的网页了。
2. 传输  
传输就是把数据从A点转移到B点，或者从B点转移到A点。数据虽然是在A和B之间传输，但允许中间有中转或接⼒。在HTTP⾥，只要中间⼈遵从HTTP协议，不打扰基本的数据传输，就可以添加任意额外的东⻄。
3. 协议  
协议就是规定了一个双方都要遵守的一种规范。  

**因此总的来说HTTP就是⼀个在计算机世界⾥专门在两点之间传输文字、图⽚、⾳频、视频等超⽂本数据的约定和规范**

## HTTP常见的状态码有哪些
|状态号|含义|具体|
|--|--|--|
|1xx|提示信息，表示是协议处理的中间阶段还需要后续的处理||
|2xx|表示成功，已经收到请求并且被正确的处理了|200,204,206|
|3xx|重定向，表示资源位置发生了变动，要客户端重新发送请求|301,302,304|
|4xx|客户端错误，发送的请求有问题，服务端无法处理|400,403,404|
|5xx|服务端错误，服务器在处理请求的时候发生了内部错误|500,501,502,503|

1. **2xx** 
* 200：一切正常
* 204：一切正常但是返回的数据里面没有body数据
* 206：表示响应返回的body数据并不是资源的全部，⽽是其中的⼀部分，也是服务器处理成功的状态。
2. **3xx**  
* 301：永久重定向，表示资源已经不存在了，要用新的url访问了。
* 302：暂时重定向，表示资源还在但是临时需要一个新的url访问
* 304：缓存重定向，表示资源已经在缓存里面了。
3. **4xx**  
* 400：笼统的客户端错误
* 403：权限不够，服务器拒绝访问
* 404：要访问的资源不存在
4. **5xx**
* 500：笼统的服务器错误
* 501：表示客户端请求的功能还不支持
* 502：通常是服务器为代理或者网关时的错误，服务器本身没发生错误，但是在调用后端服务器的时候发生了错误。
* 503：表示服务器忙，没有时间来响应这个请求。
## HTTP的常见字段
* Host:客户端发送请求时，⽤来指定服务器的域名。可以用Host来指定同一台服务器上的不同网站。
* Connection：最常⽤于客户端要求服务器使⽤TCP持久连接，以便其他请求复⽤。设置成keep-alive就是持久连接
* Content-Length：返回的数据的长度。
* Content-Type：返回数据的格式，``text/html; charset=utf-8``
* Content-Encoding：数据的压缩⽅法。表示服务器返回的数据使⽤了什么压缩格式。
## HTTP的优缺点
* 优点  
1. 灵活和易于扩展  
HTTP工作在应用层，他的下层可以随意的更换。HTTPS就是在HTTP和TCP之间加了一层SSL/TLS，还没发布的HTTP3甚至把TCP层换成了UDP。
2. 应用广泛和跨平台  
HTTP的应用非常广泛，基本上网络上所有数据的传输都用到了HTTP协议。且HTTP协议可以跨平台。
* 缺点
1. 无状态  
HTTP协议是没有状态的，服务器不会去记录HTTP的状态，因此在进行一些有关联性的操作时会非常的麻烦。因为服务器没有记录HTTP的状态，所以每次都需要重新的查询一遍信息。比如我要把一个商品加入购物车，然后结算购买。那么在加入购物车时，服务器会查询一遍用户信息，在结账的时候服务器也会查询一遍用户信息。
2. 不安全  
* 通信使⽤明⽂（不加密），内容可能会被窃听。⽐如，账号信息容易泄漏，那你号没了。
* 不验证通信⽅的身份，因此有可能遭遇伪装。⽐如，访问假的淘宝、拼多多，那你钱没了。
* ⽆法证明报⽂的完整性，所以有可能已遭篡改。⽐如，⽹⻚上植⼊垃圾⼴告，视觉污染，眼没了
## HTTP1.0
在HTTP1.0，HTTP是非持续性连接。浏览器的每次请求都需要与服务器建立一个TCP连接，服务器完成请求处理后立即断开TCP连接，连接无法复用。这就浪费很多时间在，建立TCP的连接上了。
## HTTP1.1
相对于HTTP1.0
* HTTP 1.1支持持久连接，在一个TCP连接上可以传送多个HTTP请求和响应，减少了建立和关闭连接的消耗和延迟。
* ⽀持管道（pipeline）⽹络传输客户端不用等待上一次请求结果返回，就可以发出下一次请求.（但是服务端还是按照客户端请求的顺序来返回数据）
	* 队头堵塞：因为当顺序发送的请求序列中的⼀个请求因为某种原因被阻塞时，在后⾯排队的所有请求也⼀同被阻塞了，会导致客户端⼀直请求不到数据，这也就是「队头阻塞」。好⽐上班的路上塞⻋。
## HTTP2.0
1. **二进制分帧**  
HTTP2.0全⾯采⽤了⼆进制格式，头信息和数据体都是⼆进制，并且统称为帧（frame）：头信息帧和数据帧接收端收到报⽂后，⽆需再将明⽂的报⽂转成⼆进制，⽽是直接解析⼆进制报⽂，这增加了数据传输的效率。  
   <img src="img/二进制帧.png">

2. **数据流**  
http2的数据并不是按照顺序发送的，每个连接上连续的两个数据可能属于不同的回应或者请求。因此要对数据包做标记来表示他是属于哪个回应或者请求的。  
每个回应或者请求的所有数据包，称为一个数据流Stream。每个数据流都有独一无二的编号，其中规定客户端发出的数据流编号为奇数，服务端发出的数据流编号为偶数。客户端还可以指定数据流的优先级，优先级高的，服务端会优先相应。

3. **多路复用**  
HTTP/2可以在⼀个连接中处理多个请求或回应，⽽不⽤按照顺序⼀⼀对应。由于不用排队等候，也就不会出现队头阻塞的事件。降低了延迟提高了连接的利用率。  
比如有A和B两个请求，A处理起来很耗时，服务器就会先把B返回了之后再返回A。这两个回应用的是同一个TCP连接。
4. **服务器推送**：可以在发送页面HTML时主动推送其它资源，而不用等到浏览器解析到相应位置，发起请求再响应。比如说服务端可以主动把JS和CSS文件推送给客户端，而不需要客户端解析HTML时再发送这些请求。当然客户端也可以拒绝。
5. **头部压缩**：HTTP 1.1请求的大小变得越来越大，**HTTP2.0通过只发送消息头中变化的部分来压缩消息头的大小**。HTTP/2在客户端和服务器端使用“首部表”来跟踪和存储之前发送的键－值对，对于相同的数据，不再通过每次请求和响应发送；首部表在HTTP/2的连接存续期内始终存在，由客户端和服务器共同渐进地更新;每个新的首部键－值对要么被追加到当前表的末尾，要么替换表中之前的值。
![](img/http1.1_s_2.png)
### HTTP2的缺陷
HTTP2中，多个HTTP请求复用一个TCP连接，当其中有一个HTTP请求出现了丢包的情况就会触发TCP的重传机制，这个TCP连接上所有的HTTP都要等待这个包被重传回来。
## HTTPS
HTTPS：是以安全为目标的HTTP通道，简单讲是HTTP的安全版，即HTTP下加入SSL/TLS层，HTTPS的安全基础是SSL/TLS，因此加密的详细内容就需要SSL/TLS。
HTTPS协议的主要作用可以分为两种：一种是建立一个信息安全通道，来保证数据传输的安全；另一种就是确认网站的真实性。
### 对称加密和不对称加密
**对称加密**：只使用一个密钥就可以进行加密解密，使用密钥A进行加密后也可以使用密钥A进行解密。常见的有RC算法、**IDEA**算法等  
**非对称加密**：加密和解密的密钥是不同的。两个密钥成对出现，使用密钥A加密后只能使用密钥B才能解密。常见的有**RSA**，**ECC**。

### SSL/TLS加密方式
![在这里插入图片描述](https://img-blog.csdnimg.cn/f9113447d1374ff1b4c664bda7e9d011.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAT2xkWmhhbmdZSA==,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)
1. ClientHello：客户端发送所支持的 **SSL/TLS 版本号**、**所支持的加密算法集合**和随机数**r1**等信息给服务器端。
2. ServerHello：服务器选择双方都支持的**加密算法集合**、**SSL/TLS 版本号**和一个随机数**r2**返回给客户端
3. Certificate：服务端发送CA证书给客户端
4. ServerKeyExchange：发送公钥给客户端
5. ServerHelloDone：服务器返回结束
6. 客户端生成随机数**r3（注意这个随机数在客户端生成后，没有被明文发送过所以外人并不知道r3具体的值）**，使用公钥加密**r3**生成**preMaster**。客户端使用公钥加密**r1+r2+r3**生成**会话密钥**
7. ClientKeyExchange：客户端发送**preMaster**给服务器
8. 服务端接收到**preMaster**后使用私钥解密获得**r3**，使用公钥加密**r1+r2+r3**生成**会话密钥**
9. ChangeCipherSpec：（互相发）通知对方，自己切换到了加密模式
10. Encrypted Handshake Message：开始加密通话。之后的通讯都是用**会话密钥**来进行对称加密 
### 为什么要有三个随机数
1. 计算机产生的随机数都是伪随机，一个伪随机可能不那么随机，可是三个伪随机就十分接近随机了。所以最终使用了三个随机数。
2. 因为客户端时不可信的，服务端不能相信客户端给出的是否是个随机数，还是个固定值又或者是有什么规律。。所以在生成会话密钥的时候一定要往里面加一个服务器自己生成的随机数，因此就有r2。但是r2是明文发送的，所以需要客户端加上后续的加密过的r3。
## HTTP和HTTPS的区别
1. HTTP传输的数据都是明文的，任何人可见。HTTPS传输的数据是经过SSL/TLS加密的，安全性更高
2. HTTPS需要使用CA证书（表明域名是谁的，日期什么的还包括了公钥私钥）。
3. HTTP默认端口80，HTTPS默认端口443
4. http的连接很简单，是无状态的（服务器是个渣男，他不会记得某个客户端曾经访问过他）；HTTPS协议是由SSL/TLS+HTTP协议构建的可进行加密传输、身份认证的网络协议，比HTTP协议安全。

## POST和GET
1. 一般来说**get**用于**获取、检索**，**post**用于**创建、更新**
2. **get**会将请求的参数附加到Url中，而**post**不会在URL中附加参数，而是将参数放在request body中。所以**get**比**post**更不安全（**post**也不安全，因为HTTP本身是明文协议。每个HTTP请求和返回的所有数据都会在网络上明文传播，不管是url，header还是body）
3. 使用**get**请求时，浏览器会把http header和data一并发送出去。而使用**post**请求时，服务器会先发送header，等待服务器响应后在发送具体data。
4. **get**请求在URL中传送的参数是有长度限制的（因为url的长度是由限制的，谷歌浏览器限制为2mb），而POST没有。
5. **get**请求只能进行url编码，而**post**支持多种编码方式
6. get是安全且幂等的，post是不安全且不幂等的。（这里的安全指的是，是否会破坏服务器上的内容。幂等指的是执行多次相同的操作，结果也是相同的）
7. 总的来说，**get**和**post**甚至可以说是一个东西，你可以在**get**请求中附带request body，也可以在**post**请求的url中加上参数。
## Cookie和Session
Http是**无状态**的，也就是说服务器并不会知道这个发送http请求的客户端是谁。（服务器是个渣男，相应完之后就把客户端忘了，客户端下一次再来就和新人一样。）**Cookie**和**Session**就是为了让服务器记住客户端而诞生的。
### Cookie
Cookie存数据得方式是键值对的模式`key:value`，且Cookie是保存在客户端的。浏览器每次发送请求就会自动带上Cookie。假设有一个需要让用户保持登录的需求。客户端在发送登录请求后，服务器响应并验证了信息。之后服务器把用户名和密码放在Cookie中返回给客户端。之后客户端每次发送请求就都会带上Cookie，这样就能保持登录状态了。但是Cookie中保存的信息是明文的，用户名和密码存在里面很不安全。

![在这里插入图片描述](https://img-blog.csdnimg.cn/c934876a686f48b1982331035b54bcd2.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAT2xkWmhhbmdZSA==,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)
**Cookie常用的属性：**
1. maxAge ：Cookie失效时间，单位秒。Cookie在maxAge秒之后失效。
2. path：该Cookie使用路径。如果设置为”/sessionWeb/”,则只有contextPath为”/sessionWeb”的程序可以访问该Cookie。如果设置为”/”,则本域名下contextPath都可以访问该Cookie。
3. domain：可以访问该Cookie的域名。如果设置为”.google.com”,则所有以”google.com”结尾的域名都可以访问该Cookie。
### Session
Session与Cookie不同，Session是存在服务端的。还是假设一个保持登录的场景。服务端在验证登录请求后就生成了一个SessionID，通常来说是一个随机的字符串。并把这个字符串返回给客户端（因为是随机的字符串，没有使用明文的用户名和密码所以相对Cookie安全一点。）之后客户端在下一次访问时可以在Cookie中或者在url中带上SessionId，服务端接收到SessionID进行验证就是做到保持登录的效果了。
但是当服务器是一个集群时，就出现了一个问题。就是服务器A生成了SessionId并保存，但是服务器B并没有这个SessionID。所以当客户端下次访问的是服务器B时，就无法通过SessionID保持登录了

 - [ ] 多服务器时的Session
## 输入网址后回车发生了什么

* 解析URL，来获得需要使用的传输协议、域名、资源路径、非法字符的转义等
* DNS域名解析，获得域名之后需要把域名转化为IP地址，客户端才可以访问。客户端会先检查本地的host文件有没有存这个域名对应的IP地址，没存的话就会像根DNS服务器查询，根服务器根据域名的后缀返回子DNS服务器地址。本地再向这个子服务器查询。
* 获得IP地址后，经过TCp的三次握手就和服务器建立起了TCP连接
* 建立连接后就可以根据协议发送请求了，一般都是http请求
* 服务器处理请求，SpringMVC获得请求后会根据请求寻找对应的Controller处理，并将结果返回
* 当双方都没有消息要传递的时候，就可以经过TCP4此挥手来关闭连接了。到这里为止服务器的工作就已经完成了。
* 浏览器解析响应数据，如果是http请求。浏览器需要加载解析HTML、CSS、JS、图片、视频等资源。
* 浏览器渲染

# TCP
TCP是**面向连接的、可靠的、基于字节流**的**传输层**通信协议。  
* 面向连接：TCP是点对点，一对一的无法做到一对多。
* 可靠的：由于超时重传等技术，TCP可以保证一个报文一定可以达到接收端。
* 基于字节流的：TCP传递的消息是不限制大小的，无论多大都可以传输。而且是有序的，就算因为网络波动导致消息延误，先收到了后发送的消息也不会给应用层去处理，重复的消息会被丢弃。  
通过源地址、源端⼝、⽬的地址、⽬的端⼝可以唯一的确定一个TCP连接。
## TCP和UDP的区别
UDP不提供复杂的控制机制，他利⽤IP提供⾯向⽆连接的通信服务。  
1. 连接
* TCP是基于连接的，要首先建立起一个连接之后才可以传输数据。
* UDP不用建立连接可以直接发送数据。
2. 服务对象
* TCP主要服务是⼀对⼀的两点服务，即⼀条连接只有两个端点。是两个点之间的通信
* UDP可以一对多、多对多的通信。
3. 可靠性
* TCP会保证数据完整正确的发送到接收端
* UDP只负责发送数据，不保证数据是否完整正确的被接收端接收。
4. 拥塞控制、流量控制
* TCP在网络拥堵时有拥塞控制、流量控制算法来进行调整
* 即使⽹络⾮常拥堵了，UDP也会照常发送数据
5. 首部的开销
* TCP的首部长，有20个字节。（使用了选项吼头部还能更长）
* UDP头部固定时8个字节
6. 传输方式
* TCP是流式传输，没有边界，但保证顺序和可靠。
* UDP是⼀个包⼀个包的发送，是有边界的，但可能会丢包和乱序。
7. 分片方式
* TCP的数据⼤⼩如果⼤于MSS⼤⼩，则会在传输层进⾏分⽚，接收端收到后，也同样在传输层组装TCP数据包，如果中途丢失了⼀个分⽚，只需要传输丢失的这个分⽚
* UDP的数据⼤⼩如果⼤于MTU⼤⼩，则会在IP层进⾏分⽚，⽬标主机收到后，在IP层组装完数据，接着再传给传输层，但是如果中途丢了⼀个分⽚，在实现可靠传输的UDP时则就需要重传所有的数据包，这样传输效率⾮常差，所以通常UDP的报⽂都⼩于MTU。
8. 应用场景
* TCP的传输是可靠的，因此常用于**FTP**、**HTTP/HTTPS**
* UDP面向无连接但是传输迅速，处理高效。因此经常用于**视频，音频通信**、**广播通信**
## TCP首部（头格式）
TCP请求的格式入下图所示  
![在这里插入图片描述](https://img-blog.csdnimg.cn/cd5a7bdb07b24af0830d1de2a285c682.png#pic_center)
并不是所有的内容都很重要，标了颜色的比较重要。
* 序列号：在建立连接时由计算机生成成的随机数作为其初始值，每发送⼀次数据，就累加⼀次该序列号的大小。用来解决TCP请求的乱序问题。
* 确认应答号：指下⼀次**希望**收到的数据的序列号，发送端收到这个确认应答以后可以认为在这个序号以前的数据都已经被正常接收。用来解决丢包的问题
* 控制位：
	* ACK：该位为 1 时，确认应答的字段变为有效，TCP 规定除了最初建立连接时的 SYN 包之外该位必须设置为 1 。
	* RST：该位为 1 时，表示 TCP 连接中出现异常必须强制断开连接。
	* SYN：该位为 1 时，表示希望建立连接，并在其序列号的字段进行序列号初始值的设定。
	* FIN：该位为 1 时，表示今后不会再有数据发送，希望断开连接。当通信结束希望断开连接时，通信双方的主机之间就可以相互交换 FIN 位为 1 的 TCP 段。
## 为什么TCP可靠
TCP有三次握手建立连接和4次挥手关闭连接的机制，还有**滑动窗口**和**阻塞控制**算法。最关键的是还有**超时重传**机制。对于每份报文进行校验，保证报文的可靠性。
###  超时重传
 当发送端的TCP发送数据时，会给数据编号。且每发送一次数据就会等待接收端返回ACK（表示收到了）。当没收到ACK时，发送端就会重新发送数据。如果接收端收到了数据，并发送了ACK但是因为网络或其他原因导致发送端没收到ACK时。接收端就会重复收到发送端发出的数据，但是因为发送端对每个数据都进行了编号，所以接收端重复收到数据时，直接丢弃就行了。当发送端重新发送几次数据但还是接受不到ACK时，发送端就会当方面断开连接。  
 这个重传的时间应该略大于数据往返一次的时间，太短会导致经常性的重复发送，性能差。太长会导致，数据丢了半天了才重新发送，效率低。
### 滑动窗口
发送端的TCP一次次发送数据等待返回ACK效率就会很低。因此正常情况下发送端会将数据一部分，一部分的发送。这个就是滑动窗口技术。  
假定总共有6份数据要发送，窗口大小为3。那么发送端一次发送就会把1~3的数据全部发送给接收端。当接收到接收端返回的1号数据的ACK后就会将窗口滑到2-4，如果没接收到2号数据的ACK那么此时就会重发2-4数据。发送窗口的大小时根据接收端的接受能力来调整的。接收端会将窗口大小放在TCP首部返回给发送端。
### 阻塞控制
网络带宽是有限的，因此在网络拥挤时发送端还发送大量数据就有可能导致网络瘫痪。接收端没有接收到数据，发送端接收不到ACk进行重传又加重网络瘫痪。这个时候就需要阻塞控制，阻塞窗口的大小由网络环境决定。一开始阻塞窗口较小，之后每接收到一次ACK就表示当前网络时畅通的。就会增加阻塞窗口的大小，发送数据时的窗口大小取**阻塞窗口和接收端返回的窗口大小的最小值**。随着阻塞窗口的变大，网络压力变大导致ACK接受失败时，，就会减小阻塞窗口。
## TCP的三次握手
![在这里插入图片描述](https://img-blog.csdnimg.cn/9af8735b3d3f4959bc451b067296a087.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAT2xkWmhhbmdZSA==,size_19,color_FFFFFF,t_70,g_se,x_16#pic_center)

在说明握手前我们首先得直到TCP报文中有很多标识，在握手中用到的有**SYN（同步）、ACK（确认）**，设置为1就是开启，设置为0就是关闭。报文中还携带有随机生成的序列号**Sequence**和确认应答号**AckSequence**
### 第一次握手
假定**Sequence**为10086，客户端将**SYN**设置为1（表示想和服务器同步），并把报文发送给服务端。
### 第二次握手
服务器接收到报文后，**根据端口号等信息运算生成一个序号**Sequence假定为10000和一个确认应答号**AckSequence**，**AckSequence**的值是**Sequence**+1也就是10087。并将**SYN**和**ACK**设置为1，表示确认同步，发送给客户端。
### 第三次握手
客户端接收到服务器的报文后，将**ACK**设置为1，并生成**Sequence和AckSequence**，这个**Sequence**为第二次握手的**AckSequence**+1，也就是10001。**AckSequence**为上一次的**Sequence**+1，也就是10088。然后将报文发送给服务端。
三次握手过后客户端和服务端就建立了连接可以发送HTTP请求了。
### Sequence和AckSequence
总的来说就是第一次生成的**Sequence**为随机生成，第二次生成的**Sequence**为服务器根据端口号等信息运算生成的，第三次的**Sequence**是上一次的**AckSequence**+1。
**AckSequence**都是上一次**Sequence**+1
### 为什么要三次握手，两次行不行
不行，
1. **会导致数据包重复或混乱**
TCP连接稳定的关键就是一对序列号**Sequence**，只有两次握手只能确定客户端的**Sequence**，无法确定服务端的**Sequence**。所以客户端就无法判断服务端返回的数据的顺序。就有可能出现客户端接收到的数据包重复或者顺序混乱的问题。
2. **两次握手会导致重复连接**
在网络环境比较复杂的情况下，客户端可能会连续发送多次请求。如果只设计成两次握手的情况，服务端只能一直接收请求，然后返回请求信息，也不知道客户端是否请求成功。这些过期请求的话就会造成网络连接的混乱。
## SYN攻击
SYN攻击就是攻击者短时间伪造大量不同IP地址的SYN报⽂（也就是第一次握手的那次请求），确不回应服务器发出的ACK+SYN报文。导致服务器无法相应正常用户的请求。
**避免SYN攻击的方法**：
1. 减少SYN等待的时间
2. 修改服务端重发的次数
3. 对攻击源进行拉黑操作

## TCP的4次挥手
![在这里插入图片描述](https://img-blog.csdnimg.cn/dcbe05a07887487abed065eee2916adb.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAT2xkWmhhbmdZSA==,size_17,color_FFFFFF,t_70,g_se,x_16#pic_center)  
挥手的发起者可以是客户端，也可以是服务端。下面以客户端发起为例。在挥手中用到的标识有**FIN**，**ACK**
### 第一次挥手
客户端向服务端发送**FIN=1**，**Sequence=u**（意思是我没有数据要给你啦）。
### 第二次挥手
服务端向客户端返回**ACK=1**，**Sequence=a**,**AckSequence=u+1**(意思是，我接收到你的第一次挥手了)。但是服务端还有可能有未发送完毕的数据。
### 第三次挥手
服务端将未发送完毕的数据全部发送完毕后，向客户端发送**FIN=1**，**Sequence=b**,**AckSequence=u+1**（意思是，我数据发送完成啦可以关闭连接了）
### 第四次挥手
客户端向服务端返回**ACK=1**，**Sequence=u+1**,**AckSequence=b+1**（表示收到了，我要准备关闭了）。此时客户端不会立刻关闭连接，而是会等待2MSL，防止因为网络问题**ACK**丢失。如果期间没有收到回复（表示服务器正常关闭）就会关闭连接，不然就会进行重传。
### 为什么建立连接只要三次握手，而关闭连接需要四次挥手
因为在主机A请求关闭连接时，主机B可能还有数据没有发送完。所以主机B会在第二次挥手时先返回**ACK**表示自己接收到了关闭请求，然后再发送完剩余的数据后进行第三次挥手返回**FIN**表示数据已经全部发送完了，可以关闭连接了。
### 第四次挥手等等2MSL才关闭连接
MSL是Maximum Segment Lifetime，报⽂最⼤⽣存时间，它是任何报⽂在⽹络上存在的最⻓时间，超过这个时间报⽂将被丢弃。防止因为网络问题第四次挥手的**ACK**丢失，导致被关闭方没有正常关闭。等待2MSL时用来重发可能丢失的**ACK**的。这个2MSL是从主动关闭方收到FIN开始计时的。
