/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-29 23:07:11
 * @ Modified by: OldZhang
 * @ Modified time: 2022-07-05 10:01:38
 * @ Description:
 */

package base.JavaBagu;

public class HashCodeEquals {
    private String a;
    private String b;
    HashCodeEquals(String a,String b){
        this.a=a;
        this.b=b;
    }
    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    public boolean equals(Object obj) {
      
        HashCodeEquals h = (HashCodeEquals) obj;
        if (this.getA().equals(h.getA()) && this.getB().equals(h.getB())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return a.hashCode()+b.hashCode();
    }

}