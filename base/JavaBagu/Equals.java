/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-29 22:54:48
 * @ Modified by: OldZhang
 * @ Modified time: 2022-07-05 10:01:26
 * @ Description:
 */

package base.JavaBagu;

public class Equals {
    public static void main(String[] args) {
        String a = "1";
        String b = new String("1");
        String c = b;
        String d = "1";
    
        System.out.print("a==b:");
        System.out.println(a==b);
        System.out.print("a==d:");
        System.out.println(a==d);
        System.out.print("c==b:");
        System.out.println(c==b);

        System.out.print("a.equals(b):");
        System.out.println(a.equals(b));
        System.out.print("a.equals(d):");
        System.out.println(a.equals(d));
        System.out.print("c.equals(b):");
        System.out.println(c.equals(b));
    }
    
}