/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-29 22:54:48
 * @ Modified by: OldZhang
 * @ Modified time: 2022-07-05 10:01:41
 * @ Description:
 */

package base.JavaBagu;

public interface interfacePlane {
     void fly();
     void refuel();
}
