/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-29 22:54:48
 * @ Modified by: OldZhang
 * @ Modified time: 2022-09-19 21:24:44
 * @ Description:
 */

package base.JavaBagu;

public class test {
    public static void main(String[] args) {
       
        HashCodeEquals h1=new HashCodeEquals("1","2");
        HashCodeEquals h2=new HashCodeEquals("1","2");
        System.out.println("h1和h2是否相等："+h1.equals(h2));
        System.out.println("h1的hashocode："+h1.hashCode());
        System.out.println("h2的hashocode："+h2.hashCode());
    }
}
