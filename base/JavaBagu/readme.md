# 个人相关

面试官好，我是张宇航，现在就读于浙江工商大学计算机学院。我熟悉Java编程语言、也对python有一定的了解。并且实践了工作室的论坛和健康分析的小程序两个项目。在健康分析小程序中使用I/O流来传输菜品图片。用户可以登录并填写自己的身体参数来获取推荐摄入指标，并通过推荐摄入指标与实际摄入指标的对比来得到健康建议。在论坛的项目中使用Redis、SSM、kafka等技术实现了登录注册、敏感词过滤、点赞、个人页面、热帖排行等功能。并申请了“一种基于特征对比的菜品识别算法”和“集装箱编码数据识别方法”等两个发明专利。在校期间，我还以团队负责人的身份参加浙江省第十三届挑战杯竞赛和浙江省互联网+竞赛，并获得浙江省金奖和铜奖。   
除此之外我热爱游戏，游戏经验丰富。且对于rpg、avg和slg游戏的游玩比较深入。rpg游戏中游玩时间比较长的游戏有巫师三、天国：拯救和剑网3等。avg游戏中玩的比较久的有：GTA5。slg游戏中玩的比较久的有全面战争：战锤2、文明6、欧陆风云4等。除了以上三类游戏之外，其他类型的游戏我也都玩过，例如只狼、辐射4等。

## 未来规划

未来的规划的话主要其实就是两个点，一个是完成好本职工作，一个是不断学习。这两个其实也是相辅相成的。完成本职工作这个不用多说。然后在工作的过程中肯定是会遇到一些知识盲区的，就要针对这些盲区去查漏补缺。放到实际来说就是首先第一年刚入职的阶段主要是了解公司的组织架构，熟悉公司的工作状态，工作环境。跟着项目组学习、工作、查漏补缺。尽快让自己进入工作状态。在能很好的完成自己的本职工作之后，就可以根据工作内容进行针对性的学习了。比如我们公司是做实时大数据分析的，那我就会去学习一些大数据、分布式、图计算、图数据库相关的知识不过这个具体的还是要看工作的岗位。同时也要提升自己的代码质量，这个可以去复习设计模式、多看看源码看看其他人是怎么写的。

# Java基础八股

## JDK、JRE、JVM的区别
其实他们三个的区别从他们的英文全称就可以看出来。  
 * @ Modified by: OldZhang

 * @ Modified time: 2022-10-10 14:04:54
    JDK：Java Developement Kit Java开发工具
    很明显的可以看出，JDK是面向开发人员的。JRE只是一个运行环境，JVM是Java虚拟机。  
    他们三者是一个包含的关系，JDK最大向下包含JRE，JRE又向下包含JVM。  
    JVM：就是java虚拟机，负责将class文件解释成机器码来运行  
    JRE：包含了JVM，以及一些类库（String类啊啥的）。JVM在解释class文件时需要调用类库中的类。  
    JDK：包含了JRE，且集成了各种java工具。比如将java文件编译成class文件的javac。    
    ![在这里插入图片描述](https://img-blog.csdnimg.cn/6d745d76c69e414693ec6c813b9977ed.png#pic_center)  
    java文件使用javac编译成class文件后，由JVM调用Lib类库中的类来解释并最终运行。    
    这也是JAVA一处编译到处运行的原因。  

## Java反射机制

  java的反射机制可以通过一个类的class对象来调用该类的描述信息，类的信息。他甚至可以调用私有的对象和方法。因此它可以让我们在程序运行的时候动态获取类的实例，大大提高系统的灵活性和扩展性。让我们可以通过外部文件的配置，在不修改源代码的情况下，来控制程序，符合OCP开闭原则。比如要创建一个student对象使用反射就可以``class.forName(student).newInstance()``来实现。但是反射的效率比较慢。
##  equals和==
* ==比较的是栈中的值，对于基本数据类型来说是变量的值，对于引用类型来说是地址的值。
* euqals是可以自己重写的，具体怎么比较可以自己来设置。但是在object类中是用==实现的。
```
public class Equals {
    public static void main(String[] args) {
        String a = "1";//存在常量池里
        String b = new String("1");//存在堆里面
        String c = b;
        String d = "1";
    
        System.out.print("a==b:");
        System.out.println(a==b);
        System.out.print("a==d:");
        System.out.println(a==d);
        System.out.print("c==b:");
        System.out.println(c==b);

        System.out.print("a.equals(b):");
        System.out.println(a.equals(b));
        System.out.print("a.equals(d):");
        System.out.println(a.equals(d));
        System.out.print("c.equals(b):");
        System.out.println(c.equals(b));
    }
}
```
输出：  
a==b:false  
a==d:true  
c==b:true  
a.equals(b):true  
a.equals(d):true  
c.equals(b):true  
## 访问控制修饰符
总共四种访问控制修饰符，用来控制类、对象、方法的访问权限。
* public：权限最大的修饰符，对于所有的类都可见
* protected：对于同一包内的所有类和子类都可见，但是不能用于修饰外部类
* default：默认的访问控制修饰符，在同一包内可见。
* private：私有的，只在当前类中可见。不能用来修饰外部类。
## final
* 修饰类：不可被继承
* 修饰方法：不可被重写，但是可以被重载
* 修饰变量：赋值之后就不可以修改值了

``final``修饰的变量在使用前一定要赋值，不然会报错。因此用``final``修饰类的成员变量时，就需要在定义的时候就初始化。  
当``final``修饰基本数据时，一旦赋值，值就不能修改了。  
当``final``修饰引用类型时，一旦赋值，引用类型的地址就不能修改了，但是引用的值可以修改。比如我们修饰了一个``List``，这个``List``指向的地址不能变，但是``List``里面的内容可以改。
```
public class Final {
    public static void main(String[] args) {
        final List<Integer> list=new ArrayList<>();
        list.add(1);
        System.out.println(list);
        list.add(2);
        System.out.println(list);
        list.set(0,100);
        System.out.println(list);
        list=null;
    }
}
```
输出：  
[1]  
[1, 2]    
[100, 2]  
**最后设置为null时报错**

### 为什么局部内部类和匿名内部类只能访问局部final变量
内部类不会随着外部类的结束而结束，当外部类结束时，JVM会自动回收外部类的变量等资源。如果内部类引用外部类的变量，而这个变脸又被回收了就会产生错误。因此内部类在使用外部类变脸时会拷贝一份来使用，但是这又造成拷贝的变量和原变量的数据同步问题（原变量变了，拷贝的也得跟着变）。所以JVM左做出了妥协，只能使用final修饰的变量，因为不能变因此就没有了数据同步的问题。

## String、StringBuffer和StringBuilder
### String
java中字符串String有两种创建方式。一种储存在公共池中，一种储存在堆上。

```java
String str="储存在公共池中的创建方式";
String str2=new String("储存在堆上的创建方式");
```
String是静态的，一旦创建就不可变。当改变str的值时，jvm会在内存中创建一个新的String存储str+str2的值，并将其赋值给str。因此变量str只是指向了一个新的对象，**效率极低**
```java
String str="储存在公共池中的创建方式";
String str2=new String("储存在堆上的创建方式");
String str=str+str2;
```
[String相关方法](https://www.runoob.com/java/java-string.html)
### StringBuffer和StringBuilder
当对字符串进行频繁修改的时候，需要使用 StringBuffer 和 StringBuilder 类。和 String 类不同的是，StringBuffer 和 StringBuilder 类的对象能够被多次的修改，并且不产生新的未使用对象。
|| StringBuffer |StringBuilder  |
|--|--|--|
|多线程|线程安全  |线程不安全  |
运行速度|慢（但还是比String快）  |快  |

 #### 清空StringBuffer和StringBuilder

值得一提的是StringBuffer和StringBuilder并没有`clear()`方法来清空元素。因此要采用其他方式来清空所有元素。

 1. 使用delete方法清空

```java
StringBuffer str=new StringBuffer("test");
str.delete(0,str.length());
```

 2. 使用setlengeh
```java
StringBuffer str=new StringBuffer("test");
str.setLength(0);
```
### 使用场景
在不需要频繁的修改字符串的情况下使用``String``。在需要频繁修改的情况下优先使用``StringBuilder``，如果这个字符串时用来左共享变量的或者是在多线程的环境下那就用``StringBuffer``。
## 重载和重写的区别
* 重载：方法名相同然后参数列表不同（顺序，个数，类型都可以不同），发生在同一个类中。**重载和返回值类型无关**
* 重写：方法名相同然后参数列表也相同但是方法体可以不一样，发生在父子类中。子类方法的**访问修饰符**需要大于等于父类的，其他的（**抛出的异常**、**返回值范围**）都小于等于父类的(不能忤逆爸爸)。父类的方法是``private``的，就不能重写了。
## 接口和抽象类的区别
1. 抽象类里面可以具体的已经实现了的方法，而接口中只能由抽象方法（不过现在也可以由默认是实现）。
2. 抽象类中的成员变量可以是任意类型，而**接口中的成员变量**只能是``public static final``的（没错，接口也可以由成员变量）。
3. 抽象类是被继承，而且只能继承一个。接口是被实现，而且可以实现无限个。
4. 抽象类是对于**特征**的抽象，接口时对于**行为**的定义  
   比如说要用抽象类定义飞机的话，那就是  
```
public abstract class absPlane {
    private String wheel;
    private String wings;
    private String chair;
    abstract void fly();
    abstract void refuel();
}
```
抽象类会注意到，飞机都有轮子，有椅子，有翅膀，而且能飞，需要加油。  
而接口就只能定义两个方法，飞机能飞，且要加油。虽然也可以定义轮子什么的成员变量。但那都是static且final的其实就是常量。定义出来没有意义（因为不同的飞机可以时不同的轮子和翅膀）。
```
public interface interfacePlane {
     void fly();
     void refuel();
}
```
## List和Set的区别
* ``List``是有序的顺序存储，可以重复，按照数据进入的顺序排列。可以随机读取。
* ``Set``是无需的，不可重复，不可随机读取。
## HashCode和equals
``hashcode``和``equals``都是``object``类中的方法，因此所有的类都可以重写这两个方法。object中默认的hashcode方法是通过内存地址来获得``hash``的。
关于``hashcode``和``equals``有4条规律：  
1. 两个对象相等，那么他们的``hash``一定相等
2. 两个对象的``hash``相等，这两个对象也不一定相等
3. 两个相等的对象，调用``equals``之后返回的是``True``
4. 重写了一个类的``equals``方法之后，必定要重写这个类的``hashcode``方法

### 为什么重写了equals之后，必须要重写hashcode方法
**因为hashcode是通过对象的内存地址来获取hash的，如果重写了equals(equals就不是比较地址了)之后不重写hashcode方法就会导致两个equals相等的对象，却有着不同的hashcode。这就会导致这种类无法在set集合中正常的工作，会在set中出现相同的对象**  
以``hashset``为例，``hasheset``是一个无序不重复的集合。当我们要添加新元素时，会首先调用``hashcode``方法来获取具体要存放的位置。如果这个位置有元素了，就调用这个类的``equals``方法判断这两个类是否相同，如果不相同就再找个位置（以链表的形式挂在后面）。
**但是当我们重写equals之后**，如果不重写``hashcode``方法，就有可能出现，两个``equals``相等的类=对象，``hashcode``却不同，违反了上述的**1**。同样的这种情况就会让这个类无法在``Set``集合中正常的工作。
```
public class HashCodeEquals {
    private String a;
    private String b;
    HashCodeEquals(String a,String b){
        this.a=a;
        this.b=b;
    }
    @Override
    public boolean equals(Object obj) {
      
        HashCodeEquals h = (HashCodeEquals) obj;
        if (this.getA().equals(h.getA()) && this.getB().equals(h.getB())) {
            return true;
        } else {
            return false;
        }
    }
}
```
```
public static void main(String[] args) {
        HashCodeEquals h1=new HashCodeEquals("1","2");
        HashCodeEquals h2=new HashCodeEquals("1","2");
        System.out.println("h1和h2是否相等："+h1.equals(h2));
        System.out.println("h1的hashocode："+h1.hashCode());
        System.out.println("h2的hashocode："+h2.hashCode());
    }
```
输出：  
h1和h2是否相等：true  
h1的hashocode：1940030785  
h2的hashocode：1869997857  
这样在set中添加HashCodeEquals的对象的时候，**相同的对象也会被放到不同的位置**，**导致set中存在相同的对象**  
**当我们重写了hashcode之后**

```
public class HashCodeEquals {
    private String a;
    private String b;
    HashCodeEquals(String a,String b){
        this.a=a;
        this.b=b;
    }
    @Override
    public boolean equals(Object obj) {
      
        HashCodeEquals h = (HashCodeEquals) obj;
        if (this.getA().equals(h.getA()) && this.getB().equals(h.getB())) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    public int hashCode() {
        return a.hashCode()+b.hashCode();
    }

}
```
```
public static void main(String[] args) {
        HashCodeEquals h1=new HashCodeEquals("1","2");
        HashCodeEquals h2=new HashCodeEquals("1","2");
        System.out.println("h1和h2是否相等："+h1.equals(h2));
        System.out.println("h1的hashocode："+h1.hashCode());
        System.out.println("h2的hashocode："+h2.hashCode());
    }
```
输出：  
h1和h2是否相等：true  
h1的hashocode：99  
h2的hashocode：99  
## ArrayList和LinkedList的区别
### 存储结构
* ``ArrayList``是动态数组，是顺序表。存储的时候需要一块连续的内存来存放数据。
* ``LinkedList``是链表。存储的时候可以是碎片化的内存。
### 查找和修改
* ``ArrayList``可以随机读取，知道了索引之后可以直接根据索引获取元素。修改数据只要知道元素位置就能直接修改
* ``LinkedList``想要查找一个元素只能从头到尾遍历一遍，且在知道索引的情况下也只能从头开始遍历到索引的位置。因此修改也需要花时间在遍历上。
### 插入和删除
* ``ArrayList`` 在插入或删除非尾部元素时，会涉及到数组内元素的移动。比较费时。当然是尾部元素时，ArrayList的插入和删除还是很快的。（甚至比LinkedList快，因为LinkedList还得新建一个``Node``对象）
* ``LinkedList``插入和删除数据的时间复杂度都是O(1)
### 扩容
* ``ArrayList``是数组。因此在数组满了的时候就会重新创建一个**新的空间更大的数组**（第一次扩容是在原来的基础上容量+10，之后每次都是在原来的基础上乘以1.5）。然后把原数组的数据复制到新数组那里去。
* ``LinkedList``没有扩容的需要，他是一个链表，有新的元素了往上面加就是了。
## HashMap和HashTable和currentHashMap的区别
|| HashMap | ConcurrentHashMap |HashTable|
|--|--|--|--|
|底层实现|数组+链表/红黑树|数组+链表/红黑树|数组+链表|
|线程安全|不安全|线程安全（用``synchronized``修饰了``Node``结点）|线程安全（用``synchronized``修饰了整个数组）|
|效率|高|中等|最慢|
|扩容|初始是16，每次扩容成2n|初始是16，每次扩容成2n|初始11，每次扩容成2n+1|
|key和value是否能为null|都可以|都不行|都不行|
## HashMap
* ``HashMap``声明时，数组是空的，只有第一次添加元素时才会有数组长度
* ``HashMap``的底层实现是数组+链表/红黑树
* ``HashMap``初始容量是16
* ``Key``为``null``的结点在数组的第一个位置
* 当``HashMap``的容量大于当前最大容量的**0.75**（默认是这个，可以自行修改）时，``HashMap``就会扩容为当前容量的**两倍**，扩容前，当前元素已经放好了
* 当出现冲突时，会比较当前结点的``Key``是否和要加入元素相同，**相同就替换**。不相同就继续往链表/红黑树中比较，比较到最好都没有相同的就加入到链表或者树中。
* 当链表长度超过8时
    * 如果数组长度超过64，链表就会转化成红黑树
    * 如果数组长度没超过64，数组直接扩容。
* HashMap的最大容量必定是$2^n$
### HashMap的最大容量为什么必定是$2^n$
HashMap的默认初始长度是16，如果自己自定义了初始的长度**n**，HashMap也会自动切换成大于**n**最小的2的幂次方。  
会使用``tableSizeFor``函数你把传进去的值，切换成大于这个值的最小2的幂次方。而每次扩容又是乘以2，所以必定是$2^n$。

```
static final int tableSizeFor(int cap) {
        int n = -1 >>> Integer.numberOfLeadingZeros(cap - 1);
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }
```
### HashMap的最大容量为什么要是$2^n$
``HashMap``是通过``Key``的``hash``值来计算具体放置到数组的哪个位置，由于``hash``的值很大（就是int的范围），不可能创建这么大的数组。因此需要使用``hash``的值对数组长度``length``取余来计算出最终的位置。如果数组的长度是$2^n$，那么 **（length-1）&hash就等于hash%length**，就可以使用位运算来代替%取余操作来提高性能。

### HashMap添加数据的逻辑流程图
<img src="img/HashMap插入流程.png">  

### HashMap扩容后原本元素的位置会发生怎么样的改变
扩容后HashMap会将原数组中的元素都重新散列到新数组中。 
我们假设**一个元素没有next**在扩容前所在的位置为**i**，扩容前数组的长度为**n**。那么扩容后这个元素所在的位置有两种可能
1. 还在位置**i**
2. 在位置**i+n**
元素所在的位置是 **(length-1)&hash** 计算出来的，我们假设这个元素的**hash=2**,**n=16**。
那么扩容前元素位置为2  
```
0000 0000 0000 1111 // length-1的二进制表示
&
0000 0000 0000 0010 // hash的二进制表示
=
0000 0000 0000 0010 // 转化为10进制就是2
```
扩容后，**n**变成了32,元素位置还是2
```
             ^
0000 0000 0001 1111 // length-1的二进制表示
&
0000 0000 0000 0010 // hash的二进制表示
=
0000 0000 0000 0010 // 转化为10进制就是2
```
假设另一个元素的**hash**是16，那么扩容前位置0，扩容后元素位置就变成了16  
扩容前计算过程
```
0000 0000 0000 1111 // length-1的二进制表示
&
0000 0000 0001 0000 // hash的二进制表示
=
0000 0000 0000 0000 // 转化为10进制就是0
```
扩容后计算过程
```
             ^
0000 0000 0001 1111 // length-1的二进制表示
&
0000 0000 0001 0000 // hash的二进制表示
=
0000 0000 0001 0000 // 转化为10进制就是2
```
我们可以发现，由于数组容量的变大导致参与 **&** 运算的二进制数多了一位，就是上面那个带"^"号的1。
* 如果这个1运算后的结果是0，元素位置不变
* 如果这个1运算后的结果是1，元素位置变成了**i+n**。

如果是有next的元素也就是链表（不是红黑树），那么他的新位置是通过(hash & oldCap) == 0
* (hash & oldCap) == 0 ，就是原地**i**

* (hash & oldCap) != 0 ，就是**i+n**
  **原来一条链上的，扩容后就被分散了。**

## Java IO/NIO

  ### JAVA IO模型

  1. 阻塞IO模型  
     这个是最传统的一种IO模型，就是线程在进行IO操作的时候阻塞。当线程发出IO申请之后，内核就会去查看数据是否准备好了，如果没有就绪就会等待数据就绪。这个时候线程就处于阻塞的状态，知道数据就绪了才会接触阻塞状态。比较典型的就是java IO包中的read()和write()方法。
  2. 非阻塞IO模型  
     当线程发起IO申请时，内核会马上返回给线程一个结果表示数据有没有准备好。如果没有准备好，线程就会再次发送IO申请，直到数据准备完毕。这个模型的线程不会进入阻塞状态，而是不停的询问内核数据是否准备就绪，因此这个线程会一直占用着CPU导致CPU占用率会很高。
  3. 多路复用IO模型  
     这个是目前用的比较多的IO模型，JAVA NIO就是一个多路复用的IO。在这个模型中会有一个线程不断地轮询多个socket的状态，只有当socket真正有读写事件时，才真正调用实际的IO读写操作。**此时其他的线程可以去干其他事情**，但由于不是每个线程都在自己轮询数据是否准备完毕，而是一个专门的线程再干这个活，因此比非阻塞IO快得多。
  4. 信号驱动IO模型  
     在信号驱动IO模型中，当用户线程发起一个IO请求操作，会给对应的socket注册一个信号函数，然后用户线程会继续执行，当内核数据就绪时会发送一个信号给用户线程，用户线程接收到信号之后，便在信号函数中调用IO读写操作来进行实际的IO请求操作。
  5. 异步IO模型  
     异步IO模型才是最理想的IO模型，在异步IO模型中，当用户线程发起read操作之后，立刻就可以开始去做其它的事。而另一方面，从内核的角度，当它受到一个asynchronous read之后，它会立刻返回，说明read请求已经成功发起了，因此不会对用户线程产生任何block。然后，内核会等待数据准备完成，然后将数据拷贝到用户线程，当这一切都完成之后，内核会给用户线程发送一个信号，告诉它 read 操作完成了。也就说用户线程完全不需要实际的整个IO操作是如何进行的，只需要先发起一个请求，当接收内核返回的成功信号时表示 IO 操作已经完成，可以直接去使用数据了。也就说在异步IO模型中，IO操作的两个阶段都不会阻塞用户线程，这两个阶段都是由内核自动完成，然后发送一个信号告知用户线程操作已完成。用户线程中不需要再次调用 IO 函数进行具体的
     读写。这点是和信号驱动模型有所不同的，在信号驱动模型中，当用户线程接收到信号表示数据已经就绪，然后需要用户线程调用 IO 函数进行实际的读写操作；而在异步 IO 模型中，收到信号表示 IO 操作已经完成，不需要再在用户线程中调用 IO 函数进行实际的读写操作。

  ### Java IO包  

  ![](img/javaio.png)  
  javaIO包是面向流的，对于数据的输入/输出操作以"流（stream）"的方式进行。这里的流分为字节流和字符流。
  Java程序（内存）—输入流—> 文件（磁盘）—输出流->Java程序（内存）

  |            | 字节流                                   | 字符流                               |
  | ---------- | ---------------------------------------- | ------------------------------------ |
  | 读取单位   | 以字节为单位读取（8bit）                 | 以字符为单位读取                     |
  | 处理对象   | 字节流可以处理任何对象，包括视频、图像等 | 字符流只能处理字符                   |
  | 具体实现类 | 字节流的实现类是``xxxOut/InputStream``   | 字符流的实现类是``xxxReader/Writer`` |

  - Java IO包中提供了很多处理流，他通过连接一个已经存在的流来提供更多的操作或者功能。比如``BufferedInputStream，BuffereredReader，ObjectWriter``等。尽量使用这种包装类来进行I/O操作，速度会快很多。


  ### Java NIO（new input/output）

  Java NIO有三个核心

  * Channel（通道）
  * Buffer（缓冲区）
  * Selector(选择器)  
    区别于传统的IO基于字节流和字符流操作，JavaNIO基于Channel和Buffer操作，数据从Channel写入到Buffer或者从Buffer写入到Channel。Selector(选择区)用于监听多个通道的事件（比如：连接打开，数据到达）。因此，单个线程可以监听多个数据通道。**Java NIO是一个多路复用的IO模型**    
    ![](img/nio.png)  

  - NIO和IO最大的区别就是IO是面向流的，而NIO是面向缓冲区的。

  1. Channel 通道  
     Channel 和 IO 中的 Stream(流)是差不多一个等级的。只不过 Stream 是单向的，譬如：InputStream, OutputStream，而 Channel 是双向的，既可以用来进行读操作，又可以用来进行写操作。
  2. Buffer（缓冲区）  
     ![](img/nio%E6%B5%81%E7%A8%8B.png)  
     Buffer，故名思意，缓冲区，实际上是一个容器，是一个连续数组。Channel 提供从文件、网络读取数据的渠道，但是读取或写入的数据都必须经由 Buffer。上面的图描述了从一个客户端向服务端发送数据，然后服务端接收数据的过程。客户端发送数据时，必须先将数据存入 Buffer 中，然后将 Buffer 中的内容写入通道。服务端这边接收数据必须通过 Channel 将数据读入到 Buffer 中，然后再从 Buffer 中取出数据来处理。
  3. Selector(选择器)  
     Selector类是NIO的核心类，Selector 能够检测多个注册的通道上是否有事件发生，如果有事件发生，便获取事件然后针对每个事件进行相应的响应处理。这样一来，只是用一个单线程就可以管理多个通道，也就是管理多个连接。这样使得只有在连接真正有读写事件发生时，才会调用函数来进行读写，就大大地减少了系统开销，并且不必为每个连接都创建一个线程，不用去维护多个线程，并且避免了多线程之间的上下文切换导致的开销。
# 并发
## 并发的三大特性
### 原子性
原子性指的是一个操作，**要么全部执行完，要不全部不执行**。  
假设我们现在有两个线程来执行以下代码。
```
static int i=0;
add();

public void add(){
    i++;
}
```
每个线程都有自己的工作内存，线程从Java主存中复制一份要用的变脸到工作内存中，操作完之后在赋值回去（这一步由操作系统决定什么时候做）。
当线程A执行到``i++``时cpu切换到了线程B，并执行完成。此时i的值因为已经加了一次了所以应该时1。然后cpu切换回线程A继续执行，而线程A之前读到的i的值是0，i++之后i的值是1。也就是加了两次之后i的值还是1。
可以使用：synchronized
### 可见性
当多个线程访问同一个变量的时候，一个线程修改了这个值，其他的线程可以立刻看到修改的值。
可见性可以保证**操作完之后在赋值回去**这个过程是原子的。  
可以使用：final，volatile，synchronized
### 有序性
JVM在执行时，会在不影响单线程最终结果的情况下调整代码的执行顺序(指令重排)。但是在多线程的情况下，可能会出现错误，因此我们需要保证在多线程的情况下，代码的执行是有序的。
```
int a=0;
boolean flag=false;
a=2;
flag=true;
if(flag){
    a++;
}
```
可以使用：volatile，synchronized
## 对线程安全的理解
### 什么是线程安全
我们说某某东西是不是线程安全的，意思就是这东西在多线程和单线程的情况下运行的结果是否是一样的。如果是一样的那么他就是线程安全的，如果是不一样的那他就是线程不安全的。

### 为什么会有线程安全问题
现在主流的操作系统都是多任务的，然后每个进程都有一个自己的堆。堆是进程内线程共享的，同个进程下的任何线程都可以访问。这就造成了线程的安全问题。  
**堆**  
操作系统有一块内存区域**堆**，堆是共享内存，同个进程下的任何线程都可以访问。堆是进程和线程共有的空间，可以分为全局堆和局部堆。
* 全局堆：所有没有分配的空间都属于全局堆
* 局部堆：用户分配的空间  
  

堆在操作系统初始化进程的时候分配，进程在运行的过程中也可以向系统索要额外的堆。但是用完了要还给系统，还不回去就是内存泄漏了。  
**在Java中**，堆是JVM管理的内存中最大的一块，在JVM启动的时候创建，用来存放对象实例和数组，几乎所有的对象实例和数组都存在在这里。**（什么情况下会不在？）**  
**栈**
栈是线程独有的，保存线程的运行状态和局部变量。栈在线程开始的时候初始化，由于栈是线程独立的，线程无法访问其他线程的栈，因此栈是线程安全的。系统在切换线程的时候会自动的切换栈，栈也不需要我们手动释放或者分配空间，他是自动的。

@[TOC](Java 浅拷贝和深拷贝)
## 浅拷贝和深拷贝
**浅拷贝**：直接复制了对象A地址给另一个对象B。由于对象A，B指向的是同一个地址因此修改B，A也会跟着改变。
![在这里插入图片描述](https://img-blog.csdnimg.cn/23cf94c6a3ac41668dffd51af6155b74.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAT2xkWmhhbmdZSA==,size_14,color_FFFFFF,t_70,g_se,x_16#pic_center)
**深拷贝**：复制了对象A的值给另一个对象B，这两个对象没有任何关系指向不同的地址。修改其中一个并不会对另一个造成影响。
![在这里插入图片描述](https://img-blog.csdnimg.cn/34c2f390094446ecaf108e3f99340c75.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAT2xkWmhhbmdZSA==,size_11,color_FFFFFF,t_70,g_se,x_16#pic_center)
### 浅拷贝
先把要用到的两个类放上来，一个**狗类**，一个**毛类**。`get()、set()`什么的就不放了。

```cpp
class Dog{
    private String name;
    private int age;
    private String color;
    private Mao mao;
    }
class Mao {
    private String changduan;
    private String color;
    }
```

#### 直接赋值

```cpp
public void assign() {
		Dog d1 = new Dog("d1", 1, "green");
        Dog d2 = d1;
        d2.setName("d2");
        // 可以看到修改了d2后，d1也改变了
        System.out.println("d1 name:" + d1.getName());
        System.out.println("d2 name:" + d2.getName());
    }
```
输出：
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d1 name:**d2**
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d2 name:**d2**
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**可以看到修改了d2的name后，d1的name也发生了改变**

#### 重写clone()
`clone()`其实可以说是**半深半浅**。下面细🔒
可以通过重写`Object`类的`clone()`方法来进行拷贝。  
##### clone的深拷贝方面
**首先**要重写`clone()`方法需要实现`Cloneable`接口

```cpp
class Dog implements Cloneable{
    private String name;
    private int age;
    private String color;
    private Mao mao;
     /**
     * 重写clone方法
     */
    @Override
    public Dog clone() {
        try {
            return (Dog)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
    }
```
**然后**创建一个一岁长毛狗**d1**，并使用`clone()`方法将**d1**拷贝给**d3**
```cpp
  public void clone_() {
      Dog d1 = new Dog("d1", 1, "green");
      Mao mao =new Mao("long","black");
      d1.setMao(mao);
      Dog d3 = d1.clone();
      d3.setAge(2);
      System.out.println("d1:的年龄" + d1.getAge());
      System.out.println("d3:的年龄" + d3.getAge());
    }
```
输出：
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**d1**:的年龄**1**
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**d3**:的年龄**2**
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我们发现修改了**d3**的age后并没有影响到**d1**。这就是深拷贝。
##### clone的浅拷贝方面
还是之前那两个对象，只不过这次我们修改其中的**Mao(毛)** 属性。
```cpp
    public void clone_() {
        Dog d1 = new Dog("d1", 1, "green");
        Mao mao =new Mao("long","black");
        d1.setMao(mao);
        Dog d3 = d1.clone();
        d3.setAge(2);
        mao.setChangduan("short");
        mao.setColor("red");
        System.out.println("d1的Mao属性" + d1.getMao()+"d1:的年龄" + d1.getAge());
        System.out.println("d3的Mao属性" + d3.getMao()+"d3:的年龄" + d3.getAge());
    }
```
输出：
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**d1**的Mao属性Mao [changduan=short, color=red]**d1**:的年龄1
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**d3**的Mao属性Mao [changduan=short, color=red]**d3**:的年龄2
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我们发现修改了修改**d3**的`age`年龄属性不会对**d1**造成影响，但是修改**d3**的**Mao**属性就会把**d1**的**Mao**属性也改变了。这就是`clone()`的**浅拷贝！**
##### 为啥又有深又有浅
如下图所示，虽然重写了`clone()`方法，拷贝出来的对象指向了不同的地址。但是**d1**和**d3**的**Mao**属性并没被拷贝出来。也就是说**d1**和**d3**的属性**Mao**指向的是同一个地址，所以修改**Mao**的值，就会同时影响到**d1**和**d3**。
![在这里插入图片描述](https://img-blog.csdnimg.cn/896a7267245240eca43fc6fa1627c334.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAT2xkWmhhbmdZSA==,size_14,color_FFFFFF,t_70,g_se,x_16#pic_center)
##### 总结
可以使用`clone()`直接深拷贝的：
1. 本数据类型如**int**、**char**等
2. 基本数据类型的包装类**Integer**等
3. **String**  

不可以**简单的**使用`clone()`直接深拷贝的：
引用类型的数据如我上面自己定义的**Mao**类，数组等。
**但是重写clone()的时候处理下还是可以深拷贝的**
### 深拷贝
#### 重写clone()
没错又是重写`clone()`,只要我们对**对象中的引用类型**进行处理，就可以使用`clone`进行深拷贝。就是让这个引用类型也重写`clone`（套娃！）  
**首先**让Mao类也重写`clone()`，并修改**Dog**类的`clone()`逻辑，把拷贝后的**Mao**赋值给拷贝后的**Dog**
```cpp
class Mao implements Cloneable{
    private String changduan;
    private String color;
    }
    @Override
    public Mao clone()  {
        try {
            return (Mao)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
class Dog implements Cloneable{
    private String name;
    private int age;
    private String color;
    private Mao mao;
     /**
     * 重写clone方法
     */
 	@Override
    public Dog clone() {
        try {
            Dog res=(Dog)super.clone();
            res.mao=mao.clone();
            return res;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
```
然后使用`clone()`进行拷贝

```cpp
public void deepClone() {
        Mao mao = new Mao("long", "black");
        d1.setMao(mao);
        Dog d3 = d1.clone();
        d3.setName("d3");
        mao.setChangduan("short");
        mao.setColor("red");
        System.out.println("d1:" + d1);
        System.out.println("d3:" + d3);
    }
```
d1:Dog [age=1, color=green, mao=**Mao [changduan=short, color=red], name=d1**]
d3:Dog [age=1, color=green, mao=**Mao [changduan=long, color=black], name=d3**]
但是如果**Mao**类里面又有一个引用类型的属性，那就又要继续重写`clone()`无限套娃很麻烦。
**完美实现深拷贝**
#### new一个新对象
我们可以通过new一个新的对象，然后把老对象的值复制给新对象来做到深拷贝（**对象较复杂的时候很麻烦**）  

```cpp
   public void newObj() {
        Dog d2 = new Dog(d1.getName(), d1.getAge(), d1.getColor());
        d2.setAge(88);
        System.out.println("d1:" + d1);
        System.out.println("d2:" + d2);
    }
```
老简单了，不多说了
#### 使用序列化和反序列化进行深拷贝
序列化可以把java类的所有**数据及其数据类型**都输出到IO中，然后反序列化就是从IO中读取这些数据重新生成对象。由于是直接读取值然后新生成一个对象，所以就是**深拷贝**！
```cpp
    public void serDeepCopy() throws IOException, ClassNotFoundException {
        Mao mao=new Mao("short", "red");
        d1.setMao(mao);
        //先把序列化数据放到内存操作流buf中
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        ObjectOutputStream obs = new ObjectOutputStream(buf);
        obs.writeObject(d1);
        obs.close();
        //从buf流中读取数据 并反序列化
        ByteArrayInputStream ios = new ByteArrayInputStream(buf.toByteArray());
        ObjectInputStream ois=new ObjectInputStream(ios);
        Dog d4=(Dog)ois.readObject();
        mao.setColor("black");
        d4.setName("d4");
        System.out.println("d1:" + d1);
        System.out.println("d4:" + d4);
    }
```
d1:Dog [age=1, color=green, mao=Mao [changduan=short, **color=black], name=d1**]
d4:Dog [age=1, color=green, mao=Mao [changduan=short, **color=red], name=d4**]



# JVM

## 类加载的过程

1. 使用类加载器把class文件加载到内存中，生成对应的class项
2. 验证是否有正确的内部结构、给静态变量分配空间、将符号引用转成直接引用。
3. 初始化静态变量、静态代码块


## Java类加载器
JDK自带有三个类加载器：
* BootstrapClassLoader
* ExtClassLoader
* AppClassLoader

BootstrapClassLoader是ExtClassLoader的父类，负责加载%JAVA_HOME%/lib目录下的jar包和class文件   
ExtClassLoader是AppClassLoader的父类，负责加载%JAVA_HOME%/lib/ext目录下的jar包和class文件   
AppClassLoader是自定义加载器的父类，是我们最常接触的类加载器，负责加载classPath（也就是项目路径）下的jar包和class文件。他还是线程上下文加载器。
我们自定义的类加载器的父类要指定为AppClassLoader。  
**PS**：这里说的父类不是说继承关系，而是类加载器里面的一个parent属性。  
## 类加载机制
### 双亲委派机制
1. 如果一个类加载器收到了类加载请求，它并不会自己先加载，而是把这个请求委托给父类的加载器去执行
2. 如果父类加载器还存在其父类加载器，则进一步向上委托，依次递归，请求最终将到达顶层的引导类加载器；
3. 如果父类加载器可以完成类加载任务，就成功返回，倘若父类加载器无法完成加载任务，子加载器才会尝试自己去加载，这就是双亲委派机制
4. 父类加载器一层一层往下分配任务，如果子类加载器能加载，则加载此类，如果将加载任务分配至系统类加载器也无法加载此类，则抛出异常
原文链接：https://blog.csdn.net/nsx_truth/article/details/109145080
![](img/%E5%8F%8C%E4%BA%B2%E5%A7%94%E6%B4%BE%E6%9C%BA%E5%88%B6.png)  
> 为什么要有双亲委派机制？  
> 这种设计有个好处是，如果有人想替换系统级别的类：String.java。篡改它的实现，在这种机制下这些系统的类已经被Bootstrap classLoader加载过了（为什么？因为当一个类需要加载的时候，最先去尝试加载的就是BootstrapClassLoader），所以其他类加载器并没有机会再去加载，从一定程度上防止了危险代码的植入。
## java类加载顺序
先静态，再其他。先父类，再子类。
1. 父类静态代变量
2. 父类静态代码块
3. 子类静态变量
4. 子类静态代码块
5. 父类非静态变量
6. 父类构造函数
7. 子类非静态变量
8. 子类构造函数
## Java内存结构
JVM内存区域主要分为
* **线程私有区域**【程序计数器、虚拟机栈、本地方法区】。这块区域的生命周期和线程的生命周期相同，随着线程的创建而创建。随着线程的销毁而销毁。  
* **线程共享区域**【JAVA堆、方法区】。这块区域的生命周期和JVM相同，随着JVM的创建而创建。随着JVM的销毁而销毁。  
* **直接内存**。直接内存并不是JVM运行时数据区的一部分。他是使用Native函数库直接分配的堆外内存, 然后使用   DirectByteBuffer对象作为这块内存的引用进行操作。就不用再JAVA堆和Native堆里面来回复制数据了。    
<img src="img/jvm内存模型.png">

### 程序计数器（Program Counter Register）
程序计数器是线程私有的，是一块一块较小的内存，他记录了当前线程执行的字节码文件的行号。如果执行的是java方法的话，那么程序计数器记录的就是字节码指令的地址。是native方法的话就是空。这也是jvm内存中唯一一个没有设置OOM异常的区域。
### 虚拟机栈 (Java Stack)
虚拟机栈是线程私有的。虚拟机栈存放和Java方法相关的一些变量。每个方法在执行的时候都会生成一个栈帧。栈帧Frame是一个存储了，局部变量表、操作数栈、动态链接、方法出口等信息的数据结构。每一个方法从调用直至执行完成的过程，就对应着一个栈帧在虚拟机栈中入栈到出栈的过程。他随着方法的调用而创建，随着方法的结束而销毁。执行出异常也会导致栈帧的销毁。  
<img src="img/虚拟机栈.png">  

### 本地方法区
和Stack差不多，也是线程独有的。只不过本地方法区是服务于本地（Native）方法的,HotSpot VM将本地放大区和Stack合二为一了。
### 堆(Heap)-运行时数据
堆是线程共享的，堆里面存放在的是对象和数组。这也是GC回收机制，工作最频繁最重要的区域。  
从Java垃圾回收的角度，堆可以分为两个部分：

* 新生代
  * Eden区
  * SurvivorFrom区
  * SurvivorTo区
* 老年代
<img src="img/年代.png">  

#### 新生代
是用来存放新生的对象。一般占据堆的 1/3 空间。由于频繁创建对象，所以新生代会频繁触发MinorGC进行垃圾回收。新生代可以被划分为三个部分：
* Eden区：新对象的出生地(如果新的对象太大的话，就会直接被分配到老年代)。当Eden区域满了的时候就会触发一次MinorGC对整个新生代进行一次回收。
* SurvivorFrom区：在MinorGC对整个新生代进行回收后，幸存下来的对象都会被存放在这个地方。
* SurvivorTo区：MinorGC回收时的暂存区，先在这里放一下然后在复制到SurvivorFrom去。

#### 老年代
主要存放应用程序中生命周期长的内存对象。使用MarjorGC来回收对象。MarjorGC使用标记清除法。  
老年代的对象很稳定，MarjorGC不会频繁的执行。

* 当没有足够的连续空间分配给大对象的时候就会执行MarjorGC。
* 没有足够的空间分配给新生代达到年龄标准晋升的对象。
由于使用的是标记清除法，所以会造成很多的内存碎片，所以一般需要进行合并。  
当MarjorGC之后还是没有足够的空间来进行，就会抛出OOM异常（Out of Memory）。
#### 永生代
用来存放永久数据，包括Class和meta数据，GC是不会在永生代工作的，因此在Class永远不会被回收。最终永生代里面加载的Class越来越多最终导致OOM异常。
#### 元数据区
在Java8以后，堆中就没有永生代了。取而代之的是元数据区（元空间）。元数据区不在Java堆中，甚至不在JVM中，而是使用了本地内存。类的元数据放入元空间，字符串池和类的静态变量放入java堆中。这样限制加载的Class数量的内存空间就从堆的大小，变成了系统实际可用内存的大小。

#### MinorGC
MinorGC采用的是复制算法。
1. 首先对整个新生代来一次GC，之后将幸存下来对象复制到SurvivorTo。如果年龄到了就放到老年代，一般是15岁。并且将这个对象的年龄+1，如果SurvivorTo放不下，就放到老年代。
2. 清空Eden和SurvivorFrom中的对象。
3. 最后，SurvivorTo和SurvivorFrom互换，原SurvicorTo成为下一次GC时的 SurvicorFrom区。


### 方法区 ****(?)
方法区也就是我们说的永生代(Permanent Generation),用于存储被JVM加载的类信息、常量、静态变脸、即时编译器编译后的代码等。HotSpot VM把GC分代收集扩展至方法区,即使用Java堆的永久代来实现方法区, 这样HotSpot的垃圾收集器就可以像管理 Java堆一样管理这部分内存,而不必为方法区开发专门的内存管理器(永久带的内存回收的主要目标是针对常量池的回收和类型的卸载, 因此收益一般很小)。
#### 运行时常量(Runtime Constant Pool)
运行时常量是方法区的一部分。字节码文件(.class)中除了有类的版本、字段、方法、接口等描述等信息外，还有一项是常量池(Constant Pool Table),用于存放编译期间生成的各种常量(?)和符号引用。这部分内容在类加载后存放到方法区的运行时常量中。JVM对Class文件的每一部分的格式都有严格的规定，必须要符合他们的规定这样才能被JVM认可装载。





## GC机制是怎么判断对象可以被回收的
GC是JVM中的垃圾回收机制，会把不用的对象，变量回收来节省内存。
1. 引用计数法：每个对象都会有一个引用计数，当这个对象被引用时计数+1，减少一个引用时计数-1。当引用计数为0的时候就说明这个对象没有被任何东西引用，就可以被回收。但是当两个对象互相引用的时候，引用计数法就会失效。（A引用B，B引用A，A和B的引用计数永远都大于等于1）。
2. 可达性分析法：从GCRoots出发，向下搜索，经过的路程称为引用链。当一个对象不在引用链上的时候，就说明这个对象是可以被回收的。

### 什么是GCRoots
1. 虚拟机栈中引用的对象（比如正在执行的方法中，创建的对象。方法执行完了就出栈了就不是GCRoots了）
2. 方法区中类静态属性所引用的对象
3. 方法区中常量引用的对象
4. 本地方法栈中JNI（Java Native Interface）引用的对象

### 是否在可达性分析中判断不在引用链上之后就会直接被回收
不可达对象不是直接就被回收的，而是有一次“复活”机会。  
1. 第一次可达性分析之后，不可达对象如果没有重写finalize()，或者已经执行了finalize()。就不会直接进入F-Queue队列。
其他的进入F-Queue队列，并稍后在由虚拟机建立的低优先级Finalizer线程中触发该对象的 finalize()方法。
2. 第二次可达性分析之后，如果没进入finalizer队列又被标记了，就会被回收。如果在F-Queue队列中的对象在finalize()方法又被引用了那么就成功复活。其他的就回收掉。 
<img src="img/GCRoot.png">  

### finalize方法
finalize方法定义在Object中，我们可以重写这个方法来释放对象的各种资源。一般不需要去重写这个方法,只有当java调用了c或者c++申请了内存的时候才需要重写finalize来释放这部分内存。因为GC回收不会回收这部分内存。

## GC回收对象的几种方式
### 标记清除法(Mark-Sweep)
标记清楚法分为两个步骤，标记和清除。首先标记出需要清除的对象，然后回收被标记对象的空间。这种方法会造成内存碎片化。可能会导致后续的大对象没有足够的连续空间。  
<img src="img/标记清除法.png">      

### 复制算法(Copying)    
复制算法，按内存容量将内存划分为等大小的两块。每次只使用其中一块，当这一块内存满后将尚存活的对象复制到另一块上去，并回收这一块内存里的所有东西。  
这种算法虽然不会产生碎片内存，但是可用内容变成了原来的一半。且复制的过程比较耗费时间，存活对象多的话就很慢  
<img src="img/复制算法.png">  

### 标记整理法((Mark-Compact)  
标记整理法也分为标记和整理两部分。首先标记出可以被回收的对象，然后将幸存的对象移到内存的一端。之后回收端边界外的所有对象。可以利用完整的内存且不会产生碎片化内存。   
<img src="img/标记整理法.png">  

### 分代收集法
分代收集法是综合了上述的几个方法，他把堆分成了新生代和老年代，其中新生代还可以分为edan占新生带的0.8，surviviofrom和survivoto各占0.1。新对象都会被放到edan中，如果太大放不进的话，就会直接放到老年代去。当edan满了就会触发一次回收。在新生代会使用复制算法，将幸存的对象复制到survivoto中，并把它们的年龄+1，日过超过了15就放到老年代，太大的，放不到survivoto里面的也放到老年代里面。然后回收除了survivoto以外的区域，并且将survivoto和survivofrom互换。当老年代没有足够的空间分配给对象的时候，就会使用标记整理法进行回收。回收了也还是不够就会爆out of menory 异常了。

### CMS算法

CMS 采用的是标记清除算法，并且是并发执行的。但是并发也会导致几个问题

- 某个对象将要被当成垃圾回收时，工作线程中突然有一个引用准备指向它，导致**标记了不该回收的对象**。

- 某个对象在 GC 扫描时没有被当成垃圾，扫描过后又变成了垃圾，导致**没有标记到应该回收的对象**。

  CMS分为四个阶段：

  1. **初始标记**（Initial-Mark）阶段：在这个阶段中，程序中所有的工作线程都将会因为**STW**机制而出现短暂的暂停，这个阶段的主要任务仅仅只是标记出 GCRoots 能直接关联到的对象。由于直接关联对象比较小，所以这里的速度非常快。
  2. **并发标记**（Concurrent-Mark）阶段：从 GC Roots 的直接关联对象开始遍历整个对象图的过程，这个过程耗时较长但是不需要停顿用户线程，可以与垃圾收集线程一起并发运行。
  3. **重新标记**（Remark）阶段：为了修正并发标记期间，因用户程序继续运作而导致标记产生变动的那一部分对象的标记记录，这个阶段的停顿时间通常会比初始标记阶段稍长一些，但也远比并发标记阶段的时间短。
  4. **并发清除**（Concurrent-Sweep）阶段：此阶段清理删除掉标记阶段判断的已经死亡的对象，释放内存空间。由于不需要移动存活对象，所以这个阶段也是可以与用户线程同时并发的。

### G1算法

G1将Java堆划分为多个大小相等的独立区域（**Region**），一般是2048个。一般Region大小等于堆大小除以2048，比如堆大小为4096M，则Region大小为2M。 G1保留了年轻代和老年代的概念，但不再是物理隔阂了，它们都是（可以不连续）Region的集合。年轻代对堆内存的占比是5%，随着系统运行这个比例也会不断增加但是最多不会超过60%。一个大对象超过了一个Region的50%，它就会被放到**Humongous区**里面，Humongous区专门存放短期的大对象，不用直接进老年代，可以节约老年代的空间，避免因为老年代空间不够的GC开销。

* 什么场景用G1

  * 50%以上的堆被存活对象占用

  * 对象分配和晋升的速度变化非常大

  * 垃圾回收时间特别长，超过1秒

  * 8GB以上的堆内存(建议值)

  * 停顿时间是500ms以内

  * 比如说有一个每秒处理几十万消息的系统，由于每秒处理的消息大所以就需要大内存来支撑。内存一大GC起来就慢了

    [G1算法]: https://blog.csdn.net/xueyushenzhou/article/details/128507344	"G1算法"

    

    



# Java多线程
## 多线程的实现方式
1. 继承Thread类  
Thread类本质上来说也是runnable接口的一个实例。想要启动一个线程的唯一方法就是new一个Thread类，然后调用它的start()方法。start()方法会启动一个新线程并执行run()方法。
```
public class Main extends Thread{
    public void run(){
        System.out.println(1);
    }
    public static void main(String[] args) {
        Thread t= new Main();
        t.start();
    }
}
```
2. 实现Runable接口  
由于一个类只能继承一个类，所以当无法继承时可以通过实现runnable接口来创建多线程。  
实现了runnable接口后需要将该类的实例传入Thread类的实例中，然后调用Thread的start()方法才可以开启多线程。
```
public class Main implements Runnable {
    @Override
    public void run() {
        System.out.println(2);
    }
    public static void main(String[] args) {
        Runnable r=new Main();
        Thread t=new Thread(r);
        t.start();
    }
}
```
3. 实现Callable接口  
   Callable能接受一个泛型，然后在call方法中返回一个这个类型的值。而Runnable的run方法没有返回值  
```
public interface Callable<V> {
    V call() throws Exception;
}
```
就像Runable需要装载到Thread中使用一样，Callable也需要装载到线程池中使用。
```
ExecutorService pool = Executors.newFixedThreadPool(taskSize);
// 创建多个有返回值的任务
Callable c = new MyCallable(); 
pool.submit(c);
```
执行Callable后会返回一个,线程池会返回一个Future，调用Future的get方法就能获得线程的返回值。  
也可以使用FutureTask，将Callable装载进FutureTask中，再把FutureTask装载到Thread类里面。  
Future和FutureTask的get方法可以获取线程的返回值，如果没返回值就会阻塞直到运行完毕。
## 线程池
线程池就是线程的池子，有很多线程，但是数量不会超过池子的限制。有任务来的时候就派一个线程去执行。  
java自带4种线程池，顶级接口是Executor。  
1. CachedThreadPool：可以重用之前创建的线程（如果可以重用），并且会删除1分钟未被使用的线程
2. FixedThreadPool：一个可重用固定线程数的线程池，具体看下面。
3. ScheduledThreadPool：可以定时或者延迟执行任务的线程池
4. SingleThreadExecutor：单一线程池，但是当线程结束时会创建一个新线程来继续任务。


## 为什么使用线程池，并解释下参数
1. 降低资源的消耗，使用线程池之后就不用频繁的创建和销毁线程。可以降低资源的消耗。
2. 提高响应速度，有任务来了可以直接分配给线程池中的线程，不用重新创建线程。
3. 方便管理线程，都在线程池里面方便统一分配监控调优
* ``corePoolSize``：线程池中的核心线程数量，核心线程是常驻在线程池中的，除非线程池被销毁不然不会消除。
* ``maxinumPoolSize``：最大线程数量。当任务量过多，常驻线程处理不过来的时候。线程池会额外创建几个临时的线程来处理任务。但是总的线程数量不能超过``maxinumPoolSize``。
* ``workQueue``：任务队列，用来存放还没被执行的任务。只有核心线程都在工作的时候才会把任务放到``workqueue``中，当``workQueue``满了的时候才会创建临时线程。
* ``keepAliveTime``、``unit``：规定了临时线程的空闲存活时间，当临时线程的空闲时间超过设置的时间后就会被销毁。``setKeepAliveTime``，``unit``是时间的单位
* ``Handler``：拒绝策略，当线程池无法执行任务时，使用Handler指定的策略来执行线程。有两种情况下会：  
   * 当线程池线程数量已经达到了``maxinumPoolSize``，且``workQueue``也已经满了的情况下，又来了新的任务。这个时候线程池就已经没办法再处理这个任务了，就会拒绝执行这个任务。
   * 当线程池已经关闭，但是还有未执行完的线程再执行任务，这个时候提交任务请求就会被拒绝。
   * 直接抛出异常
   * 丢弃最老的任务，然后尝试重新提交新任务
   * 直接不管新任务了
   * 只要线程池未关闭，就在调用者线程中，运行当前被丢弃的任务。显然这样做不会真的丢弃任务，但是，任务提交线程的性能极有可能会急剧下降。
   ``ThreadFactory``：线程工厂，用来生产线程。默认的``ThreadFactory``生产出来的线程都不是守护线程，都在同一个数组中，有相同的优先级。也可以自定义线程工厂。
## 线程池的处理流程
<img src="img/线程池处理流程.png">

## 线程池为什么可以做到线程复用****
普通线程在创建的时候会指定一个线程任务，当执行完这个线程任务之后，线程自动销毁。但是线程池却可以复用线程，一个线程执行完线程任务后不销毁，继续执行另外一个线程任务。也就是说线程池将任务和线程解耦了，线程是线程，任务是任务。    
Thread类在调用start的时候其实调用的是Runnable对象的run()方法，也就是说其实真正要执行的任务是在Runnable对象里面的，Thread对象只是一个运行的载体。线程池重写了start方法，在start中不断循环获取Runnable对象，也就是要执行的任务。当没有任务的时候就阻塞。
## sleep(),join(),wait(),yeild(),start(),run()的区别
### 锁池
多个线程竞争同步锁时，未获得锁的线程都会在锁池中等待。当线程释放同步锁了之后，在锁池的中线程竞争这个锁，抢到的线程就会进入就绪队列等待cpu资源。
### 等待池
当调用``wait()``方法后，线程就会进入等待池中。等待池中的线程不会去竞争同步锁，只有其他线程调用了``notify()``,或者``notifyAll()``之后，等待池中的线程就会进入锁池去竞争同步锁。``notify()``是随机挑一个等待池中的线程进入锁池，``notifyAll()``是将等待池中的所有线程都放置到锁池中。
### sleep()和wait()
||sleep()  | wait() |
|--|--|--|
|来源|Thread类的方法  | object类的方法 |
|是否释放锁|不会释放锁|会释放锁|
|能否自行苏醒|超时后会自动苏醒|需要被其他线程唤醒（不指定时间的话）|
|用途|用于当前线程的休眠或者暂停轮询的操作|用于不同线程之间的通信|
|目标|作用于自身|作用于其他线程|

### yeild()和join()
* ``yeild()``方法是当前线程释放cpu资源然后转入就绪队列，不会释放锁。在cpu资源充足的情况下，``yeild()``用了和没用差不多。
* ``join()``方法是阻塞其他线程让调用``join()``的线程先运行.
```
   public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 500; i++) {
                    System.out.println(1111);
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 500; i++) {
                    System.out.println(2222);
                }
            }
        });
        t1.start();
        t1.join();
        t2.start();
    }
```
上面的代码就是先让t1执行完了t2才会执行。如果没有``join``那么t1，t2就会交替执行。  
``join()``方法就是插队，让我先执行完，其他的都往后稍稍。
### start()和run()的区别
* start()方法用来启动线程，不用等待run方法体里面的代码执行完毕，可以继续执行下面的代码
* run()方法会直接运行run方法体里面的内容，没有开启多线程二十顺序执行的。
* start()方法调用后，线程进入就绪状态，run方法调用后线程进入运行状态


## 线程的生命周期，线程有哪些状态
线程一般有5个状态：
1. 创建New：新创建了一个线程对象
2. 运行Running：该线程占据了cpu资源正在运行
3. 阻塞Block：线程因为某些原因放弃了cpu资源，暂时停止运行。
   * 等待阻塞：是说一个线程调用了``wait()``方法后，释放了所有的资源进入了阻塞状态，``JVM``会把他放置于等待池中。在这个状态下，该线程无法自行复苏只能通过其他线程调用``notify()``,或者``notifyAll()``来唤醒。``wait()``是``object``类的方法。
   * 同步阻塞：一个线程在获取其他同步锁的时候进入的阻塞状态。JVM会把该线程放到“锁池”中。
   * 其他阻塞：线程调用了``sleep()``、``join()``方法或者在等待I/O操作时，JVM会把该线程置为阻塞状态。当``sleep``到点了``join``等到线程结束或超时或者I/O结束时，该线程就会转入就绪状态。
4. 就绪Runnable：线程处于可以运行的状态但是还没有获得cpu资源。
5. 死亡Dead：执行完了或者出现了异常，线程销毁。

## 终止线程的4种方式
1. 运行完自然退出
2. 使用标识符来退出  
设置一个变量来控制run方法中的循环。通过修改这个变量来控制线程运行。
```
public class Main implements Runnable{
    public volatile int flag=0;
    @Override
    public void run() {
        while(flag==0){
            System.out.println(1);
        }
        
    }
}
```
使用volatile的原因时让flag变量同步，也就是让同时间只有一个线程修改flag。
3. Interrupt方法退出线程  
interrupt是给线程一个信号，改变线程中一个中断标识符，其实并不会改变线程的状态。真正改变线程状态的是后续因为这个标识符的改变进行的操作。  
* 当线程处于阻塞状态时，调用interrupt方法时，会抛出InterruptException异常。捕获这个异常然后使用break跳出循环即可。
* 当线程未阻塞时，使用isInterrputed()方法在循环条件中来判断是否被中断。被中断的就跳出循环
```
public class Main implements Runnable{
    public volatile int flag=0;
    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()){
            try {
                System.out.println(1);
            } catch (Exception e) {
                System.out.println("阻塞中断");
                break;
            }
        }
    }
}
```
1. stop方法
直接使用Thread.stop()来终止线程。stop会突然的释放锁，可能导致加锁数据的不一致。造成一些奇奇怪怪的错误。
## 后台线程
1. 定义：守护线程--也称“服务线程”，他是后台线程，它有一个特性，即为用户线程提供公共服务，在没有用户线程可服务时会自动离开。
2. 优先级：守护线程的优先级比较低，用于为系统中的其它对象和线程提供服务。
3. 设置：通过 setDaemon(true)来设置线程为“守护线程”；将一个用户线程设置为守护线程的方式是在线程对象创建之前用线程对象的setDaemon方法。
4. 在Daemon线程中产生的新线程也是Daemon的。
5. 线程则是JVM级别的，以Tomcat为例，如果你在Web应用中启动一个线程，这个线程的生命周期并不会和Web应用程序保持同步。也就是说，即使你停止了Web应用，这个线程依旧是活跃的。
6. 举例: 垃圾回收线程就是一个经典的守护线程，当我们的程序中不再有任何运行的Thread,程序就不会再产生垃圾，垃圾回收器也就无事可做，所以当垃圾回收线程是 JVM 上仅剩的线程时，垃圾回收线程会自动离开。它始终在低级别的状态中运行，用于实时监控和管理系统中的可回收资源。
7. 生命周期：守护进程（Daemon）是运行在后台的一种特殊进程。它独立于控制终端并且周期性地执行某种任务或等待处理某些发生的事件。也就是说守护线程不依赖于终端，但是依赖于系统，与系统“同生共死”。当 JVM 中所有的线程都是守护线程的时候，JVM就可以退出了；如果还有一个或以上的非守护线程则JVM不会退出。

## java锁
### 乐观锁
乐观锁认为读多写少，遇到并发的可能性很低。因此在读取数据的时候并不会加锁，但是在写入数据的时候会使用**CAS**或者**版本号机制**来判断是否是否被修改了。  
1. 版本号机制  
版本号机制给会被并发修改的对象加上了一个版本号**version**。当线程A和线程B都读取了该对象以及相对应的版本号version=1，**线程A修改了对象并要提交修改时会去比较对象当前的版本号和读取数据时的版本号是否相等。相等则更新，并将版本号version+1**.这时version=2，线程B也修改了对象数据并要提交更新时，也会去比较对象当前的版本号和读取数据时的版本号是否相等，因为之前线程A更新了version，**所以导致前后version不相等，所以提交失败需要重写读取数据，再走一遍读取、对比、更新的流程。**
2. CAS(compare and swap)  
CAS全称compare and swap，也就是比较和交换。CAS的操作需要三个数值，**需要修改的内存位置V，该位置原本的值A，预期修改成的值B**，当要修改时，V位置的值还是A，那么就可以修改成B。如果不是A那么久修改失败，重复上述过程。
* CAS的缺点
  * 只能保证一个共享变量的原子性，对于多个变量的时候就无法保证保证操作的原子性了，这个时候就可以使用锁来保证原子性
  * CAS是会反复重试的，在高并发的环境下。CAS会不停的重复尝试给CPU造成较大的开销。可以避免在高并发的情况下使用乐观锁，或者加一个最大重试次数，到达次数了就自动退出。
  * **存在ABA问题**  
  ABA问题就是，V所在的内存位置的值本来是A，然后被其他线程修改为B然后又被修改为A了。也就是虽然该位置的值没有变，但是还是被修改了。只不过刚好被修改的和原来一样而已。单单靠V所在内存位置的是否发生变化，无法判断该内存位置是否被修改过。在一些共享变量是int或者其他基本数据类型的情况下，ABA问题没有任何影响。但是当共享的变量是栈时，栈顶元素没有发生变化但是栈内元素可能已经发生了变化，这个时候CAS就不适用了。可以引入版本号机制，通过判断版本号和预期修改的值双重保险来解决ABA问题。
- 乐观锁本身是不加锁的，他是一种思想。只是在更新数据的时候会判断一下数据是否被其他线程已经更新过了。
- 当然有时候乐观锁也会和加锁的操作一起使用。（不说）
### 悲观锁
悲观锁认为写的情况比较多，因此不管是读还是写都会认为有其他线程正在更新数据，所以都会加锁。其他线程想读数据也得先拿到锁才行。在Java中，典型的悲观锁就是Synchronized。
### 悲观锁和乐观锁的适用场景
1. 功能限制  
和悲观锁相比，乐观锁的限制更多。在高并发的情况下，乐观锁的效率比较低。而且CAS和版本号机制都只能确保一个共享变量的原子操作。
2. 适用场景
乐观锁适用于有并发但是冲突较少的情况下，悲观锁适用于高并发的情况下。
### 自旋锁
自旋锁在没有抢占到锁的情况下也不会释放CPU，而是会等待一会后继续尝试获得锁。
* 优缺点
自旋锁这对于锁的竞争不激烈，且占用锁时间非常短的代码块来说能大幅度的提升性能，因为自旋的消耗会小于线程阻塞挂起再唤醒的操作的消耗，这些操作会导致线程发生两次上下文切换！  
但是如果锁的竞争激烈，或者持有锁的线程需要长时间占用锁执行同步块，这时候就不适合使用自旋锁了，因为自旋锁在获取锁前一直都是占用cpu做无用功，同时有大量线程在竞争一个锁，会导致获取锁的时间很长，线程自旋的消耗大于线程阻塞挂起操作的消耗，其它需要cup的线程又不能获取到cpu，造成cpu的浪费。所以这种情况下我们要关闭自旋锁。
### 公平锁与非公平锁
* 公平锁，顾名思义就是锁的争夺过程是公平的，先提出申请的线程先获得锁。
  * 优点：公平，不会有线程等待的很久
  * 缺点：效率比较低，即使有新的线程进入，cpu也需要唤醒等待队列的第一个线程。
* 非公平锁，争夺锁的过程不是公平的。也就说，先提出申请的线程不一定先获得锁。多个线程去获取锁的时候，会直接去尝试获取，获取不到，再去进入等待队列，如果能获取到，就直接获取到锁。
  * 优点：效率高，吞吐量大
  * 缺点：会导致一些线程的等待较长时间

### 可重入锁（递归锁）
顾名思义就是可以被多次获取的锁。当某个线程已经获得了某个锁，可以再次获取而不会出现死锁。递归是其中的一种表现形式。当然如果你多次获得了锁，也需要多次释放锁。Java中，synchronized和ReentrantLock都可可重入锁。
### 读写锁
读写锁是两种锁，一种是读锁，一种是写锁。按照需求来使用其中：
* 读锁：允许多个线程同时读取数据，但是不允许写入，是共享的。
* 写锁：允许单个线程写入，写入时不允许其他线程做任何操作，是独占的。
### 共享锁和独占锁
* 独占锁：每一次只能有一个线程持有锁。并发性比较差，效率不高。但是很安全。（ReentrantLock和synchronized都是独占锁）
* 共享锁：允许多个线程获得锁，并发的访问资源共享资源。比如ReadWriteLock。并发性较好，效率高。
### 重量级锁
基于操作系统底层**mutex Lock(互斥锁)**的，需要进行操作系统**用户态和内核态转变的锁**，我们都称为重量级锁。操作系统状态的转变需要的时间比较长，所以这也是重量级锁的效率比较低的原因。
### 轻量级锁
轻量级锁是在并发不太严重的情况下用来减少重量级锁带来的消耗的。使用轻量级锁时，**不需要向操作系统申请互斥锁**，仅仅将**Mark Word（一个保存对象锁状态的数据结构）**中的部分字节CAS更新，如果更新成功，则轻量级锁获取成功，记录锁状态为轻量级锁；否则，说明已经有线程获得了轻量级锁，目前发生了锁竞争（不适合继续使用轻量级锁），接下来膨胀为重量级锁。
### 偏向锁
在大多数情况下，锁不仅没啥竞争而且在经常由同一个线程多次获得。**而轻量级锁每次申请、释放锁都至少需要一次CAS，但偏向锁只有初始化时需要一次CAS。偏向锁的目的就是在某个线程获得了锁之后，消除多次CAS的消耗。**  
偏向锁会将对象头中的ThreadId,CAS为线程ID，重入时，如果ID相同就能直接获得锁。当有其他线程来竞争锁且失败的时候，就会升级成轻量级锁。
### 锁的优化方式
1. 锁消除：把那些不可能被并发访问的锁去掉
2. 减少锁的粒度，也就是不要把锁加在整个对象上，而是把一个大的对象拆分然后吧锁加在拆分出来的小对象上。比如currentHashMap。
3. 锁分离：根据功能把不同的锁分离出来，比如读写锁。还有就是队列的锁也可以分离成队头锁和队尾锁。
4. 锁粗化：遇到一连串的对同一个锁频繁的请求和释放时，可以把这些操作合并。避免频繁的请求和释放锁
### 死锁
死锁就是多个线程都被阻塞了，他们在等待某个被占有的资源的释放。但是占有这个资源的锁也被阻塞了，所以导致陷入了死循环。  
**造成死锁的条件**  
* 互斥条件：某资源只能一个进（线）程使用，其他无法使用要进行等待，直到资源使用完毕释放资源
* 请求和保持条件：即资源请求者在请求其他资源时，保持对原有资源的占有，并不会释放自己所占有的资源
* 不可抢占条件：资源请求者不能强制从资源占有者那里抢夺资源，只能从等待资源占有者释放资源
* 循环等待条件：存在一个循环依赖链，使得每个进程都占有下一个进程所需的至少一种资源。
### Synchronized锁
Synchronized可以吧任意一个非空的对象当作锁。是一个独占的悲观锁，也是可重入锁。  
1. 作用范围
* 作用于方法时，锁的是对象实例

* 作用于静态方法时，锁的是类的Class对象实例。也就是所有该类的实例都会被锁住

* 作用于对象实例时，锁住的时所有以该对象为锁的代码块。

#### Synchronized的实现原理

  修改对象头中某个标识来做到的。具体不太了解。


### ReentrantLock
ReentrantLock实现了Lock接口，是一种可重入锁。不仅实现Synchronized的所有功能外，还提供了定时锁、可相应的中断锁等一些避免死锁的方法。  
**Lock接口的主要方法**：  
*  void Lock():如果锁空闲就获得锁，不然就阻塞直到获得锁。
*  boolean tryLock(long timeout,TimeUnit unit):如果锁空闲就获得锁，然后返回true。不然就返回false。这个方法不会造成阻塞。传入时间参数后，就会在这段时间内超市获得锁，超时还没获得就返回false。
*  void unlock():释放锁，如果不持有锁会有异常
*  Condition newCondition():创造一个Condition对象，这个Condition对象和当前的线程绑定。用来进行线程之间的通信等操作。  
*  void lockInterruptibly():如果当前线程未被中断，就去获得锁。在等待锁的过程（阻塞）中可以被中断（interrupt()），会抛出InterruptException异常。

**ReentrantLock示例代码**
一个ReentrantLock可以对应对各Condition，每个Condition唤醒指定的线程。就是把线程分组了，每组对应一个condition对象
```
//使用多个Condition实现等待/通知部分线程
class application_ReentrantLock2 {
    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        Condition conditionA = lock.newCondition();
        Condition conditionB = lock.newCondition();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    lock.lock();
                    System.out.println(Thread.currentThread().getName()+"start await");
                    conditionA.await();//Condition的await()方法相当于Object的wait()方法，会释放锁;
                    System.out.println(Thread.currentThread().getName()+"end await");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    System.out.println(Thread.currentThread().getName()+"unlock");
                    lock.unlock();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    lock.lock();
                    System.out.println(Thread.currentThread().getName()+"start signal");
                    conditionA.signal();//signal()方法,会唤醒对应的await()线程。
                    System.out.println(Thread.currentThread().getName()+"end signal");
                } catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    System.out.println(Thread.currentThread().getName()+"unlock");
                    lock.unlock();
                }
            }
        });

        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    lock.lock();
                    System.out.println(Thread.currentThread().getName()+"start await");
                    conditionB.await();//Condition的await()方法相当于Object的wait()方法，会释放锁;
                    System.out.println(Thread.currentThread().getName()+"end await");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    System.out.println(Thread.currentThread().getName()+"unlock");
                    lock.unlock();
                }
            }
        });

        Thread t4 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    lock.lock();
                    System.out.println(Thread.currentThread().getName()+"start signal");
                    conditionB.signal();//signal()方法,会唤醒对应的await()线程。
                    System.out.println(Thread.currentThread().getName()+"end signal");
                } catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    System.out.println(Thread.currentThread().getName()+"unlock");
                    lock.unlock();
                }
            }
        });

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}

```
### synchronized和ReentrantLock的区别
1. ReentrantLock显示的获得释放锁，synchronized隐式的释放锁。
2. ReentrantLock更灵活是可相应中断的，synchronized不可以相应中断
3. ReentrantLock是API级别的，synchronized是JVM级别的。
4. ReentrantLock可以是公平锁
5. ReentrantLock可以绑定多个Conditon，做到唤醒指定线程
6. ReentrantLock发生异常时不会自动释放锁，要在finally中手动的释放。synchronized会自动释放锁
7. ReentrantLock可以知道有没有成功的获得锁，synchronized不行。
### Condition和Object方法的区别
* Condition.await()=Object.wait()
* Condition.signal()=Object.notify()
* Condition.singnAll()=Object.notifyAll()
* ReentranLock可以唤醒指定的线程，object是随机唤醒
## 阻塞队列
阻塞队列首先是一个队列，
* 队头是消费者。消费者会从队头中取出数据进行处理，当队空时消费者就会阻塞。  
* 队尾是生产者。生产者会将数据存入队尾，当队列满时，生产者就会停止生产。  
**方法**：
* boolean add():入队操作，成功返回true，不成功抛出异常（队满等原因）
* boolean offer():入队操作，成功返回true，不成功返回false。可以传入时间参数来设置时间
* void put():入队操作，会阻塞直到成功
* pool():获取队头数据，可传入时间参数，超时还取不到就返回false
* take():获取对头数据，会阻塞直到成功
* drainTo():一次性把队列里的数据都拿走，也可以传入参数设定个数。
## ThreadLocal
不同于synchronizd等锁的机制，ThreadLocal将每个线程之间的数据相互隔离。为每个线程都单独的提供了一份变量的副本，从而实现同时访问而互不相干扰。一般可以用来放用户的信息。
## 线程调度
1. 抢占式的调度  
这种情况下，每个线程的所获得的时间片多少由系统控制。可能每个线程都分到相同的时间片，也可能有些线程多，有些线程分不到时间片。**在这个调度模式下，一个线程的阻塞不会导致整个系统的阻塞。**  
JVM就是抢占式的调度，JVM会根据线程的优先级来分配时间片，但是高优先级也不能独占时间片，低优先级也不会分不到时间片。
2. 协同式的调度  
   在这个情况下，一个线程结束后，主动通知系统切换到下一个线程。所有线程是顺序执行的不存在并发的问题。**但是如果其中一个线程阻塞了就会导致整个系统的阻塞。**
## 进程调度
1. 先来先服务FCFS  
系统按照顺序来进行服务。系统会从就绪队列中获取最前面的进程服务。**算法简单，而且比较公平**
1. 短作业优先SJF  
系统会在就绪队列中选择一个预估处理时间最短的进程服务。**会导致长作业饥饿，而且没有照顾到紧急任务**
3. 高优先级优先  
* 非抢占式优先级算法：根据进程的优先级来服务
* 抢占式优先级算法：根据进程的优先级来服务，但是有更高优先级的出现时，会立即停止当前的进程先去服务更高的那个
* 高相应比优先：使用一个公式来描述进程的优先级  
  ![](img/xiangyingbi.png)  
  *  等待时间相等那么短作业优先
  *  处理时间相同那么先来先服务
  *  长作业的优先级也会随着等待时间的增加而增加
4. 时间片轮转
系统将所有的就绪进程按先来先服务的原则排成一个队列，每次调度时，把 CPU 分配给队首进程，并令其执行一个时间片。执行完后将该进程放到队尾。
5. 多级反馈  
![](img/duojifankui.png)  
## AQS(AbstractQueuedSynchronizer)
AQS抽象队列同步器，它定义了一套多线程访问共享资源的同步器框架。ReentrantLock就是依赖于AQS的。
它维护了一个 volatile int state变量来代表资源和一个FIFO的线程等待队列。  
他有两种共享模式
* 独占式：ReentrantLock
* 共享式：Semaphore（信号量）  
AQS只是一个框架，具体的资源的资源释放和获取需要自定义的同步器来实现。  
![](img/aqs1.png)  
![](img/aqs1.png)  
# Mybatis

## 流程
* 加载配置文件来生成sqlSessionFactoryBuilder
* 使用sqlSessionFactoryBuilder来创建sqlSessionFactory，这也是mybatis的核心，这个类在整个app的生命周期中会一直存在用来生产sqlSession。
* 之后每次和对数据库的操作都会由sqlSessionFactory创建对应的sqlSession。
* sqlSession会从对应的mapper里面读取sql然后对数据库操作
## sqlSession
SqlSession中存储的是编译好的sql语句，这些sql语句是从mapper的xml或者注解中读取的。
在没有使用事务的情况下，每一次数据库的会话都会生成一个sqlSession，会话结束后要销毁sqlSession。  
sqlSession是线程不安全的，所以不能将SqlSession实例的引用放在一个类的静态字段或实例字段中。（最好不说）  

## Mybatis一级缓存
``sqlSessionFactory``是``Mybatis``的核心，是线程安全的。每个新的查询都需要从``sqlSessionFactory``中获取``sqlSession``对象，然后使用``sqlSession``对象对数据库进行操作。而``Mybatis``的一级缓存就是对于``sqlSession``来说的。``Mybatis``会把一次查询的``mapperId+offset+limit+sql``所有参数作为key，查询结果作为value存到sqlSession的一个``map``中。当有重复查询时就会直接从``map``中取出数据返回，而不会去查询数据库。如果sqlSession执行了commit操作，也就是update，insert，delect操作，map就会被清空。但是mybatis的一级缓存时sqlSession级别的，不同的sqlSession，无法共享这个map。也就是说即便是相同的sql，在不同的sqlSession中也无法命中缓存。

- 使用spring整合了mybatis之后，在没有使用事物的情况下由于每次和数据库的会话都会重新生成sqlSession，使用完毕后spring会销毁sqlSession。所以在不适用事物的情况下，一级缓存和没有一样。
## Mybatis二级缓存
* 二级缓存是mapper级别的，以mapper为单位创建的HashMap作为缓存结构。不同的sqlSession可以共享一个缓存
* 默认是关闭的，要开启的话要在配置文件中添加cache标签。
* commit操作会清空二级缓存
* 多表查询会导致脏读，多表查询表A，表B的数据。然后更新了表B下一次查的时候还是缓存中的数据，因为表A和表B对应的不是同一个mapper，所以没有清空缓存。
* 而且返回的POJO要是可以序列化的（不说）
* 二级缓存的key是MapperId+offset+limit+sql+入参，value是查询到的信息
## #{},${}的区别
* #是占位符。使用#{}的时候，mybatis会将传入的参数当成一个字符串自动为他添加双引号。#可以防止sql注入
* $是一拼接。使用${}的时候，不会做任何处理直接把参数拼接到sql中。
  * 在传入数据库表名或者列名作为动态参数时使用，不然传进去的值带引号会报错

## 常用的标签



## 分页
1. sql limit语句分页  
limit a，b 表示从a开始查询b行。
2. RowBounds类实现分页  
在dao层的方法中传入一个RowBounds类，RowBounds中有2个字段offset和limit。最后的查询结果就是offset开始的limit个数据。实际上这个是把所有数据都查出来然后在内存中在对数据进行操作。在数据量大的时候不怎么好用。
3. [自定义拦截器来进行分页](http://events.jianshu.io/p/2f32d34e9d71)  
建立一个Mybatis拦截器用于拦截Executor接口的query方法。在拦截之后通过ParameterHandler来获取参数。如果有分页请求对象就重新拼接sql，否则按原来方式执行。实际查询的时候还是用的sql的limit语句。

## 数据库连接池



## Mybatis的ORM

ORM对象关系映射是将java中的对象映射到数据库中对象映射成数据库中的记录。

# Mysql

## 关系型数据库和非关系型数据库的区别
1. 存储方式不同：关系型数据库一行一行的存储在表格里面的，彼此关联，而非关系型数据库的存储方式多种多样有k-v形式的redis，也有图数据库等等，通常大块的组合在一起。
2. 事务不同：关系性数据库有事务的功能，而非关系型数据库没有。Redis的每个操作都是原子性的。
3. 数据库的结构也不一样：关系型数据库的结构是预先定义好的。就是说你得先把表建好，设置号主键，索引等才能去往里面存数据。非关系型数据库就没有表这种概念可以直接往里面添加新数据。
### 关系型数据库的优缺点
* 优点
  * 易于维护：都是使用表结构的，格式一样容易维护
  * 使用方便：可以使用sql语言，可以支持复杂的查询
* 缺点
  * 读写性能差，尤其是大量数据的高性能读写
  * 表结构固定，**灵活程度低**
  * 由于存在硬盘中，高并发读写时**IO**是一个很大的瓶颈
### 非关系型数据库的优缺点
*  优点
   *  存储结构灵活：可以是k-v形式也可以是图，json等
   *  速度快：非关系型数据库可以是存储在内存中，读取速度比硬盘快得多
   *  扩展性好，没有固定的表结构可以随时加东西
* 缺点  
  * 不支持sql
  * 没有事务
  * 数据结构多变，复杂查询不方便
## 索引
索引是用来帮助关系型数据库快速找到目标数据的一种数据结构。他是添加在字段上的，每个字段都可以添加索引，也可以多个字段联合添加一个索引。**主键上会自动添加索引，mysql中有唯一约束的字段也会自动添加索引**
### 索引的数据结构
1. 哈希表  
哈希索引可以直接在O(1)的时间范围内查找到目标数据。但是哈希表为数据结构的索引
* 不支持排序
* 不支持部分列索引查找
* 不支持范围查找功能
2. B+树  
B+树是B树的进化版本，他**非叶子节点上只存储键值**，不存储数据，这样一来，在**固定（16kb）**的节点空间（页空间）内就可以**存放更多的键值、指针**，然后**所有数据都放在叶子节点中**，所有叶子节点之间有**链指针（双向循环列表）**，便于范围查找，也便于排序。  
* 优点：
  *  由于非叶子结点只存了键值了，所以树就会更矮更胖。查找数据时候的磁盘IO次数就会少。
  *  叶子结点用双向链表连接，而且数据是有序的。所以支持排序、范围查找等。
  ![](img/b%2B%E6%A0%91.png)  
### 索引类型
#### 聚簇索引
聚簇索引，一般来说指的是主键索引。在InnoDB中聚簇索引是使用B+树实现的。聚簇索引B+树的叶子节点中，存储的是**行数据**，也就是说你通过聚簇索引找到目标数据之后可以**直接读取**。一旦创建了聚簇索引之后，表里面的数据都会按照聚簇索引的关键列来排序，**所以数据在物理层面上的顺序就是聚簇索引的顺序**。
#### 非聚簇索引
非聚簇索引B+树的叶子节点中存放的是**聚簇索引**（一般是主键），或者说**行号**，之后还需要通过聚簇索引或者行号再去查找一遍才能在最终得到目标数据。

> 那为什么要有非聚簇索引呢？  
> 因为聚簇索引只能有一个，所以在根据其他列作为索引进行查询时就只能使用非聚簇索引了。

#### 主键索引
主键索引在innodb中就是聚簇索引，主键索引的叶子节点保存了完整的信息不需要回表查询。主键索引中数据的排列顺序，就是数据在物理盘中的排列顺序。
#### 唯一索引
唯一索引的索引字段不可重复，但是可以有多个null值。
#### 普通索引
就是b+数中的叶子节点寸的是主键或者行偏移量，想要获得完整数据还得根据主键或者行偏移量再查一次。
#### 联合索引
将两个字段联合起来作为b+数的排序标准。比如拿``name、phone``作为联合索引。那么在b+数的顺序中就是先比较name，在name相同的情况下在比较phone，phone也一样就比较主键。
#### 主键索引和唯一索引的区别
* 主键索引一定是唯一索引，但是唯一索引不一定是主键索引
* 唯一索引可以有多个，但是主键索引只能由一个。
* 主键索引是不允许空值的，唯一索引允许空值

### 索引失效的原因

1. 在使用联合索引的时候，不符合最左匹配原则
2. 在查询的时候使用了 like %，而且%在前面


## 事务
事务是把一系列操作作为同一个逻辑单元来执行。这些操作作为一整体来执行，要么都执行成功，要么都执行失败。执行失败的时候，会回滚到事务发生前的状态。
### 事务的四个属性ACID
1. 原子性Atomicity  
事务是一个整体，要么都执行，要么都不执行
2. 一致性Consistency  
当事务完成时，数据必须处于一致状态，也就是说，数据库中的数据都是已经提交后的结果。
3. 隔离性Isolation  
事务之间是彼此隔离，独立的，事务与事务之间不会相互影响。隔离性可以防止多个事务并发执行式导致的数据不一致问题。
4. 永久性Durability  
事务完成后，事务对数据库做的更改会被永久保存。
### 脏读、幻读、不可重复读
由于事务并发执行可能会导致数据不一致的问题
* 脏读  
事务A读取到了事务B还未提交的数据。

|事务A|事务B|
|--|--|
|select a=10||
|update a=100||
||select a=11|
|commit 失败，a回滚到10||

事务A一开始从库中获取到a=10，然后将a更新为11，此时事务B读取到a=11.但是事务A的更新提交失败了，导致事务B读取到了错的的值。  
* 幻读  
事务A两次查询同一个数据，但是由于事务B在事务A第二次查询前插入或者删除了数据，导致事务A两次查询的数量不一致。  

|事务A|事务B|
|--|--|
|select * ||
||insert|
||commit|
|select *||
|commit||

假设事务A第一次查询到的是x，那么第二次查询到的就是x+1。  
* 不可重复读  
某个事务对同一个数据前后两次查找的数值不一样。   

|事务A|事务B|
|--|--|
|select a=10||
||update a=11|
||commit|
|select a=11||

事务A第一次读取时a=10，但是第二次读取前事务B更新了a，导致事务A前后两次读取的数值不一样。  
**幻读和不可重复读很像，幻读表示的是前后两次查询的数据数量不一致。不可重复读指的是前后两次查询的数据内容不一致**
### 事务的隔离级别
可以通过设置事务的隔离级别来防止脏读、幻读、不可重复读  
1. Read Uncommitted（读未提交）  
这个隔离级别中，事务可以读取其他事务为提交的数据，会导致脏读、幻读、不可重复读一般情况不会用
2. Read Committed(读已提交)  
在这个隔离级别下，事务只能看见其他事务已经提交的内容。可以解决脏读的问题，但还是会有幻读和不可重复读的问题出现，
3. Repeatable Read（可重复读）  
在这个隔离级别下，事务会对尧都区的数据加上读锁，且在事务结束前都不会释放锁。因此解决了不可重复读的问题，但还是会有幻读的问题。**在innodb中，Repeatable Read，的实现不是使用加锁，而是MVCC机制（多版本并发控制）。他会生成一个数据快照，事务读取的是数据快照，也就是数据库中老的数据。其他事务对数据库的修改并不会影响到这个快照。因此innodb的Repeatable Read可以解决幻读问题**
4. Serializable（串行化）  
这是最严格的隔离级别。事务是顺序执行的（给数据行加上了排他锁），因此也就没有了并发问题。但是可能导致大量的锁竞争和超时。
5. 总结  

|隔离级别|脏读|不可重复读|幻读|
|--|--|--|--|
|Read Uncommitted|有|有|有|
|Read Committed|无|有|有|
|Repeatable Read|无|无|有（innodb没有）|
|Serializable|无|无|无|


# Redis
## Redis缓存过程
Redis数据库是一个nosql数据库，存储的数据格式是key-value。Redis数据库运行在**内存**中，因此他的查询速度比MySql**快的多**。所以我们会把一些用户经常查询的数据放在Redis中，当Redis有的时候就直接返回，当Redis中没有的时候再去数据库中查找。以此增加服务的运行效率。
![在这里插入图片描述](https://img-blog.csdnimg.cn/9d9bc8b9965740f59abbfa3f109f696c.png#pic_center)

## Redis持久化
### RDB（redis database）
RDB的方式就是把数据以快照（当前时刻的数据）的形式放在硬盘中。存在一个默认名为dump.rdb的二进制文件中。
#### RDB实现方式
1. 使用save来进行RDB操作会导致redis在保存数据时阻塞，无法响应客户端请求（所以一般也没人用他）
2. 使用bgsave来进行RDB操作时，redis会fork一个子进程来进行数据的保存。redis可以继续相应客户端的请求不会阻塞（在fork子进程的时候会阻塞一会会）
3. 根据情况自动保存，可以在配置文件中修改配置做到在m秒内出现了n次数据变动就自动使用bgsave进行RDB操作
### AOF（append only file）
aof操作会把redis的每次操作都添加到aof文件中。AOF有三种方式，每次修改追加、每秒追加和不追加
#### AOF重写机制
随着时间增长，AOF文件的大小也会随之变大。redis就提供了重写机制来给AOF文件瘦身。
redis会根据现有数据来生成一个新的AOF文件，在生成新AOF文件的过程中，redis也在相应客户端的请求，在这个过程中会产生新的数据。这个数据会存在缓存中。等新的AOF生成完了，再把缓存中的数据加过去。再然后用新的AOF文件替换老的AOF文件。这样就获得了一个新的，更小的AOF文件。
#### AOF和RDB优缺点
1. RDB只能保存当前的快照数据，如果在RDB保存后redis服务宕机就会导致上一次保存完的时间点到宕机的时间点内的数据丢失。
2. RDB数据恢复的速度比AOF快
3. AOF文件比RDB文件大
4. AOF的IO开销比RDB大
### 混合持久化
混合持久化就是同时应用了AOF和RDB的持久化方式，在redis4中引入。
上面说了AOF的重写机制，混合持久化就是在重写阶段将前面的全量（所有）数据以RDB的形式写入AOF文件，然后再将后面的增量数据（缓存区里面的）以AOF的形式写入AOF文件“缝合怪" 文件形式是这样 **{[RDB],[AOF]}**

## Redis主从复制  
一般在高并发的环境下，单台服务器扛不住这么大的压力。使用多个Redis服务。他们分为主数据库和从数据库。**从数据库是一般只读，然后它会接受主数据库同步过来的数据**。一个主数据库可以对应多个从数据库。一个从数据库只能有一个主数据库。  
主从复制的过程分为三部分，一个是全量复制用于新建立的从节点，一个是增量复制用于连接断开后重连的从节点，一个是命令传播，这个是正常情况下建立连接后的主从之间的信息传递方式。
* 全量复制
  * 新建立一个从数据库时，主节点会启动一个后台线程，开始生成一份RDB快照文件。生成期间写入的数据会存在内存缓存中。
  * RDB生成后会发送给从数据库，从数据库再从RDB文件中加载数据到内存
  * 主数据库将缓存中的数据发送给从数据库
* 增量复制
  * 从节点发送偏移量给主数据库
  * 主数据通过对比偏移量将增量数据发送给从节点
* 命令传播
  * 维持一个长连接，将主数据库的命令操作同步给从数据库。
## 哨兵机制
当主节点宕机时，就需要将从节点切换为主节点来防止系统无法写入。但是人工切换会有一段时间的延迟，导致系统在一定时间内不可用。哨兵机制可以解决这些问题。  
客户端在建立连接时会先访问哨兵，通过哨兵拿到主节点的地址后，在连接主节点进行后续操作。当哨兵检测到主节点宕机后，会重新推选出某个表现良好的从节点成为新的主节点，然后通过发布订阅模式通知其他的从服务器，让它们切换主机。  
![](img/%E5%93%A8%E5%85%B5.png)  
## redis分布式锁
实际的应用场景中，服务基本上都是分布式部署的。分布式锁就是用来处理多个服务器之间的同步问题。redis可以实现分布式锁。
![](img/redis%E5%88%86%E5%B8%83%E5%BC%8F%E9%94%81.png)  
使用``setnx key value``命令，如果key不存在就会设置它的值，如果存在就不操作。当多个服务器同时使用这个命令时，只有一个服务器可以获取到锁。获取到锁的服务器，任务结束后，删除key表示释放锁。
### 避免死锁
当服务器A获取到锁后，A宕机或者出异常了，导致没有人去删除key。进而让其他的服务器都无法获得锁，进入死锁状态。  
1. 在finally中删除  
把使用到redis锁的代码块 try catch finally。在finally中删除redis的锁。这种方式只能解决出现异常的情况，服务器宕机的情况无法解决。
2. 看门狗“自动续时”  
可以通过给key设置一个自动过期时间，并开启一个“守护线程”“看门狗”来监控服务器是否完成了业务，如果没有完成就续一段时间。

### RedLock
当**有多个redis服务器时**，主节点宕机导致有一部分数据没有及时同步到从节点，服务A在主节点宕机前获得了锁，服务B在主节点宕机，主从切换之后也获得了锁，就会导致有两个服务都获得了锁。此时的分布式锁就是不安全的。  
这个时候就可以使用RedLock。   
RedLock至少需要5个实例，而且每个都是master，不需要从库和哨兵。  
1. 服务获取时间戳T1
2. 服务依次向master发起加锁命令，并设置超时时间。
3. 如果成功的实例一半以上，获取时间戳T2.**如果T2-T1<锁的过期时间**，即为加锁成功。
4. 结束后向所有的redis节点发送所释放命令。  
即当服务在大多数redis实例上申请加锁成功后，且加锁总耗时小于锁过期时间，则认为加锁成功。  
* 缺点
  * 至少要5个redis实例，成本高
  * RedLock太重了，效率低
  * 有NPC问题
### 分布式系统中的NPC问题
* N：Network Delay，网络延迟
* P：Process Pause，进程暂停（GC）
* C：Clock Drift，时钟漂移
这三个问题，都是因为时间的原因导致分布式锁失效。
比如：  
* 服务 1 请求锁定节点 A、B、C、D、E
* 服务 1 的拿到锁后，进程暂停了
* 所有 Redis 节点上的锁都过期了
* 服务 2 获取到了 A、B、C、D、E 上的锁
* 服务1暂停回来继续执行（他还认为自己获得了锁）
* 服务2也认为获取到了锁，发生**冲突**
![](img/NPC%E9%97%AE%E9%A2%98.png)  
## 数据类型
### 基本数据类型
1. String
2. Hash
3. Set，提供了交集并集方法。可以用来实现共同好友、共同关注等功能
4. List
5. SortedSet，有序的Set。适合用来实现排行榜或者优先级消息队列等功能
### 特殊数据类型
1. Bitmap，位图一种二进制的数组，数组元素只能是0或者1。由于最小的单位是位，所以占用的内存很小，可以用来实现布隆过滤器。
2. Hyperloglog，基数统计的数据结构。输入数据很大时需要的空间也是固定较小的。但是只能算出一个大概的数，并不准确
3. Geospatial：存地理信息的。
## 如何保证缓存和数据库数据一致
在高并发的环境下，由于各个线程并不是顺序执行。因此有可能会出现Redis和mysql数据不一致的问题。
### 延时双删
1. 先删除缓存
2. 修改mysql
3. 阻塞一会
4. 第二次删除缓存
> 为什么要阻塞一会呢？  
> 如果不阻塞直接把缓存删除了，就会导致其他线程在你第二次删除后又将脏数据写入到redis缓存了。  

缺点：
* 会有一段时间的线程阻塞
* 在线程阻塞的时间段中还是会存在数据不一致的问题
### 监听Mysql的binlog
binlog是mysql的一个二进制日志文件，它记录了mysql的所有变化。  
这个的具体操作就是先更新mysql，然后删除redis。
1. 监听binlog
2. mysql有更新时，binlog也会有变化。
3. 将该变化发送到消息队列中
4. 消费消息队列中的任务
> 为什么不在具体的接口中王消息队列中放任务，而是监听binlog？  
> 因为更新数据库的接口有很多个，一个个加太麻烦，而且如果后续需要修改就更麻烦了。这样代码的耦合度就比较高。  

缺点：
* 但是在将队列里的缓存更新任务处理掉之前还是会有脏数据。
### 总结
其实上面的两个方法都没有能做到实时的让Redis和mysql同步。所以在一切极端的情况下，一定要做到数据同步不能错的话。就直接将那些数据放在redis中，然后定期往mysql中更新吧。（比如双11的0点）



## 缓存雪崩
Redis中的缓存数据是有过期时间的，当在**同一时间大量的缓存同时失效时就会造成缓存雪崩**。
比如说，在11点的时候大家都去饿了么点外卖，这个时候饿了么的Redis中就存了一大批商家的信息，并且饿了么的程序员给这个缓存设置的过期时间是6个小时。那么到下午5点晚饭时间又是一大波人来饿了么点外卖，这个时候Redis的缓存刚刚好集体过期了，短时间内大量的查询请求就全部落到了脆弱的MySql上，导致MySql直接爆炸！
![在这里插入图片描述](https://img-blog.csdnimg.cn/a89bb1ef5f1d44a0bb756529d94b9546.png#pic_center)
### 解决方案
要解决Redis的缓存雪崩就需要**避免Redis的缓存在短时间内大量的过期**
#### 永不过期
设置Redis中的key永不过期，但是这样会占用很多服务器的内存。
#### 合理的设置过期时间
根据业务需要来合理的设置过期的时间。但是架不住有一些突发的情况。
#### 使用Redis的分布式锁
既然一瞬间大量请求落到MySql上会导致MySql爆炸！那么就加一点限制，让一时间只有**一个相同请求**落到MySql上，反正都是查询同一个信息，之后的其他请求就可以去Redis中找了。
![在这里插入图片描述](https://img-blog.csdnimg.cn/40bb4008e3b34f828210c5cbdb0815d8.png#pic_center)


## 缓存穿透
Redis缓存穿透指的是，**在Redis缓存和数据库中都找不到相关的数据**。也就是说这是个非法的查询，客户端发出了大量非法的查询 **比如id是负的** ，导致每次这个查询都需要去Redis和数据库中查询。导致MySql直接爆炸！
![在这里插入图片描述](https://img-blog.csdnimg.cn/98ed86e794f849d0b72d9f375ac0a786.png#pic_center)
### 解决方案
#### 过滤非法查询
在后台服务中过滤非法查询，直接不让他落到Redis服务上。比如**id<=0**或者**分页内容过大的**等
#### 缓存空对象
如果他的查询数据是合法的，但是确实Redis和MySql中都没有，那么我们就在Redis中储存一个空对象，这样下次客户端继续查询的时候就能在Redis中返回了。**但是，如果客户端一直发送这种恶意查询，就会导致Redis中有很多这种空对象，浪费很多空间**
#### 布隆过滤器
布隆过滤器由一个二进制数组和k个哈希数组组成。
##### 布隆过滤器的新增
当我们想新增一个元素时（例如新增python），布隆过滤器就会使用hash函数计算出几个索引值，然后将二进制数组中对应的位置修改为1。
![在这里插入图片描述](https://img-blog.csdnimg.cn/13d108767af643f78397a3c6037af29d.png#pic_center)
##### 布隆过滤器的查询
当我们想查询一个元素时（例如查询python），布隆过滤器就会使用hash函数计算出几个索引值，然后查询二进制数组中的对应位置是否**都为1**。如果都为1就说明改元素存在。**但是布隆过滤器存在误判的可能性**，因为不同的元素hash后的值可能是一样的，例如我们查询java，java经过hash计算出来的索引值和python的一模一样，那么就会认为java也在布隆过滤器中。
![在这里插入图片描述](https://img-blog.csdnimg.cn/1bc7e1a2cc2b4b49b58fc5ed3ea4adc3.png#pic_center)
##### 布隆过滤器的删除
同上，布隆过滤器删除就是把hash后数组对应的位置改成0.但是存在误删的可能。按照上面的例子删除python就会同时把java给删掉。
##### 布隆过滤器解决缓存穿透
我们首先把MySql中的数据存到布隆过滤器中（由于使用二进制数组，也就是位图所以空间使用很少），之后如果Redis缓存中没有命中，就需要查询MySql数据库前先在布隆过滤器中查询是否在MySql有数据。
![在这里插入图片描述](https://img-blog.csdnimg.cn/e80e1f413c244e67a45cc42145cab936.png#pic_center)
##### 布隆过滤器的特点
1. 存在误判的可能性
2. 如果数据存在，那么一定返回true
3. 查询的时间复杂度是O(k),k为hash函数个数
4. k越大，数组长度越大，误判的可能性越低
5. 使用位图（二进制数组）所以内存压力较小
## 缓存击穿
缓存击穿和缓存雪崩类似，也是因为Redis中key过期导致的。只不过缓存击穿是某一个热点的key过期导致的。当有一个热点数据突然过期时，就会导致突然有大量的情况直接落到MySql上，导致MySql直接爆炸！
![在这里插入图片描述](https://img-blog.csdnimg.cn/76d3984e034e43a481f7450a1aa402b7.png#pic_center)
### 解决方案
主要是两个思路，
1. 让那个热点的key不要突然过期
2. 不要让大量的请求落到MySql上。
#### 设置热点Key永不过期
简单粗暴，我都不过期了，你就不可能绕开我去访问MySql。**但是可能会对Redis内存造成巨大的压力，所以一般会设置一个较长的时间。**
#### 使用Redis的分布式锁
既然一瞬间大量请求落到MySql上会导致MySql爆炸！那么就加一点限制，让一时间只有一个相同请求落到MySql上，反正都是查询同一个信息，之后的其他请求就可以去Redis中找了。
![在这里插入图片描述](https://img-blog.csdnimg.cn/40bb4008e3b34f828210c5cbdb0815d8.png#pic_center)

# Spring
## IOC
IOC控制反转，是一种如何管理对象的编程原则。他把之前由开发人员控制的对象创建、赋值、生命周期管理都反转交给容器来管理。降低了代码耦合度。  
在Spring中，使用DI也就是依赖注入，来实现IOC的功能。  
Spring在启动时，会读取xml文件或者注解等地方的Bean的配置信息，然后根据这个配置信息使用反射实例化这些Bean，并存在一个HashMap中，等待使用。
## AOP
AOP，面向切面编程。他把日志、错误处理等和业务无关，但是被很多业务模块共同调用的逻辑或责任封装起来，便于减少系统的重复代码，降低模块之间的耦合度。   
### AOP的两种代理模式
AOP有两种代理模式，一种是JDK代理，一种是Cglib代理。
1. JDK代理  
jdk代理主要是使用反射来创建代理类的对象，但是jdk动态生产的代理类$ProxyO会继承reflect.Proxy，同时又会去实现被代理类的接口。而且java不支持多继承。所以说，要使用jdk动态代理的话，代理对象必须实现了一个接口。不然就需要使用Cglib代理了。
1. Cglib代理
CGlib在运行期间可以动态生成新的class文件。与JDK动态代理相比，CGlib可以代理类。
## 静态代理和动态代理的区别
* 静态代理，就是在系统运行前。由开发人员预先定义好了的代理对象。在需要代理的时候直接拿来调用即可
* 动态代理，就是在系统的运行期间，由系统动态的创建代理对象。而不是预先定义好的。
### 优缺点
* 静态代理要代理的对象很多的时候，就需要预先创建一大堆代理对象。重写一大堆方法很麻烦
* 如果被代理的接口增加了一个方法，就需要在所有的实现类里面都增加这个方法，增加了代码耦合度。

## Bean的作用域

## Bean的生命周期
![](img/bean%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F1.png)  
![](img/bean%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F2.png)  
![](img/bean%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F3.png)  

## SpringMVC的实现流程
![](img/SpringMVC%E6%89%A7%E8%A1%8C%E6%B5%81%E7%A8%8B.png)    
1. 用户发送请求至前端控制器DispatcherServlet
2. DispatcherServlet收到请求调用处理器映射器HandlerMapping。
3. 处理器映射器根据请求url找到具体的处理器，生成处理器执行链HandlerExecutionChain(包括处理器对象和处理器拦截器)一并返回给DispatcherServlet。
4. DispatcherServlet根据处理器Handler获取处理器适配器HandlerAdapter执行HandlerAdapter处理一系列的操作，如：参数封装，数据格式转换，数据验证等操作
5. 执行处理器Handler(Controller，也叫页面控制器)。
6. Handler执行完成返回ModelAndView
7. HandlerAdapter将Handler执行结果ModelAndView返回到DispatcherServlet
8. DispatcherServlet将ModelAndView传给ViewReslover视图解析器
9. ViewReslover解析后返回具体View
10. DispatcherServlet对View进行渲染视图（即将模型数据model填充至视图中）。
11. DispatcherServlet响应用户。
## SpringBoot
SpringBoot是用来简化Spring应用的初始搭建和开发过程的。他内嵌了Tomcat，不用部署WAR文件，简化了Maven配置，不需要进行xml配置。
### 为什么SpringBoot配置简单
1. SpringBoot的pom文件配置更简化：我们只需要在 pom 文件中添加starter依赖就行了，无需像以前一样引入很多依赖而造成容易漏掉。
2. SpringBoot有一个@EnableAutoConfiguration注解，顾名思义他的作用就是开启自动配置。他会从配置文件中读取相关参数然后自动配置类。
3. SpringBoot提供了一些很实用的功能，比如健康检查和指标之类的。
### SpringBoot事务
SpringBoot提供了声明式事务和编程式的事务两种事务。  
要开启事务，首先要在启动类中使用@EnableTransactionManagement注解，开启了事务支持。
1. 声明式事务  
声明式事务需要使用@Transaction注解，要访问数据库的方法（Service）上加上@Transaction注解便可。如果@Transaction注解是加在类上的，那么这个类的所有方法就都是支持事务的。  
2. 编程式事务  
编程式事务不使用@Transaction注解，而是使用事务管理器对事务进行管理。要new一个事务管理器（jpa，DataSource等），然后调用它的commit、rollback来进行提交回滚等操作。
#### 传播机制
事务的传播机制是针对事务嵌套的，总共有7中传播方式
1. required  
required是springboot的默认传播机制，**如果当前的方法已经在一个事务中了就加入这个事务，否则就开启一个新的事务。**
外层提交了，内层才会提交。  
内外层只要有一个报了错就会同时回滚。  
2. requires_new  
每次都创建一个新的事务，如果已经在事务中了就会挂起当前事务。  
内层正常执行完毕就可以提交不用等外层。  
**内存报错如果外层try-catch处理了外层不会回滚。如果外层没有处理外层回滚。**外岑回滚不影响内层。
3. nested  
如果外层有事务了就嵌套，不然就新开一个事务。  
内外层要一起提交  
外层回滚，内层也回滚。  
内层异常且没有被外层捕获的话，也会一起回滚。  
4. supports  
顾名思义就是支持事务，当前如果有事务就加入，没有事务的话就算了不会开启一个新事务。
5. mandatory  
强制要求事务，有事务就加入。没事务就报错。
6. not_supported  
不支持事务。如果这个方法在事务中，就会先挂起事务，等该方法执行完毕，事务再继续执行。  
7. never  
比上面那个更极端，如果在事务中就直接抛出异常

## [常用注解](https://blog.csdn.net/qq_44693065/article/details/124423025)



# Sql
## 左连接、右连接
简单的说就是，左连接中where语句只影响右表，右连接中where语句只影响左表。
### 左连接
select * from tbl1 Left Join tbl2 where tbl1.ID = tbl2.ID  
最终结果是table1中所有符合条件的的数据
### 右连接
select * from tbl1 Right Join tbl2 where tbl1.ID = tbl2.ID  
最终结果是table2中所有符合条件的的数据  

# kafka

## 什么是kafka

kafka是apache开源的一款消息中间件。它具有高吞吐量、低延迟、**高并发**等特点。它由Producer、Consumer、Topic、Partition、Broker等组成。可以用他来实现

**服务解耦**（多个服务之间相互调用和依赖会让整个系统越来越难以维护。可以将要传递的信息丢在消息队列里面，之后那个服务要就订阅这个主题就行了），

**异步处理**（一个完整的业务流程可能很长，要完整的处理完的话用户就需要等待很长时间，如果后续的流程不需要响应的那么及时的话，就可以把之后的流程都先放到消息队列里面，先把结果返回给用户。坐地铁，刷手机是先开门后扣费）、

**流量控制**（当一瞬间来了一大波请求的时候后端处理不过来时，可以用消息队列来做一个缓冲，把请求存储在队列中让后端慢慢的取。存的久了就直接返回超时。）等功能。

**Producer** - 消息生产者，就是向kafka broker发消息的客户端。

**Consumer** - 消息消费者，是消息的使用方，负责消费Kafka服务器上的消息，非线程安全。

**Topic** - 主题，由用户定义并配置在Kafka服务器，用于建立Producer和Consumer之间的订阅关系。生产者发送消息到指定的Topic下，消息者从这个Topic下消费消息。

**Partition** - 消息分区，一个topic可以分为多个 partition，每个
partition是一个有序的队列。partition中的每条消息都会被分配一个有序的
id（offset）。partition之间的数据是不重复的。

**Broker** - 一台kafka服务器就是一个broker。一个集群由多个broker组成。一个broker可以容纳多个topic。

**Consumer Group** - 消费者分组，用于归组同类消费者。每个consumer属于一个特定的consumer group，多个消费者可以共同消息一个Topic下的消息，每个消费者消费其中的部分消息，这些消费者就组成了一个分组，拥有同一个分组名称，通常也被称为消费者集群。

**Offset** - 消息在partition中的偏移量。每一条消息在partition都有唯一的偏移量，消息者可以指定偏移量来指定要消费的消息。

## 怎么保证消息不丢失

可以从生产、存储和消费消息三个阶段入手。

**生产消息**阶段生产者发送消息至 Broker ，需要处理 Broker 的响应，不论是同步还是异步发送消息，同步和异步回调都需要做好 try-catch ，妥善的处理响应，如果 Broker 返回写入失败等错误消息，需要重试发送。 当多次发送失败需要作报警，日志记录等。 这样就能保证在生产消息阶段消息不会丢失。

**存储消息**阶段要在消息要在写入了本机和副本机之后再给生产者返回发送成功的相应。这样一台挂了还有一台副本机。

**消费阶段**在取出消息完成了业务流程之后再给broker响应，防止突然断电导致内存中的消息丢失。

## offset的具体

partition中的消息是连续、有序的，offset用来唯一标识一条消息。可以分为两种

* Current Offset记录在客户端里面，表示客户端拉到哪条消息了

* Committed Offset存在服务端里面，表示消息被消费到了哪条。

  **总结一下，Current Offset是针对Consumer的拉过程的，它可以保证每次拉消息都返回不重复的消息；而Committed Offset是用于Consumer Rebalance过程的，它能够保证新的Consumer能够从正确的位置开始消费一个partition，从而避免重复消费。**

## 如何处理重复消息

生产者由于网络问题没有收到broker的响应重发了一遍消息，消费者处理完消息后没来得及更新offerset就挂了然后拎一个消费者又出力了一遍都会导致消息被重复处理。可以说在kafka的层面消息的重复是难以避免的。**所以只能曲线救国，来降低消息被重复处理的影响。**比如使用**version版本号**机制来判断是否被处理过，通过订单号等关键记录来判断这条消息有没有被处理过等手段来防止消息的重复处理。

## 有序性

kafka在多分区的情况下只能保证Partition分区内是有序的，Partition之间无法保证有序。

## 如果处理消息堆积

消息堆积的根本原因就是消费者处理的太慢了。

1. 可以改成批量的处理消息（一次性从broker拉多条数据）
2. 增加分区数量和消费者数量

# 项目相关

## 你项目中的AOP是怎么用的？

我在项目中用AOP实现了用户操作日志的功能。具体是使用@Aspect 来声明一个日志切面，并让Spring（@Component）来管理这个类。之后就是在这个切面容器中用@pointcut声明切点，可以是controller，也可以是类。然后根据需求使用before after等注解来实现在方法前还是方法后使用。具体的信息可以从joinpoint对象里面取得。

[]: https://blog.csdn.net/weixin_44806772/article/details/120438164



## 具体怎么做的，用了哪些算法，怎么测试的，后面如果需要优化，该从哪些方面入手
说一下热点排行、敏感词过滤怎么做的。定时任务怎么做到，定时任务后面换线程池。ScheduledExecutorService
## SSM是什么，你怎么用到项目中的
SSM框架是spring MVC，spring-boot和mybatis框架的整合，是标准的MVC模式，他将整个系统划分为表现层，controller层，service层，DAO层四层使用spring MVC负责请求的转发和视图管理spring-boot实现业务对象管理，mybatis实现对数据库的操作。

mybatis：MyBatis使用简单的XML或注解用于配置和原始映射，将接口和Java普通对象映射成数据库中的记录。

IOC:控制反转，是一种降低对象之间耦合关系的设计思想，比如说租房子，没有IOC的话租房子需要一个房子一个房子找，使用了IOC的思想之后就相当于加入一个房屋中介，把你需要的房型告诉中介，就可以直接选到需要的房子，中介就相当于spring容器，由容器来管理对象之间的依赖。
AOP:面向切面编程，是面向对象开发的一种补充，它允许开发人员在不改变原来模型的基础上动态的修改模型以满足新的需求，如：动态的增加日志、安全或异常处理等。AOP使业务逻辑各部分间的耦合度降低，提高程序可重用性，提高开发效率。

## 健康建议是怎么得到的？
这个建议的具体内容是我问一个食品专业的同学的。  
然后建议是由两部分组成的：
* 今天的表现，具体就是吃了哪些东西，然后吃的是不是合理。这个如果某项指标超过或者不到推荐摄入值的10%，就认为是不合理的。
* 应该怎么样。这个就根据今天的表现来对应生成。比如热量摄入过多久推荐慢跑多久多久这样。
## 做项目的时候遇到了什么样的困难，怎么解决的？
* 在做热帖排行功能的时候一开始我是每5min就重新计算一次所有帖子的分数。但是因为当时一开始我造数据造的太多了，导致计算全部帖子的分数有点慢，再加上我租的那个云服务器是最低档的单核的就更慢了。所以我就想到每次只计算数据有变化的帖子和排行榜上的帖子，其他收藏、点赞等数据没有变化的帖子都不重新计算分数了。具体我是用Redis解决的，每次一个帖子的收藏、点赞等数据更新的时候我就把帖子id作为value记录到redis里面。然后要重新计算分数的时候就只计算redis里面的这些帖子。
* 敏感词过滤我一开始用的是字符串匹配。把所有发布的字符串都拿来和铭感词库里面的字符串匹配一遍，如果匹配得上就替换成*号，不然就直接发布。字符串匹配用的还是最笨的暴力匹配，后来因为敏感词库多了改成了kmp但还是很慢。最后查了些资料，在github上找了个DFA算法，自己改了改用上去的。
## 哪些地方使用到了Redis
我用Redis做了用户**登陆凭证的缓存**。在论坛界面跳转的时候，客户端需要向服务端发送一个ticket来表明身份。因为这个跳转比较频繁，如果不使用Redis缓存一下的话，每一次跳转都会去查询一遍数据库，会给服务器造成比较大的压力。因此我把用户的Ticket缓存到了Redis中，并且设置了过期时间。当用户登录的时候，就会以用户名+ticket这几个字母为key，随机生成的ticket的内容为value。将其缓存到Redis里面。如果用户勾选了“记住我”这个选项那么key的过期时间是7天，不然就是1天。  
最后**热帖排行**的实现也用到了Redis，我把被点赞和收藏了的帖子都存到Redis的缓存中。然后设置了一个定时任务，每隔一段时间就去读取缓存中的帖子ID，然后根据帖子的回复、关注、发帖时间等来计算帖子的分数。并根据分数进行排序。

## 哪些地方使用了kafka

我用kafka实现了点赞、关注和回复的消息通知。当用户点赞、关注或者被回复消息的时候，我会将用户ID，帖子Id，要发布的topic等信息封装到一个Event类中，并将这个类转成json字符串发给kafka服务。消费者类会监听（@KafkaListener）设定topic中的消息，在监听到有新消息后自动从kafka服务中取出消息转成Event对象再根据Event对象中信息来进行下一步的操作。

## 哪些地方用到了多线程

**kafka的消费者那里用到了多线程**：因为我只有一台机器所以kafka的topic只有一个Partition，所以没有办法开多个Consumer来消费，所以我实在这个consumer里面使用多线程。一次从topic中拉取多条消息，然后把这些消息交给线程池来处理。线程池在处理时，会把完成的消息对应的offset存到一个共享的list里面，当这一批消息全部处理完成后才会提交。

**Redis数据持久化**也用到了多线程，但是这个不算是我写的我只是直到他用了多线程

## 这个监听是怎么实现的？

具体的我还没有看过，但是我猜测应该是在后台起了一个线程来轮询broker。当有消息的时候就直接返回，没有就等待一个超时时间直到超时。然后发起下一次轮询。

## 定时任务是怎么设置的？
我用的是Linux自带的那个crontab来设置的定时任务。他会定时访问那个计算帖子分数的方法 curl。现在想想这个就很蠢，因为这就意味着把这个计算分数的方法开放给外界了。外界的人也可以调用。
## 怎么通过IO流来传输菜品图片的？
我首先通过从数据库中获得的菜品名称来拼接出菜品图片的路径。然后使用使用``OutputSteam``获得图片的字节流，之后将字节流通过``ImageIO``包输出给前端就可以了。
## 敏感词过滤是怎么实现的？
这个敏感词过滤算法我是通过DFA算法实现的，首先将需要过滤的所有敏感词构造成一个多叉树。然后比对输入的字符串，如果这个字符串中的某一个部分和多叉树中的某一块是一样的，那说明这一块就是敏感词了。然年后在对比的时候会过滤掉“空格啊，%$”等一些特殊字符，防止用户用这些特殊字符把敏感词隔开。判断出一个之后，在把指针移动到多叉树的头部，继续对比。

