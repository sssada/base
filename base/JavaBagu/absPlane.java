/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-29 22:54:48
 * @ Modified by: OldZhang
 * @ Modified time: 2022-07-05 10:01:29
 * @ Description:
 */

package base.JavaBagu;

public abstract class absPlane {
    private String wheel;
    private String wings;
    private String chair;
    abstract void fly();
    abstract void refuel();
}
