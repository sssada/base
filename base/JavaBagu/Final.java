/**
 * @ Author: OldZhang
 * @ Create Time: 2022-06-29 22:54:48
 * @ Modified by: OldZhang
 * @ Modified time: 2022-09-19 21:48:11
 * @ Description:
 */

package base.JavaBagu;

import java.util.ArrayList;
import java.util.List;

public class Final {
    public static void main(String[] args) {
        final List<Integer> list=new ArrayList<>();
        list.add(1);
        System.out.println(list);
        list.add(2);
        System.out.println(list);
        list.set(0,100);
        System.out.println(list);
        // list=null;
    }
    
}
