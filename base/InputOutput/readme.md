# Java标准输入
创建一个Scanner对象和标准输入流System.in关联。
 ```
  Scanner sc=new Scanner(System.in);
 ```
 之后就可以调用Scanner类中的方法从命令行中读取数据。大体格式是``hasXXX()``,``nextXXX()``。
 |名称|作用|
 |--|--|
 |``sc.next()``|获取下一个单词，以空格为分隔|
 |``sc.nextInt()``|获取下一个int|
 |``sc.nextLine()``|获取下一行，以回车会标志|
 |``sc.nextLong()``|获取下一个Long|
 |``sc.hasNext()``|判断有没有下一个单词|
 |``sc.hasNextInt()``|判断有没有下一个int|
 
还有其他的，命名都是按照这个规范来的。
**在使用了sc.nextXXX()后的第一个nextLine()是读不到任何值的，因为sc.nextXXX()读取到一行的回车前就停了还剩下个回车符。之后你第一次使用的nextLine读到的就是这个回车符。**

# Java标准输出
可以先创建一个格式化的String，然后再输出
```
String message = String.format("Hello, %s. Next year, you'll be %d", name, age);
```
也可以直接输出
```
System.out.printf("%.2f", x);// 保留两位小数
```

<img src="输出格式.png">


