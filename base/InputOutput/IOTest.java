package base.InputOutput;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Test;

public class IOTest{
  
    public static void main(String[] args) {
    //   getNums();
    // getLineNums();
    nextLineTest();
    }
    /*
     * 接受列表
     */
    public static void getNums(){
        Scanner sc=new Scanner(System.in);
        List<Integer> list=new ArrayList<>();
        while(sc.hasNext()){
            list.add(sc.nextInt());
        }
        System.out.println(list);
        sc.close();
    }
    /*
     * 接收多个列表
     */
    public static void getLineNums(){
        Scanner sc=new Scanner(System.in);
        List<List<Integer>> list=new ArrayList<>();
      
        while(sc.hasNextLine()){
            List<Integer> temp=new ArrayList<>();
            String str=sc.nextLine();
            int bg=0;
            for(int i=0;i<str.length();i++){
                if(str.charAt(i)==' '){
                    temp.add(Integer.parseInt(str.substring(bg,i)));
                    bg=i+1;
                }
            }
            list.add(temp);
        }
        System.out.println(list);
        sc.close();
    }
    /*
     * 换行测试
     */
    public static void nextLineTest(){
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        System.out.println(a);
        String b=sc.nextLine();
        System.out.println(b);
        String c=sc.nextLine();
        System.out.println(c);
    }
}