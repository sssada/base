package base.searchAlgorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BlockSearch {
    private List<Integer> max = new ArrayList<>();// 块内放最大值
    private List<Integer> index = new ArrayList<>();// 块起始位置
    private int[] nums;

    /**
     * 进行分块
     * 
     * @param nums
     */
    BlockSearch(int[] nums) {
        this.nums = nums;
        int n = (int) Math.sqrt(nums.length);
        int s = (int) nums.length / n;// 块内元素个数
        int count = 0;
        max.add(0);
        index.add(0);
        for (int i = 0; i < s; i++) {
            max.set(0, Math.max(max.get(0), nums[i]));
        }
        index.add(s);
        for (int i = s; i < nums.length; i++) {

            if (nums[i] <= max.get(max.size() - 1)) {
                if (max.size() == index.size()) {
                    int last = max.size() - 1;
                    max.set(last, Math.max(max.get(last), nums[i]));
                } else {
                    int last = index.size() - 1;
                    index.set(last, index.get(last) + 1);
                }

            } else {
                if (max.size() == index.size()) {
                    int last = max.size() - 1;
                    max.set(last, Math.max(max.get(last), nums[i]));
                } else {
                    max.add(nums[i]);
                }

                if (count++ == s) {
                    index.add(i + 1);
                    count = 0;
                }
            }
        }
        if (index.size() > max.size()) {
            index.remove(index.size() - 1);
        }
    }

    /**
     * 
     * @param nums
     * @param target
     * 
     * @return
     */
    public int blockSearch(int target) {
        int a = binarySearch(max, 0, max.size()-1, target);
        if(a==-1)return a;
        int end = a == index.size() - 1 ? nums.length-1 : index.get(a + 1);
        for (int i = index.get(a); i <=end; i++) {
            if (nums[i] == target) {
                return i;
            }
        }
        return -1;
    }

    public int binarySearch(List<Integer> max, int bg, int end, int target) {
        if(bg<0||end>=max.size()){
            return -1;
        }
        int mid = (bg + end) / 2;

        if(max.get(mid)==target)return mid;
        if (mid == max.size() - 1) {
            if (target < max.get(mid)&&target>max.get(mid-1)) {
                return mid;
            } 

        }

        if (mid == 0){
            if (target > max.get(mid)&&target<max.get(mid+1)) {
                return mid+1;
            }else if(target<max.get(mid))return mid;
         
        }
           
        if (max.get(mid) < target) {
            if (max.get(mid + 1) > target)
                return mid+1;
            else {
                return binarySearch(max, mid + 1, end, target);
            }
        }
        if (max.get(mid) > target) {
            if (max.get(mid - 1) < target)
                return mid;
            else {
                return binarySearch(max, bg, mid - 1, target);
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] nums = new int[100000];

        Random r=new Random();
        for (int i = 0; i < nums.length; i++) {
            nums[i]=r.nextInt(100000);
        }
        BlockSearch b = new BlockSearch(nums);
        for (int i = 0; i < nums.length; i++) {
            System.out.println(b.blockSearch(nums[i]));
        }
       
    }

}
