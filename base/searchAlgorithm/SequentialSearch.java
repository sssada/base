package base.searchAlgorithm;

import org.junit.Test;

public class SequentialSearch {
    public static void main(String[] args) {
        
    }
    @Test
    public int sequentialSearch(int[] nums,int target) {
        for (int i = 0; i < nums.length; i++) {
            if(nums[i]==target)return i;
        }
        return -1;
    }
}
