package base.searchAlgorithm;

public class BinarySearch {
    public int binarySearch(int[] nums,int target) {
        int b=0,e=nums.length-1;
        while(b<=e){
            int mid=(b+e)/2;
            if(nums[mid]==target) return mid;
            if(nums[mid]<target){
                b=mid+1;
            }
            if(nums[mid]>target){
                e=mid-1;
            }
        }
        return -1;
    }
    
    public int binarySearch(int[] nums,int target,int b,int e) {
        if(b>e) return -1;
        int mid=(b+e)/2;
        if(nums[mid]<target)return binarySearch(nums,target,mid+1,e);
        if(nums[mid]>target)return binarySearch(nums,target,b,mid-1);
        else return mid;
    }
}
