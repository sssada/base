# 游戏测试
## 游戏测试用例要点
### UI界面测试
* 画面、故事情节、背景音乐和文字的契合度
* 图片的显示、文字的排版、布局是否正常（在不同分辨率下会不会乱）
  
### 功能测试（数值、活动、存档、特殊功能是否实现了）
这个要对于不同的功能，不同的测试。首先要分析这个功能的需求，然后根据需求进行测试用例的设计
* 功能是否实现了。  
* 活动  
  * 测试活动的时间点（比如每天刷新可抽奖次数，那么再抽奖页面停留到第二天之后是否会刷新），  
  * 以及如果需要组队完成的话就要测试多个玩家
* 存档
  * 设置的自动存档有没有正常运行
  * 暂停时有没有自动存档
  * 意外退出有没有自动存档
  * 需要联网的游戏断网后有没有存档
### 可用性测试
* 操作的反馈是否顺滑
* 键鼠操作是否方便
* 手柄操作是否方便
* 陀螺仪、重力感应能不能用
### 性能测试
* 帧数
  * 有没有剧烈波动
  * 能否维持30或60以上
* 一般主流玩家的显卡和CPU的占用是否在正常的范围内
* 内存占用率
* 能否在低频内存和高频内存下运行
* 耗电量
* 流量
* 多人在线时服务器的内存和CPU占用率
###  安全测试
* 客户端
  * 防止JS脚本注入、SQL语句注入
  * 注册/登录框的测试
  * 连续错误有没有开启验证
  * 多开客户端和同一个账号的重复登录

* 服务端
  * 玩家信息的安全性
  * 能否防止外挂
  * 合服或者换代理商的时候用户信息的转移是否安全
### 兼容性
* 不同的硬件设备能否正常的安装与卸载
* 对于不同硬件设备的支持AMD、Intel、Nvidia
* 不同软件环境能否正常运行（浏览器版本、c++的库不同、jre版本）
* 热启动是不是正常
### 压力测试、强度测试
* 长时间多用户在线，服务器的CPU、内存情况。
* 单机测长时间运行，主流配置的散热、耗电、显卡CPU内存使用量。

### 剑网三背包测试用例
因为是单个功能的测试，所以主要是功能测试
1. UI界面测试  
   * 背包界面是否合理
   * 背包中文字说明是否贴切
   * 打开背后后是否会干扰到其他界面布局
   * 打开背包的音效是否合理
   * 能不能防八门神器
2. 功能测试  
   * 获得物品后是否会在背包中显示
   * 获得已有物品后是否能够叠加
   * 物品叠加到上限后是否会占用新的一格位置
   * 获得已有物品后是否能够占用新的一格位置
   * 获得新物品后是否能够占用新的一格位置
   * 自动整理是否按照一定规则来
   * 自动整理时是否会将相同物品合并
   * 拆分物品是否会正常运行
   * 拆分数量大于背包上限时能否拆分成功
   * 当物品数量等于背包上限时，背包上限能否减少。
   * 当物品数量等于背包上限时，背包上限减少了，物品是否会丢失。
   * 物品消耗完后是否会从背包中去除
   * 消耗完之后空出的位置是否会被之后的物品递补
   * 新物品是放在最前面的空位中还是放在从有物品开始的最后的空位
   * 当背包已满时能否获得物品
   * 包内物品是否有拥有上限
   * 上限物品达到上限后是否还能获得该物品
   * 上限之后呗使用了几个之后能否继续获得该物品
   * 限时物品到期后时候后是否会丢弃
   * 更换左侧小背包位置，其中物品是否会跟着变换（剑三的背包是由好几个不同容量的小背包组成的）
   * 在小背包还有物品的情况下能否替换小背包
   * 出售物品，但是不卖完。检查出售后物品数量是否正确
   * 出售完物品，检查物品数量是否正确
   * 背包满了之后，使用获丢弃意见物品看看还能否获得新物品
   * 背包满了之后，使用一个可堆叠物品（这个时候背包格子还是满的），看看能否获得这个可堆叠物品
3. 可用性测试  
   * 背包内各种按钮点击是否方便
   * 打开关闭的动画是否平滑
4. 性能测试  
   * 背包自动整理在物品数量较多时是否迅速
   * 打开背包有没有卡顿

   
